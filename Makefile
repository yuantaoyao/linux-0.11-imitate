RAMDISK= -DRAMDISK=512
ASM =nasm
AS	=as
LD	=ld
GCC =gcc

LDFLAGS= -m elf_i386 -Ttext 0 -e startup_32
CFLAGS= -m32 -g -Wall -O -fstrength-reduce -fomit-frame-pointer -funroll-loops \
	-finline-functions -fno-stack-protector $(RAMDISK)

#
# ROOT_DEV specifies the default root-device when making the image.
# This can be either FLOPPY, /dev/xxxx or empty, in which case the
# default of /dev/hd6 is used by 'build'.
#
ROOT_DEV= #FLOPPY 

#params of env 
d_floppy:=/mnt/floppy

#directors of system
d_boot:=boot/
d_tools:=tools/
d_init:=init/
d_kernel:=kernel/
d_chr_drv:=chr_drv/
d_blk_drv:=blk_drv/
d_math:=math/
d_lib:=lib/
d_fs:=fs/
d_mm:=mm/

#files of system
f_img:=a.img

ARCHIVES=$(d_kernel)kernel.o $(d_fs)fs.o $(d_mm)mm.o
DRIVERS=$(d_kernel)$(d_chr_drv)chr_drv.a $(d_kernel)$(d_blk_drv)blk_drv.a $(d_kernel)$(d_math)math.a
LIBS=$(d_lib)lib.a

.c.s:
	$(GCC) $(CFLAGS) \
	-nostdinc -Iinclude -S -o $*.s $<
.s.o:
	$(AS)  -o $*.o $<
.c.o:
	$(GCC) $(CFLAGS) \
	-nostdinc -Iinclude -c -o $*.o $<

all : image

image: buildimg

buildimg: bootstrap $(d_tools)system $(d_tools)build
	cp -f $(d_tools)system system.tmp
	strip system.tmp
	objcopy -O binary -R .note -R .comment system.tmp $(d_tools)kernel
	$(d_tools)build boot/bootsect.bin boot/setup.bin $(d_tools)kernel $(ROOT_DEV) > Image
	@echo -e '\033[36m Image deploy success \033[0m'
	sync


$(d_tools)system: $(d_init)main.o $(ARCHIVES) $(DRIVERS) $(LIBS)
	$(LD) $(LDFLAGS) $(d_boot)head.o $(d_init)main.o    \
	$(ARCHIVES) \
	$(DRIVERS) \
	$(LIBS) \
	-o $(d_tools)system 
	nm $(d_tools)system | grep -v '\(compiled\)\|\(\.o$$\)\|\( [aU] \)\|\(\.\.ng$$\)\|\(LASH[RL]DI\)'| sort > System.map

$(d_tools)build: $(d_tools)build.c
	$(GCC) $(CFLAGS) \
	-o $(d_tools)build $(d_tools)build.c
	
bootstrap: FORCE
	(cd boot/; make)

$(d_kernel)kernel.o: FORCE
	(cd $(d_kernel); make)
$(d_fs)fs.o: FORCE
	(cd $(d_fs); make)
$(d_mm)mm.o:FORCE
	(cd $(d_mm); make)
$(d_kernel)$(d_chr_drv)chr_drv.a: FORCE
	(cd $(d_kernel)$(d_chr_drv); make)
$(d_kernel)$(d_blk_drv)blk_drv.a: FORCE
	(cd $(d_kernel)$(d_blk_drv); make)
$(d_kernel)$(d_math)math.a: FORCE
	(cd $(d_kernel)$(d_math); make)
$(d_lib)lib.a: FORCE
	(cd $(d_lib); make)
	
clean :
	(cd $(d_boot)/; make clean)
	(cd $(d_kernel); make clean)
	(cd $(d_kernel)$(d_chr_drv); make clean)
	(cd $(d_kernel)$(d_blk_drv); make clean)
	(cd $(d_kernel)$(d_math); make clean)
	(cd $(d_lib); make clean)
	(cd $(d_fs); make clean)
	(cd $(d_mm); make clean)
	rm -f *.bin $(d_init)main.o
	rm system.tmp System.map Image $(d_tools)system $(d_tools)build $(d_tools)kernel
	@echo -e '\033[31m data clean success \033[0m'
	@echo -e '\033[32m success \033[0m'
FORCE:

$(d_init)main.o: $(d_init)main.c include/unistd.h include/sys/stat.h \
  include/sys/types.h include/sys/times.h include/sys/utsname.h \
  include/utime.h include/time.h include/linux/tty.h include/termios.h \
  include/linux/sched.h include/linux/head.h include/linux/fs.h \
  include/linux/mm.h include/signal.h include/asm/system.h \
  include/asm/io.h include/stddef.h include/stdarg.h include/fcntl.h

