;%define _BOOT_DEBUG_

%ifdef _BOOT_DEBUG_
	org 0x0100
%else
	org 0x07c00
%endif
SETUPLEN equ 4
BOOTSEG equ 0x07c0
INITSEG equ 0x9000
SYSSEG equ 0x1000
SETUPSEG equ 0x9020
SYSSIZE equ 0x3000
ENDSEG equ SYSSEG + SYSSIZE

; ROOT_DEV 0x000 根文件系统设备使用与引导时同样的软驱设备
;	   0x301 根文件系统设备在第一个硬盘的第一个分区上
; 0x300 - /dev/hd0 -代表整个第1个硬盘
; 0x301 - /dev/hd1 -第1个盘的第1个分区
; 0x302 - /dev/hd2 -第1个盘的第2个分区
; 0x303 - /dev/hd3 -第1个盘的第3个分区
; 0x304 - /dev/hd4 -第1个盘的第4个分区

; 0x305 - /dev/hd5 -代表整个第2个硬盘
; 0x306 - /dev/hd6 -第2个盘的第1个分区
; 0x307 - /dev/hd7 -第2个盘的第2个分区
; 0x308 - /dev/hd8 -第2个盘的第3个分区
; 0x309 - /dev/hd9 -第2个盘的第4个分区
ROOT_DEV equ 0x306

	jmp short _start
	nop
_start:
	mov ax, BOOTSEG
	mov ds, ax
	mov ax, INITSEG
	mov es, ax
	sub di, di
	sub si, si
	mov cx, 256
	cld			; 复位方向位 使DF=0  STD使DF=1
	; cp_str_loop:
	; 	lodsw         ;lodsb/lodsw 将位于ds:si所指的存储单元中的一个字节/字存储到al/ax中 si自动加/减 1/2 方向位DF=0 si自加 DF=1 si自减 
	;	stosw		 ; stosb/stosw 将位于al/ax中的值存储于指针ES:DI所指向的内存单元并根据DF位执行加/减 1/2 的操作
	; loop cp_str_loop			;循环将启动区512个字节的数据复制到0x9000:0处
	rep
	movsw
	jmp INITSEG:go-(BOOTSEG << 4) ; 跳转到新的代码区 此处为段间跳转                                                                                                     ;  0x7c19

go:	
	mov ax, cs
	mov ds, ax  ; 由于跳转到了新的地址以前的段地址就不再有效此处给段寄存器重新赋值
	mov es, ax  ; 给es寄存器重新赋值
	; 以下开辟一段栈空间 栈低位于0x9FF00（0x9000:0xFF00）处
	mov ss, ax   
	mov sp, 0xFF00
	
load_setup:			   ;13号中断为软驱/硬盘操作中断 如果出错则CF 标志置位。
	mov dx, 0x0000             ; dh 磁头号
	mov cx, 0x0002		   ; ch 柱面 cl 扇区
	mov bx, 0x0200		   ; bx 偏移量
	mov ax, 0x0200 + SETUPLEN  ;将磁盘中从第二个扇区开始的4个扇区的内容加载到地址ES:BX   ah 02 读扇区 al 读扇区个数 
	int 13h			   ; 执行中断将2、3、4、5扇区复制到 ES:BS指向的地址	
	jnc ok_load_setup	   ; 如果成功执行则跳转 否则驱动复位后循环执行
	mov dx, 0x0000
	mov ax, 0x0000
	int 13h
	jmp load_setup

ok_load_setup:
	mov dl, 0x00			;dl 为驱动器参数 00H~7FH：软盘；80H~0FFH：硬盘
	mov ax, 0x0800			; ah = 08H 读驱动器参数
	int 0x13			; 出口 ch 柱面数低8位 cl 7-6 柱面高2位 cl 5-0 扇区数 dh 磁头数 dl 驱动数 es:di 磁盘驱动参数表地址
	mov ch, 0x00			; 默认柱面不会超过256 所以 cx 为每磁道扇区数
	;seg	cs
	mov si, sectors			; 获取sectors相对地址
	call get_str_offset
	mov word [cs:si], cx	; 将扇区数保存到 sectors
	mov ax, INITSEG
	mov es, ax			; 恢复 es值 为当前段

	mov ah, 0x03			; ah 03H 在文本坐标下读取光标各种参数
	xor bh, bh			; bh 显示页号
	int 0x10			; 出口参数 ch 光标起始行 cl 光标终止行 dh 行（y坐标） dl 列 （x坐标）

	call CLEARSCREEN ;清屏

	mov ax, 0x02
	xor dx,dx
	mov dh, 0x05
	int 0x10
	
	xchg bx, bx

	mov	ax, msg1		; 此处的地址是移动前的地址 后期需要处理才能找到字符串
	mov	bp, ax			; ┓
	mov	ax, cs			; ┣ ES:BP = 串地址
	mov	es, ax			; ┛
	mov	cx, msg1len		; CX = 串长度
	call PRINTSTR

	mov ax, SYSSEG			;5 以后的扇区数据 将被加载到 0x10000后面
	mov es, ax
	call read_it
	call kill_motor


	;seg cs	
	mov si, root_dev			; 获取sectors相对地址
	call get_str_offset
	mov	ax,[cs:si]
	cmp	ax,0
	jne	root_defined
	;seg cs
	mov si, sectors			; 获取sectors相对地址
	call get_str_offset
	mov	bx, [cs:si]
	mov	ax, 0x0208		; /dev/ps0 - 1.2Mb
	cmp	bx, 15
	je	root_defined
	mov	ax, 0x021c		; /dev/PS0 - 1.44Mb
	cmp	bx, 18
	je	root_defined
undef_root:
	jmp undef_root
root_defined:
	;seg cs
	mov si, root_dev			; 获取sectors相对地址
	call get_str_offset
	mov	[cs:si], ax
	
	call CLEARSCREEN ;清屏
	
	jmp	SETUPSEG:0
;变量:
;---------------------------------------------------------------------------------------
sectors:        dw	0		; 每磁道的扇区数
root_dev:	dw      ROOT_DEV
sread:	        dw 	1+SETUPLEN	        ; sectors read of current track  当前已读取的磁头和柱面下的扇区号 默认刚刚读完第5扇区
head:		dw 	0			; current head   当前正在读的磁头号
track:		dw 	0			; current track  当前正在读的柱面号

msg1len:	equ	24
;---------------------------------------------------------------------------------------	
;字符串:
;---------------------------------------------------------------------------------------
msg1: 	db	13,10,"Loading system ... ",13,10,13,10
;---------------------------------------------------------------------------------------

read_it:
	mov ax, es			;查看SYSSEG是否是0x1000 的倍数 如果不是程序将进入死循环
	test ax, 0x0FFF
die:	
	jne die
	xor bx, bx
; 以下代码将正式进入读扇区功能 较为复杂
rp_read:
	mov ax, es
	cmp ax, ENDSEG		; 判断是否还有数据需要载入内存
	jb ok1_read		; 如果有将跳转 否则函数返回
	ret
ok1_read:
	mov si, sectors			; 获取sectors相对地址
	call get_str_offset
	mov ax, word [cs:si]		; 将每磁道的扇区数加载进ax寄存器
	mov si, sread			; 获取sectors相对地址
	call get_str_offset
	sub ax, word [cs:si]		; 每磁道的扇区数减去读取的扇区数 ax存储该磁道剩余扇区数		
	mov cx, ax
	shl cx, 9			; 将剩余扇区数左移9位 即 扇区数*512
	add cx, bx			; 通过加法检测是否有进位 是否小于 0x10000
	jnc ok2_read			; 无进位时跳转即 (cx *512 +bx) < 0x10000
	je ok2_read			; zf = 1 (cx = 0) 即刚好等于0x10000  即65535b的大小
	xor ax, ax			; 如果 (cx *512 +bx) 》0x10000
	sub ax, bx			; ax = 0 - bx 即 0x10000 - bx 即 bx关于0x10000 的模
	shr ax, 9			; 右移9位 即 ax = ax / 512 al 作为待读取扇区数传给read_track
ok2_read:
	call read_track			; 读扇区操作	
	mov cx,ax			; ax 中存储已经读完的扇区数
	mov si, sread			; 获取sectors相对地址
	call get_str_offset
	add ax, word [cs:si]		; 本次读取与上一次已经读完的扇区数相加 此时ax中存储总共读取的扇区数
	;seg cs
	mov si, sectors			; 获取sectors相对地址
	call get_str_offset
	cmp ax, word [cs:si]	; 已经读取的扇区与本磁道所有扇区比较
	jne ok3_read			; 如果不相等则说明本磁道及本磁头下还有扇区没有读完 则跳转到ok3_read
	mov ax, 1			; 如果相等则说明本磁道及本磁头下的扇区已经读取完毕 此处将ax=1
	mov si, head			; 获取sectors相对地址
	call get_str_offset
	sub ax, word [si]		; 磁头号不是1 就是 0 如果是1 则跳转到ok4_read 如果是0 则将柱面号加1
	jne ok4_read
	mov si, track			; 获取sectors相对地址
	call get_str_offset
	inc word [si]                 ; inc 对应操作数加1 将柱面号加1 准备读取下一个柱面
ok4_read:
	mov si, head			; 获取sectors相对地址
	call get_str_offset
	mov word [si], ax		; 通过上面的逻辑可得 如果之前读取的是1号磁头此时ax = 0 如果之前读取的是0号磁头此时ax=1
	xor ax, ax			; 将扇区号值为 0 
ok3_read:
	mov si, sread			; 获取sectors相对地址
	call get_str_offset
	mov word [si], ax	; 将从本磁头、本柱面下第一个扇区开始        。。此时磁头扇区柱面都已指向下已磁头、柱面的0号扇区
 	shl cx, 9		; cx 中存储着已经读完的扇区数 
 	add bx, cx		; bx 一直为 0 
 	jnc rp_read		; if（cx * 512）+bx < 0x10000 jmp rp_read 说明还没有读够65535b 的内存 返回继续读
 	mov ax, es		; 否者 说明bx已经到了64k的边界 即已经到了段存储的临界点
	add ax, 0x1000		; 此处加0x1000 等于启用下一个段
	mov es, ax		; 开启一个新的段存放数据
	xor bx, bx
	jmp rp_read  ; 可见此处是一个循环 只要 ax < ENDSEG 循环将会持续 
	
; 扇区读取函数
read_track:
	push ax		; al中存储待读取扇区数
	push bx
	push cx
	push dx
	mov si, track			; 获取sectors相对地址
	call get_str_offset
	mov dx, word [si] ;当前磁道号
	mov si, sread			; 获取sectors相对地址
	call get_str_offset
	mov cx, word [si] ;当前已读取扇区号 cl 中存储扇区
	inc cx			;定位到将要读取扇区
	mov ch, dl		;将磁道号存入 ch
	mov si, head			; 获取sectors相对地址
	call get_str_offset
	mov dx, word[si] ;当前磁头号
	mov dh, dl	   ;将当前磁头号存入dh
	mov dl, 0		;驱动器，00H~7FH：软盘；80H~0FFH：硬盘
	and dx, 0x0100	; 磁头号 将dh 与 01 逻辑与操作 确保 磁头号不是0 就是1
	mov ah, 2	;读扇区
	int 0x13	; 数据存入es:bx缓冲区
	jc bad_rt ; cx = 0成功 cx=1 失败 如果失败则进入bad_rt失败处理函数
	pop dx
	pop cx
	pop bx
	pop ax
	ret

bad_rt:
	mov ax,0
	mov dx,0
	int 0x13	;软盘复位
	pop dx
	pop cx
	pop bx
	pop ax
	jmp read_track
	
;in/out I/O操作指令 只能与dx,ax,al寄存器结合使用 out向端口写入 in重端口读出
kill_motor:
	push dx
	mov dx, 03F2h
	mov al, 0
	out dx, al
	pop dx
	ret

get_str_offset: ;获取相对地址 解决跳转后字符串地址为原绝对地址问题
	and si,  0x00FF
	ret
;通用方法
;method PRINTSTR
;打印字符串 ES:BP = 串地址 CX = 串长度
PRINTSTR:
	and	bp, 0x00FF	; 解决数据移动后偏移量错误问题 这是因为我们编译的nasm bin文件类型引起的
	mov	ax, 01300h		; AH = 13,  AL = 00h(光标不移动)
	mov	bx, 0002h		; 页号为0(BH = 0) 黑底白字(BL = 07h)
	mov	dh, 0h
	int	10h			; int 10h
	ret
	
;method CLEARSCREEN
;清屏 直接调用
CLEARSCREEN:
	; 清屏
	mov	ax, 0600h		; AH = 6,  AL = 0h 清窗口
	mov	bx, 0700h		; 黑底白字(BH = 07h)
	mov	cx, 0			; 左上角: (0, 0)
	mov	dx, 0184fh		; 右下角: (80, 50)
	int	10h			; int 10h
	ret

times 510 - ($ - $$) db 0
dw 0xaa55
