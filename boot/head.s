/*
 *  linux/boot/head.s
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 *  head.s contains the 32-bit startup code.
 *
 * NOTE!!! Startup happens at absolute address 0x00000000, which is also where
 * the page directory will exist. The startup code will be overwritten by
 * the page directory.
 */
 /*
  * head.s含有32位启动代码，setup.s 开启A20后系统地址扩展到32位，分段机制开启后系统进入32位保护模式，使用32位GNU as汇编
  * head.s 启动代码是从绝对地址0x00000000开始的，此处也是页目录存在的地方，所以head.s程序在运行的时候，边执行，边创建数据结构覆盖掉自身代码段
  *
  */
.text		# 伪代码 告诉编译器后续编译出来的内容放到代码段（程序从这里执行）
.globl idt,gdt,pg_dir,tmp_floppy_area # 伪代码 告诉编译器后续跟的是全局可见标记（可能是变量，也可能是过程段名）
pg_dir:	# 用来标识内核分页机制完成后的内核起始位置，也就是内存地址0x00000000
.globl startup_32
startup_32:			# 32位代码从这里开始
	movl $0x10,%eax		# 将全局描述符表的10h选择子加入到寄存器 因为此描述符的权限是可读可写的 其实他和0x08选择子描述的是一段相同的内存，也就是setup.s所存在的地方，由此可见本程序的代码将来会被被覆盖掉。
	mov %ax,%ds		# 刷新各个段寄存器，此后段寄存器中存储的不再是段信息而是选择子
	mov %ax,%es
	mov %ax,%fs
	mov %ax,%gs
	lss stack_start, %esp  # 将结构体_stack_start的高字节加载到esp寄存器，将低字节加载到ess段寄存器 stack_start定义在kernel/sched.c中
	call setup_idt		# 设置中断描述符表子过程
	call setup_gdt		# 设置全局描述符表子过程
	movl $0x10, %eax	# reload all the segment registers
	mov %ax, %ds		# after changing gdt. CS was already
	mov %ax, %es		# reloaded in 'setup_gdt'
	mov %ax, %fs		# 因为重新修改了gdt，所以需要重新装载所有段寄存器
	mov %ax, %gs		# CS代码段寄存器已经在setup_gdt中重新加载过了
	lss stack_start, %esp
	xorl %eax, %eax
				# 一下通过比较寻址0x000000与0x100000地址是否指向同一块区域的方式判断是否打开A20地址线
1:	incl %eax		# check that A20 really IS enabled
	movl %eax,0x000000	
	cmpl %eax,0x100000
	je 1b

/*
 * NOTE! 486 should set bit 16, to check for write-protect in supervisor
 * mode. Then it would be unnecessary with the "verify_area()"-calls.
 * 486 users probably want to set the NE (#5) bit also, so as to use
 * int 16 for math errors.
 *
 *以上注释说明486cpu中CR0控制寄存器的第16位是写保护位WP(Write-protect)
 * 用于进制超级用户的程序向一般用户只读页面进行写操作， 该标志主要用于操作系统在创建新进程时实现写时复制（copy-on-write）方法
 */

	movl %cr0,%eax		# check math chip
	andl $0x80000011,%eax	# Save PG,PE,ET
	orl $2,%eax		# set MP
	movl %eax,%cr0
	call check_x87
	jmp after_page_tables


check_x87:
	fninit			# 向协处理器发出初始化指令
	fstsw %ax		# 如果协处理器存在协处理器状态字将置0
	cmpb $0,%al		# 如果存在则向前跳转到标号1处 否则改写cr0
	je 1f			/* no coprocessor: have to set bits */
	movl %cr0,%eax
	xorl $6,%eax		/* reset MP, set EM */
	movl %eax,%cr0
	ret
.align 2 # 4字节对齐
1:	.byte 0xDB,0xE4		/* fsetpm for 287, ignored by 387 翻译：这是一条80287协处理器fsetpm机器指令,目的是设置保护模式，如果是80387则会忽略此指令当作空来处理 */
	ret

setup_idt:
	lea ignore_int,%edx   # 取ignore_int的偏移地址（OFFSET）加载到EDX扩展寄存器中
	movl $0x00080000,%eax # 加载GDT段选择符 为了与ignore_int地址拼接成一个完整的指向ignore_int函数的地址
	movw %dx,%ax		/* selector = 0x0008 = cs */ # 此时eax存储中断描述符的基地址（选择符+偏移量）
	movw $0x8E00,%dx	/* interrupt gate - dpl=0, present 中断描述符的高位0-15是中断服务程序的描述信息设为0x8E00 (‭1000 1110 0000 0000)‬ */
				# EDX存储中断描述符的高位 EAX存储中断描述符的低位

				#第47位为段存在标志（P），此位被置位，说明此中断服务程序存在于内存中。

                                #第45-46位为特权级标志（DPL），表示本中断服务程序是内核特权级

				#第40-43位为段描述符类型标志（TYPE），我们设置的是1110.即将此段描述符标记为“386中断门”。
	lea idt,%edi		# 将_idt有效地址装入edi寄存器
	mov $256,%ecx		# 设置循环次数
rp_sidt:			# 开始循环
	movl %eax,(%edi)	# eax值存入es:edi低32位
	movl %edx,4(%edi)	# edx值存入es:[edi+4]高32位
	addl $8,%edi		# 将目标寄存器edi指向下一个中断描述地址
	dec %ecx		# 循环次数减1
	jne rp_sidt		# 如果不为零则继续循环
	lidt idt_descr		# 中断数据全部设置完成后将idtr指向中断向量表的入口和限长
	ret

setup_gdt:
	lgdt gdt_descr		# 全局描述符加载到gdtr寄存器	
	ret

# 计算机寻址从段机制到页机制都是硬件来完成的
# 将32位的线性地址分为3个部分，页目录索引[10位]|页表索引[10位]|偏移地址[12位]
# 开启分页 Linus把页表直接放在页目录之后，使用4个页表来寻址16M的内存，如果想扩充可继续向后添加
.org 0x1000   # .org 编译器伪指令 强制安放内存空间，目的是修改汇编地址计数器的值 该计数器记录着程序执行的偏移地址 该句表示从偏移地址为0x1000处存放第一个页表
pg0:	      # 从0x1000到0x2000共有4096/4=1024个页表项（每项占4字节[32位]） 也就是每个页表有1024页 每页2^12=4096=4k 所以一个页表可寻址4k*1024=4M内存
	      # 由于此处设置了4个页表所以linux0.11可寻址的总大小是 4*1024*4k=16M	
.org 0x2000   # 说明从0x1000开始的原因0-0x1000要存放页目录表 即理论上寻址空间将达到1024*1024*4=4G的空间
pg1:

.org 0x3000
pg2:

.org 0x4000
pg3:

.org 0x5000	# 定义以下的内存数据块从偏移地址0x5000处开始

/*
 * tmp_floppy_area is used by the floppy-driver when DMA cannot
 * reach to a buffer-block. It needs to be aligned, so that it is not
 * on a 64kB border.
 */

tmp_floppy_area:
	.fill 1024,1,0

after_page_tables:
	pushl $0		# These are the parameters to main :-) 	0x5400
	pushl $0		
	pushl $0		# 前三个入栈0应该表示 envp argv 和 argc 的值 main()没有用到
	pushl $L6		# return address for main, if it decides to.
	pushl $main			# 0x5410
	jmp setup_paging
L6:
	jmp L6			# main should never return here, but
				# just in case, we know what happens.

int_msg:
	.asciz "Unknown interrupt\n\r"
.align 2
ignore_int:
	pushl %eax
	pushl %ecx
	pushl %edx
	push %ds    # ds es fs gs 虽然是16位寄存器 但入栈后仍然占用 32位内存空间
	push %es
	push %fs    # 以上为保存现场
	movl $0x10,%eax		# 各段选择器指向 gdt表中的数据段
	mov %ax,%ds
	mov %ax,%es
	mov %ax,%fs
	push $int_msg
	call printk # 函数在/kernel/printk.c中 "_printk"是函数"printk"编译后模块中的内部表示法
	popl %eax
	pop %fs	     # 以下为恢复现场	
	pop %es
	pop %ds
	popl %edx
	popl %ecx
	popl %eax
	iret
/*
 * Setup_paging
 *
 * This routine sets up paging by setting the page bit
 * in cr0. The page tables are set up, identity-mapping
 * the first 16MB. The pager assumes that no illegal
 * addresses are produced (ie >4Mb on a 4Mb machine).
 *
 * NOTE! Although all physical memory should be identity
 * mapped by this routine, only the kernel page functions
 * use the >1Mb addresses directly. All "normal" functions
 * use just the lower 1Mb, or the local data space, which
 * will be mapped to some other place - mm keeps track of
 * that.
 *
 * For those with more memory than 16 Mb - tough luck. I have
 * not got it, why should you :-) The source is here. Change
 * it. (Seriously - it should not be too difficult. Mostly
 * change some constants etc. I left it at 16Mb, as my machine
 * even cannot be extended past that (ok, but it was cheap :-)
 * I have tried to show which constants to change by having
 * some kind of marker at them (search for "16Mb"), but I
 * won't guarantee that's all :-( )
 */
.align 2
setup_paging:
	movl $1024*5,%ecx		/* 5 pages - pg_dir+4 page tables */ # 每个页表占用1024个双字[32位] 共5个页表
	xorl %eax,%eax
	xorl %edi,%edi			/* pg_dir is at 0x000 */ # 页目录表从0x0000开始到0x1000
	cld				
	rep					
	stosl			        # 把1个页目录和4个页表清零eax => es:[edi] edi每次增加4 重复执行ecx次 
	movl $pg0+7,pg_dir		/* set present bit/user r/w */ # pg_dir = head.s开始地址=内存开始地址=页目录表的开始地址
	movl $pg1+7,pg_dir+4		/*  --------- " " --------- */# 7 为页目录表的描述符 这里加完后0x0007 表示页目录表存在 可读可写
	movl $pg2+7,pg_dir+8		/*  --------- " " --------- */
	movl $pg3+7,pg_dir+12		/*  --------- " " --------- */ # 以上分别向页目录中填写了4项内容 因为我们只有4个页表 他们的值分别是0x00010007 0x00020007 0x00030007 0x00040007
	/*
 	 *以下程序理解起来比较困难，主要是因为有一些值没有计算过程而是直接使用了
 	 *首先我们要明确需要完成的工作是初始化从0x1000到0x5000的4个页表，
 	 *因为每个页表项长度4byte,所以每个页表有4096/4=1024个页表项，由此可知4个页表总共有4*1024=4096个页表项(也即4096*4k=16M) 转换成16进制为0xfff
 	 *由于每一个页表项都要映射4k的物理内存所以页表项0映射的物理内存为0x000000 1-0x001000 2-0x002000 ... fff-fff000 由于页表存在且可读写 所以每项加7 为0-0x000007 1-0x001007 2-0x002007 ... fff-fff007
 	 *可知16M内存都被映射完成（包括页目录表和页表）
 	 */
	movl $pg3+4092,%edi		# 3号页表最后一项的偏移地址
	movl $0xfff007,%eax		/*  16Mb - 4096 + 7 (r/w user,p) */
	std			# DF=1 edi递减4字节
1:	stosl			/* fill pages backwards - more efficient :-) */
	subl $0x1000,%eax	# 每填写一项物理地址减0x1000		总共循环了0xfff次
	jge 1b			# 如果小于0说明全添加好了
	xorl %eax,%eax		/* pg_dir is at 0x0000 */
	movl %eax,%cr3		/* cr3 - page directory start */ # 设置完成后开启分页机制，开启方法 1、设置cr3使其页目录表基址指向页目录的内存起始地址
	movl %cr0,%eax										  # 2、将cr0的第32位置位 提示：将0位置位是开启保护模式					
	orl $0x80000000,%eax
	movl %eax,%cr0		/* set paging (PG) bit */ #0x54a4
	ret			/* this also flushes prefetch-queue */ # 返回指令ret 其另一个作用是将main函数弹出并执行 0x54a7

.align 2
.word 0
idt_descr:
	.word 256*8-1		# idt contains 256 entries idtr寄存器存储 "中断向量表的线性基地址（32位）"|"中断向量表的边界（中断向量表的长度[总长度-1]）（16位）"
	.long idt
.align 2
.word 0
gdt_descr:
	.word 256*8-1		# gdtr寄存器存储 "全局描述符表的线性基地址（32位）"|"全局描述符表的边界（中断向量表的长度[总长度-1]）（16位）"
	.long gdt		

	.align 8
idt:	.fill 256,8,0		# idt is uninitialized  256*8byte
/*
 * 1、空项 2、代码段 3、数据段 4、系统调用段（没有使用） 
 * 后面还有252项用来存放 任务的LDT和TSS
 */
gdt:	.quad 0x0000000000000000	/* NULL descriptor */
	.quad 0x00c09a0000000fff	/* 16Mb */
	.quad 0x00c0920000000fff	/* 16Mb */
	.quad 0x0000000000000000	
	.fill 252,8,0			# 剩余252项用0补齐
