;-----------------------------------------------------------
;-----------------------setup.asm---------------------------
;-----------------------------------------------------------
INITSEG equ 0x9000
SYSSEG 	equ 0x1000
SETUPSEG equ 0x9020

jmp short _start
	nop
_start:				;0x90200
	mov ax, INITSEG
	mov ds, ax
	mov ah, 0x03
	xor bh, bh
	int 0x10	; ah = 03 读取光标参数  输出 ch 起始行 cl 终止行 dh y坐标 dl x坐标
	mov word [0], dx	; 将光标位置记录到 9000:0 处  （bootsect 的位置）	

	; 获取扩展内存 （kb）
	mov ah, 0x88
	int 0x15	; ah 88 读取扩展内存大小 出口 AX＝扩展内存字节数(以K为单位)  从1M以后算扩展内存   t(ax=8800)
	mov word [2], ax

	;获取显示器数据
	mov ah, 0x0f	;读取显示器模式 输出 ah 屏幕字符列数 al 显示器模式（查看00H） bh 页码
	int 0x10
	mov word [4], bx
	mov word [6], ax

	;配置显示器
	mov ah, 0x12
	mov bl, 0x10	;读取配置信息
	int 0x10
	mov word [8], ax
	mov word [10], bx
	mov word [12], cx
	
	;复制hd0 的硬盘参数表，参数表地址是硬盘向量0x41的值，表长度16b
	mov ax, 0x0000
	mov ds, ax
	lds si, [4*0x41] ;将内存[4*0x41]处的高2位传给ds 第2位传给si
	mov ax, INITSEG
	mov es, ax
	mov di, 0x0080
	mov cx, 0x10
	cld
	; cp_str_loop_h0:
	;	lodsb         ;lodsb/lodsw 将位于ds:si所指的存储单元中的一个字节/字存储到al/ax中 si自动加/减 1/2 方向位DF=0 si自加 DF=1 si自减 
	;	stosb		 ; stosb/stosw 将位于al/ax中的值存储于指针ES:DI所指向的内存单元并根据DF位执行加/减 1/2 的操作
	; loop cp_str_loop_h0			
	rep
	movsb
	
	;复制hd1 的硬盘参数表，参数表地址是硬盘向量0x41的值，表长度16b
	mov ax, 0x0000
	mov ds, ax
	lds si, [4*0x46] ;将内存[4*0x41]处的高2位传给ds 第2位传给si
	mov ax, INITSEG
	mov es, ax
	mov di, 0x0090
	mov cx, 0x10
	cld
	; cp_str_loop_h1:
	;	lodsb         ;lodsb/lodsw 将位于ds:si所指的存储单元中的一个字节/字存储到al/ax中 si自动加/减 1/2 方向位DF=0 si自加 DF=1 si自减 
	;	stosb		 ; stosb/stosw 将位于al/ax中的值存储于指针ES:DI所指向的内存单元并根据DF位执行加/减 1/2 的操作
	; loop cp_str_loop_h1
	rep
	movsb
	
	; 检测是否有第二块硬盘
	mov ax, 0x1500	;读取驱动器
	mov dl, 0x81	;驱动器，00H~7FH：软盘；80H~0FFH：硬盘 可见此处是硬盘
	int 0x13	;出口参数：CF＝1——操作失败，AH＝状态代码，参见功能号01H中的说明，
	jc no_disk1
	cmp ah, 0x03	;03H — 硬盘，
	je is_disk1
no_disk1:
	mov ax, INITSEG
	mov es, ax
	mov di, 0x0090
	mov cx, 0x10
	mov ax, 0x00
	rep 
	stosb			;清空第二块硬盘参数

is_disk1:
	cli	; 关闭中断 执行完下面操作后中断向量表将被覆盖
	mov ax, 0x0000
	cld
;由于之前bootsect.s 引导程序将 system 模块读入到 0xl0000 开始的位置。当时假设 system 模块最大长度不会超过 0x80000 (512KB)，即其末端不会超过内存地址 0x90000，
;所以 bootsect.s 会把自己移动到0x90000 开始的地方，并把 setup 加载到它的后面。下面这段程序的用途是再把整个 system 模块移动到 0x00000 位置，
;即把从 0x10000 到 0x8ffff 的内存数据块(共512KB)整块地向内存低端移动了0x10000(64KB)。
do_move:
	mov es, ax
	add ax, 0x1000
	cmp ax, 0x9000
	jz end_move
	mov ds, ax
	xor di, di
	xor si, si
	mov cx, 0x8000
	push ax
	; loop_move_sysseg:
	;	lodsw
	;	stosw
	; loop loop_move_sysseg
	rep
	movsw
	pop ax
	jmp do_move
end_move:
	mov ax, SETUPSEG
	mov ds, ax		; 恢复ds 0x902a8
	lidt [idt_48]
	
	lgdt [gdt_48]           ;GDTR中存放的是GDT在内存中的基地址(32位)和其表长界限(16位)。
				;GDTR访问全局描述符表是通过“段选择子”（实模式下的段寄存器）来完成的 段选择子包括三部分：描述符索引（12位）、TI（1位）、请求特权级（RPL 2位）
				;GDTR通过段选择子中的索引号就能找到描述符表中的相应描述符，然后描述符表中的段基址加上逻辑地址（base:offset）就可以转换成线性地址， TI表示不同的描述符1:LDT 0:GDT 
				;特权级分 0 1 2 3 四级 说明：cpu只允许访问同等特权级或比自己低的特权级的段
	call empty_8042	
	mov al, 0xD1
	out 0x64, al
	call empty_8042
	mov al, 0xDF
	out 0x60, al
	call empty_8042

; well, that went ok, I hope. Now we have to reprogram the interrupts :-(
; we put them right after the intel-reserved hardware interrupts, at
; int 0x20-0x2F. There they won't mess up anything. Sadly IBM really
; messed this up with the original PC, and they haven't been able to
; rectify it afterwards. Thus the bios puts interrupts at 0x08-0x0f,
; which is used for the internal hardware interrupts as well. We just
; have to reprogram the 8259's, and it isn't fun.
; 8259A主芯片端口是0x20-0x21 从芯片端口是0xA0-0xA1。输出值为0x11表示初始化命令开始
; 它是ICW1命令字，表示边缘触发，多片8259级联、最后要发送ICW4命令字
	mov	al,0x11		; initialization sequence 初始化命令字
	out	0x20,al		; send it to 8259A-1   发送到8259A主芯片
	jmp short $+2		; jmp $+2, jmp $+2
	out	0xA0,al		; and to 8259A-2	发送到8259A从从芯片
	jmp short $+2

	mov	al,0x20		; start of hardware int's (0x20)
	out	0x21,al		; 送主芯片ICW2命令字，设置起始中断号，要送奇端口
	jmp short $+2
	
	mov	al,0x28		; start of hardware int's 2 (0x28)
	out	0xA1,al		; 送主芯片ICW2命令字，从芯片的起始中断号
	jmp short $+2

	mov	al,0x04		; 8259-1 is master
	out	0x21,al		; 送主芯片ICW3命令字，主芯片的IR2立案从芯片INT
	jmp short $+2

	mov	al,0x02		; 8259-2 is slave
	out	0xA1,al		; 送从芯片ICW3命令字，表示从芯片的INT连接到主芯片的IR2引脚上
	jmp short $+2
	
	mov	al,0x01		; 8086 mode for both
	out	0x21,al		; 送主芯片ICW4命令字。8086模式，普通EOI、非缓冲方式。需发送指令复位。初始化结束，芯片就绪
	jmp short $+2
	out	0xA1,al		; 送从芯片ICW4命令字。8086模式，普通EOI、非缓冲方式。需发送指令复位。初始化结束，芯片就绪
	jmp short $+2
	
	mov	al,0xFF		; mask off all interrupts for now
	out	0x21,al		; 屏蔽主芯片所有中断请求
	jmp short $+2
	out	0xA1,al		; 屏蔽从芯片所有中断请求
; well, that certainly wasn't fun :-(. Hopefully it works, and we don't
; need no steenking BIOS anyway (except for the initial loading :-).
; The BIOS-routine wants lots of unnecessary data, and it's less
; "interesting" anyway. This is how REAL programmers do it.
;
; Well, now's the time to actually move into protected mode. To make
; things as simple as possible, we do no register set-up or anything,
; we let the gnu-compiled 32-bit programs do that. We just jump to
; absolute address 0x00000, in 32-bit protected mode.
	mov	ax,0x0001	; protected mode (PE) bit
	;lmsw	ax		; This is it! 操作制寄存器 CR0，其比特位 0 置 1 将使 CPU 切换到保护模式，并且运行在特权级0，即当前特权级 CPL = 0  Intel手册上建议：lmsw 指令仅用于兼容以前的 286 CPU 所以使用以下指令进入保护模式
	mov eax, cr0		;
	or eax, 1
	mov cr0, eax
	;jmpi	0,8		; jmp offset 0 of segment 8 (cs) 受CS 段描述符高速缓存器中实模式残留内容的影响 在设置CR0寄存器比特位后，随后一条指令必须是一条段间跳转指令，用于刷新CPU当前指令队列

	jmp 8:0			; 段间跳转 cs:8 ip:0       "8"是保护模式下的段选择子 0x90306
; This routine checks that the keyboard command queue is empty
; No timeout is used - if this hangs there is something wrong with
; the machine, and we probably could not proceed anyway.
empty_8042:
	jmp short $+2
	in al, 0x64	; 8042 status port 读端口0x64就是读8042的状态寄存器（一个8bit的只读寄存器），bit_1为1时表示输入缓冲器满，为0时表示输入缓冲器空。要向8042写命令（通过0x64端口写入），必须当输入缓冲器为空时才可以
	test al, 2		; is input buffer full?
	jnz empty_8042	; yes - loop
	ret
;变量:
;---------------------------------------------------------------------------------------
gdt:
	dw	0,0,0,0		; dummy

	dw	0x07FF		; 8Mb - limit=2047 (2048*4096=8Mb)
	dw	0x0000		; base address=0
	dw	0x9A00		; code read/exec
	dw	0x00C0		; granularity=4096, 386

	dw	0x07FF		; 8Mb - limit=2047 (2048*4096=8Mb)
	dw	0x0000		; base address=0
	dw	0x9200		; data read/write
	dw	0x00C0		; granularity=4096, 386
	
idt_48:		dw	0
		dw	0,0
gdt_48:		dw	0x800		;界限值 0x800 = 2048b 每个描述符占8b 所以 可描述2048 / 8 = 256个描述符
		dw 	512+gdt,0x9 ;setup.asm 被加载到0x90200地址处, 所以基地址 = （0x90000 + gdt）
;---------------------------------------------------------------------------------------	
;字符串:
;---------------------------------------------------------------------------------------

;---------------------------------------------------------------------------------------
