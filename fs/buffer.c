/*
 *  linux/fs/buffer.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 *  'buffer.c' implements the buffer-cache functions. Race-conditions have
 * been avoided by NEVER letting a interrupt change a buffer (except for the
 * data, of course), but instead letting the caller do it. NOTE! As interrupts
 * can wake up a caller, some cli-sti sequences are needed to check for
 * sleep-on-calls. These should be extremely quick, though (I hope).
 */

/*
 * NOTE! There is one discordant note here: checking floppies for
 * disk change. This is where it fits best, I think, as it should
 * invalidate changed floppy-disk-caches.
 */

#include <stdarg.h>
 
#include <linux/config.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/system.h>
#include <asm/io.h>

extern int end;
extern void put_super(int);
extern void invalidate_inodes(int);

// end 是内核模块连接期间由链接程序ld设置的一个外部变量，指向内核模块末端（data_start+datasize+bss_size）
struct buffer_head * start_buffer = (struct buffer_head *) &end;
struct buffer_head * hash_table[NR_HASH];		//为了快速找而有效的找到判断出请求数据块是否已经被纯如缓冲区中，这里使用具有307个buffer_head指针项的散列数组表 使用b_prev b_next组成的双向链表维护各个节点之间的关系
static struct buffer_head * free_list;
static struct task_struct * buffer_wait = NULL;
int NR_BUFFERS = 0;								// 系统含有缓冲块个数
// dev、block 按位异或除以hash因子NR_HASH然后取模 从而实现高速查找的目的
#define _hashfn(dev, block)((unsigned)(dev^block)%NR_HASH)
#define hash(dev, block) hash_table[_hashfn(dev, block)]

static inline void wait_on_buffer(struct buffer_head * bh){
	cli();
	while(bh->b_lock)		// 如果该块被其他进程上锁，则当前程序进入睡眠
		sleep_on(&bh->b_wait);
	sti();
}

int sys_sync(void)
{
	int i;
	struct buffer_head * bh;

	sync_inodes();		/* write out inodes into buffers */
	bh = start_buffer;
	for (i=0 ; i<NR_BUFFERS ; i++,bh++) {
		wait_on_buffer(bh);
		if (bh->b_dirt)
			ll_rw_block(WRITE,bh);
	}
	
	return 0;
}

// 释放指定设备下的所有缓存数据
void inline invalidate_buffers(int dev)
{
	int i;
	struct buffer_head * bh;

	bh = start_buffer;
	for (i=0 ; i<NR_BUFFERS ; i++,bh++) {
		if (bh->b_dev != dev)
			continue;
		wait_on_buffer(bh);
		if (bh->b_dev == dev)
			bh->b_uptodate = bh->b_dirt = 0;
	}
}

/*
 * This routine checks whether a floppy has been changed, and
 * invalidates all buffer-cache-entries in that case. This
 * is a relatively slow routine, so we have to try to minimize using
 * it. Thus it is called only upon a 'mount' or 'open'. This
 * is the best way of combining speed and utility, I think.
 * People changing diskettes in the middle of an operation deserve
 * to loose :-)
 *
 * NOTE! Although currently this is only for floppies, the idea is
 * that any additional removable block-device will use this routine,
 * and that mount/open needn't know that floppies/whatever are
 * special.

 * 该子程序检查一个软盘是否已被更换，如果已经更换就使用高速中与该软驱对应的所有缓冲区无效。该子程序相对来说较慢，所以我们要尽量
 * 少使用它。所以只有在执行‘mount’和‘open’时才调用它。我向这是将速度和实用性相结合的最好方法，若在操作过程中更换软盘，就会导致数据丢失
 * 这是咎由自取
 * 注意：尽管子系统只用于软盘，以后任何可移介质的块设备都将使用该程序，mount/open操作不需要知道是软盘还是其他什么特殊介质
 */
 // 检查磁盘是否更换，如果已更换就使对应高速缓冲区无效
void check_disk_change(int dev)
{
	int i;

	if (MAJOR(dev) != 2)		// 只支持软盘可移动介质
		return;
	if (!floppy_change(dev & 0x03))
		return;
	for (i=0 ; i<NR_SUPER ; i++)
		if (super_block[i].s_dev == dev)
			put_super(super_block[i].s_dev);
	invalidate_inodes(dev);
	invalidate_buffers(dev);
}

// 执行写盘操作
int sync_dev(int dev)
{
	int i;
	struct buffer_head * bh;
	bh = start_buffer;		//bh指向缓冲区开始处
	for(i=0;i<NR_BUFFERS;i++,bh++){		// 遍历所有缓冲块
		if(bh->b_dev != dev)			// 略去不是当前设备的缓冲块
			continue;
		wait_on_buffer(bh);				// 等待缓冲区解锁
		if(bh->b_dev == dev && bh->b_dirt)
			ll_rw_block(WRITE, bh);
	}
	
	sync_inodes();
	// 这里执行两次同步是为了提高内核的自行效率第一次是为了清除内核中许多的“脏块”使i节点同步高效执行，第二遍是为了因i节点的同步操作而变脏的块与设备中数据同步
	bh = start_buffer;		//bh指向缓冲区开始处
	for(i=0;i<NR_BUFFERS;i++,bh++){
		if(bh->b_dev != dev)			// 略去不是当前设备的缓冲块
			continue;
		wait_on_buffer(bh);				// 等待缓冲区解锁
		if(bh->b_dev == dev && bh->b_dirt)
			ll_rw_block(WRITE, bh);
	}
	return 0;
}

static struct buffer_head * find_buffer(int dev, int block){		
	struct buffer_head * tmp;

	for (tmp = hash(dev,block) ; tmp != NULL ; tmp = tmp->b_next)
		if (tmp->b_dev==dev && tmp->b_blocknr==block)
			return tmp;
	return NULL;
}
// 从hash队列和空闲队列中移走缓冲块
// hash队列是一个双向链表 空闲队列是一个双向循环链表
static inline void remove_from_queues(struct buffer_head * bh)
{
/* remove from hash-queue */
	if (bh->b_next)
		bh->b_next->b_prev = bh->b_prev;
	if (bh->b_prev)
		bh->b_prev->b_next = bh->b_next;
	if (hash(bh->b_dev,bh->b_blocknr) == bh) // 如果该块是头，则让hash表的对应项指向本队列中的下一个缓冲区
		hash(bh->b_dev,bh->b_blocknr) = bh->b_next;
/* remove from free list */
	if (!(bh->b_prev_free) || !(bh->b_next_free))
		panic("Free block list corrupted");
	bh->b_prev_free->b_next_free = bh->b_next_free;
	bh->b_next_free->b_prev_free = bh->b_prev_free;
	if (free_list == bh)		// 如果空闲链表的头指向本缓冲块，则让其指向下一个缓冲块
		free_list = bh->b_next_free;
}
// 将缓冲块插入到hash队列和空闲队列中
static inline void insert_into_queues(struct buffer_head * bh)
{
/* put at end of free list */
	bh->b_next_free = free_list;
	bh->b_prev_free = free_list->b_prev_free;
	free_list->b_prev_free->b_next_free = bh;
	free_list->b_prev_free = bh;
/* put the buffer in new hash-queue if it has a device */
	bh->b_prev = NULL;
	bh->b_next = NULL;
	if (!bh->b_dev)
		return;
	bh->b_next = hash(bh->b_dev,bh->b_blocknr);
	hash(bh->b_dev,bh->b_blocknr) = bh;
	if(bh->b_next)
		bh->b_next->b_prev = bh;
}

/*
 * Why like this, I hear you say... The reason is race-conditions.
 * As we don't lock buffers (unless we are readint them, that is),
 * something might happen to it while we sleep (ie a read-error
 * will force it bad). This shouldn't really happen currently, but
 * the code is ready.
 */
 struct buffer_head * get_hash_table(int dev, int block){
	struct buffer_head * bh;
	for(;;){
		// 在高速缓冲中寻找指定设备和指定块的缓冲区块，如果没有找到直接返回NULL，退出
		if(!(bh = find_buffer(dev, block)))
			return NULL;
		//如果找到了 则对引用次数加1，并等待该缓冲区解锁（上锁情况下）。
		bh->b_count++;
		wait_on_buffer(bh);
		// 由于经过了睡眠无法确定是否有进程对该块进行了修改所以再检查一次。如果还是原来的块内容则直接返回否则 放弃该块从新循环查找一边，不要忘了把加上去的count减下来
		if(bh->b_dev == dev && bh->b_blocknr == block)
			return bh;
		bh->b_count--;
	}

 }

/*
 * Ok, this is getblk, and it isn't very clear, again to hinder
 * race-conditions. Most of the code is seldom used, (ie repeating),
 * so it should be much more efficient than it looks.
 *
 * The algoritm is changed: hopefully better, and an elusive bug removed.
 */
// 宏返回一个字节 高位表示缓冲块是否被修改， 低位表示是否上锁
#define BADNESS(bh) (((bh)->b_dirt<<1) + (bh)->b_lock)
/*
 * 检查指定缓冲区的块号和设备号是否已经在高速缓冲区中，如果在则返回对应缓冲区的头指针
 * 如果不在则设置一个对应设备号和块号的新项并换回对应缓冲区头指针
 *
 */
struct buffer_head * getblk(int dev, int block){
	struct buffer_head * tmp, * bh;
	//第一步搜索hash表，如果指定块已经在高速缓冲中，则返回对应缓冲区头指针，退出
repeat:
	if(bh = get_hash_table(dev, block))
		return bh;
	
	tmp = free_list;
	// 第二步如果没有从哈希表中找到说明该块没有加入到缓冲区，则对空闲链表进行扫描，寻找一个空闲缓冲区
	// 在寻找时还要根据修改标志和锁定标志的权值找到一个最合适的空闲块
	do{
		if(tmp->b_count)// 不考虑正在被使用中的空闲块
			continue;
		if(!bh || BADNESS(tmp)<BADNESS(bh)){	// 寻找一个权值最小的块
			bh = tmp;
			if(!BADNESS(tmp))
				break;
		}
	}while((tmp = tmp->b_next_free) != free_list);
	if(!bh) {									// 没有找到空闲缓冲块
		sleep_on(&buffer_wait);
		goto repeat;
	}

	wait_on_buffer(bh);				// 该块是否被其他程序上锁，若上锁则休眠
	if(bh->b_count)					// 如果此时检查到有进程正则使用此块则返回重新查找（由于有进程的切换期间有可能此块已经被使用）
		goto repeat;
		
	while (bh->b_dirt) {			// 如果检测到该缓冲区的数据被修改过则将数据持久化
		sync_dev(bh->b_dev);		//如果数据已修改将数据写盘
		wait_on_buffer(bh);
		if(bh->b_count)
			goto repeat;
	}
/* NOTE!! While we slept waiting for this block, somebody else might */
/* already have added "this" block to the cache. check it */
	if(find_buffer(dev,block))// 当进程睡眠时可能有其他进程已经将该缓冲块加入到高数缓冲中
		goto repeat;
/* OK, FINALLY we know that this buffer is the only one of it's kind, */
/* and that it's unused (b_count=0), unlocked (b_lock=0), and clean */	
	// 至此我们得到了一块新的缓冲块
	bh->b_count = 1;
	bh->b_dirt = 0;
	bh->b_uptodate = 0;
	remove_from_queues(bh);
	bh->b_dev = dev;
	bh->b_blocknr = block;
	insert_into_queues(bh);
	return bh;
}	
// 释放指定缓冲区
// 等待缓冲区解锁，然后引用计数器减1，并明确唤醒等待空闲缓冲块的进程
void brelse(struct buffer_head* buf) {
	if(!buf)
		return;
	wait_on_buffer(buf);
	if (!(buf->b_count--))
		panic("Trying to free free buffer");
	wake_up(&buffer_wait);
}

/*
 * bread() reads a specified block and returns the buffer that contains
 * it. It returns NULL if the block was unreadable.
 *
 * 从设备上读取指定的数据块并返回含有数据的缓冲区， 如果指定的块不存在则返回空
 */
struct buffer_head * bread(int dev, int block) {
	int i;
	struct buffer_head * bh;
	if (!(bh = getblk(dev, block)))
		panic("brea: greblk returned NULL\n");
	if (bh->b_uptodate)
		return bh;
	ll_rw_block(READ,bh);			// 读扇区数据
	wait_on_buffer(bh);
	if(bh->b_uptodate)		//如果缓冲区已更新，则返回缓冲区头指针，退出
		return bh;
	brelse(bh);				// 否则说明读错误 释放换从去 返回NULL
	return NULL;
}

#define COPYBLK(from,to) \
__asm__("cld\n\t" \
	"rep\n\t" \
	"movsl\n\t" \
	::"c" (BLOCK_SIZE/4),"S" (from),"D" (to) \
	)

/*
 * bread_page reads four buffers into memory at the desired address. It's
 * a function of its own, as there is some speed to be got by reading them
 * all at the same time, not waiting for one to be read, and then another
 * etc.
 */

void bread_page(unsigned long address,int dev,int b[4])
{
	struct buffer_head * bh[4];
	int i;

	for (i=0 ; i<4 ; i++)
		if (b[i]) {
			if ((bh[i] = getblk(dev,b[i])))
				if (!bh[i]->b_uptodate)
					ll_rw_block(READ,bh[i]);
		} else
			bh[i] = NULL;
	for (i=0 ; i<4 ; i++,address += BLOCK_SIZE)
		if (bh[i]) {
			wait_on_buffer(bh[i]);
			if (bh[i]->b_uptodate)
				COPYBLK((unsigned long) bh[i]->b_data,address);
			brelse(bh[i]);
		}
}

/*
 * Ok, breada can be used as bread, but additionally to mark other
 * blocks for reading as well. End the argument list with a negative
 * number.
 */

// 从指定设备读取指定的一些块
// 函数参数可变，是一系列指定的块号。成功时返回第1块的缓冲块头指针，否则返回NULL
struct buffer_head * breada(int dev,int first, ...)
{
	va_list args;
	struct buffer_head * bh, *tmp;

	va_start(args,first);
	if (!(bh=getblk(dev,first)))
		panic("bread: getblk returned NULL\n");
	if (!bh->b_uptodate)
		ll_rw_block(READ,bh);
	while ((first=va_arg(args,int))>=0) {
		tmp=getblk(dev,first);
		if (tmp) {
			if (!tmp->b_uptodate)
				ll_rw_block(READA,tmp);
			tmp->b_count--;				// 因为预读模块只是将数据读入高数换从区并不是马上使用，由于getblk会增加引用次数，所以此时将引用次数减1
		}
	}
	va_end(args);
	wait_on_buffer(bh);
	if (bh->b_uptodate)
		return bh;
	brelse(bh);
	return (NULL);
}


void buffer_init(long buffer_end)
{	//高速缓冲区域从内核模块末尾到距离起始内存区4m的一块区域
	//初始化缓冲区 
	struct buffer_head * h = start_buffer;		// 将buffer_head的结构指针h指向高速缓冲的起始位置
	void * b;
	int i;

	if (buffer_end == 1<<20)	//缓冲区末端在1M处
		b = (void *) (640*1024);
	else
		b = (void *) buffer_end;
	// 以下为循环设置缓冲块的对照关系
	// 程序从两端开始分别设置缓冲块头结构和划分出对应的缓冲块
	while ( (b -= BLOCK_SIZE) >= ((void *) (h+1)) ) {		// 是否还有足够的BLOCK_SIZE+sizeof(h)空间
		h->b_dev = 0;						// 数据源的设备号（0=free）
		h->b_dirt = 0;
		h->b_count = 0;
		h->b_lock = 0;
		h->b_uptodate = 0;
		h->b_wait = NULL;
		h->b_next = NULL;
		h->b_prev = NULL;
		h->b_data = (char *) b;				// 指向缓冲块首地址
		h->b_prev_free = h-1;				// 指向上一个空闲缓冲块
		h->b_next_free = h+1;				// 指向下一个空闲缓冲块
		h++;
		NR_BUFFERS++;						// 记录块数量
		if (b == (void *) 0x100000)
			b = (void *) 0xA0000;
	}
	h--;
	free_list = start_buffer;			// 空闲块头结构指向第一个缓冲头结构
	free_list->b_prev_free = h;			// 空闲块头结构第一个节点的上一个节点指向最后一个节点
	h->b_next_free = free_list;			// 空闲块头指针最后一个节点的下一个节点指向第一个节点                            此时所有缓冲头节点链表形成一个双向闭环                    并且free_list指向第一个节点
	for (i=0;i<NR_HASH;i++)
		hash_table[i]=NULL;
}	
