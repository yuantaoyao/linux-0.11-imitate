/*
 *  linux/fs/fcntl.c
 *
 *  (C) 1991  Linus Torvalds
 */

/* #include <string.h> */
#include <errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/segment.h>

#include <fcntl.h>
#include <sys/stat.h>

extern int sys_close();

// 复制文件句柄（文件描述符）
// fd 预复制的文件句柄、arg 指定新文件句柄的最小数值
static int dupfd(unsigned int fd, unsigned int arg){
	if(fd >= NR_OPEN || !current->filp[fd])	// 如果句柄值大于系统中允许打开的文件数，或者该句柄的文件结构不存在，则返回出错码并退出(其实文件句柄就是进程文件结构指针数组的索引号)
		return -EBADF;
	if(arg >= NR_OPEN)
		return -EINVAL;
	while(arg < NR_OPEN)	// 在当前进程数组中寻找等于或大于arg，并且没有使用的项
		if(current->filp[arg])
			arg++;
		else
			break;
	if(arg >= NR_OPEN)	// 如果找的句柄值大于系统允许打开的最大文件数，则返回出错码并退出
		return -EMFILE;
	current->close_on_exec &= ~(1<<arg);// 在执行时关闭标志位图close_on_exec中复位该句柄位。即在运行exec()函数时，不会关闭dup()创建的句柄
	(current->filp[arg] = current->filp[fd])->f_count++;// 令该文件结构指针等于原句柄fd的指针，并将文件引用计数增1
	return arg;	//返回新句柄
}

int sys_dup2(unsigned int oldfd, unsigned int newfd)
{
	sys_close(newfd);
	return dupfd(oldfd, newfd);
}

int sys_dup(unsigned int fildes)
{
	return dupfd(fildes, 0);;
}
// 文件控制系统调用函数
// fd 文件句柄 cmd 控制命令 arg 对不同的控制命令有不同的涵义
int sys_fcntl(unsigned int fd, unsigned int cmd, unsigned long arg)
{	
	struct file * filp;
	if (fd>=NR_OPEN || !(filp = current->filp[fd]))
		return -EBADF;
	switch (cmd){
		case F_DUPFD: 			// 复制文件句柄
			return dupfd(fd, arg);
		case F_GETFD:			// 取文件句柄的执行时关闭标志
			return (current->close_on_exec>>fd)&1;
		case F_SETFD:			// 设置执行时关闭标志。arg位0置位是设置，否则关闭
			if(arg&1)
				current->close_on_exec |= (1<<fd);
			else
				current->close_on_exec &= ~(1<<fd);
			return 0;
		case F_GETFL:			// 取文件状态标志和访问模式
			return filp->f_flags;
		case F_SETFL:			// 设置文件状态和访问模式(根据arg设置添加、非阻塞模式)
			filp->f_flags &= ~(O_APPEND | O_NONBLOCK);
			filp->f_flags |= arg & (O_APPEND | O_NONBLOCK);
			return 0;
		case F_GETLK:
		case F_SETLK:
		case F_SETLKW:
			return -1;
		default:
			return -1;
	}
	return -1;
}
