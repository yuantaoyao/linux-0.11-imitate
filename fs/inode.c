/*
 *  linux/fs/inode.c
 *
 *  (C) 1991  Linus Torvalds
 */

#include <string.h> 
#include <sys/stat.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <asm/system.h>

struct m_inode inode_table[NR_INODE]={{0,},};

static void read_inode(struct m_inode * inode);
static void write_inode(struct m_inode * inode);
// 此处的处理与缓冲块的处理基本一致
static inline void wait_on_inode(struct m_inode * inode)
{
	cli();
	while (inode->i_lock)
		sleep_on(&inode->i_wait);
	sti();
}

static inline void lock_inode(struct m_inode * inode)
{
	cli();
	while (inode->i_lock)
		sleep_on(&inode->i_wait);
	inode->i_lock=1;
	sti();
}

static inline void unlock_inode(struct m_inode * inode)
{
	inode->i_lock=0;
	wake_up(&inode->i_wait);
}
// 释放设备dev子内存i节点表中的所有i节点
void invalidate_inodes(int dev)
{
	int i;
	struct m_inode * inode;							// 定义指向文件元数据指针

	inode = 0+inode_table;							// 指向i节点表指针数组首项
	for(i=0 ; i<NR_INODE ; i++,inode++) {
		wait_on_inode(inode);
		if (inode->i_dev == dev) {
			if (inode->i_count)						// 若引用不为0则警告
				printk("inode in use on removed disk\n\r");
			inode->i_dev = inode->i_dirt = 0;		// 释放i节点（置设备号为0）
		}
	}
}


void sync_inodes(void){
	int i;
	struct m_inode * inode;
	inode = 0 + inode_table;
	for (i=0;i<NR_INODE;i++, inode++){
		wait_on_inode(inode);
		if (inode->i_dirt && !inode->i_pipe)	// 该节点已经修改且不是管道节点
			write_inode(inode);					// 写入缓冲区
	}
	
}

static int _bmap(struct m_inode * inode,int block,int create)
{
	struct buffer_head * bh;
	int i;

	if (block<0)
		panic("_bmap: block<0");
	if (block >= 7+512+512*512)
		panic("_bmap: block>big");
	if (block<7) {
		if (create && !inode->i_zone[block])
			if ((inode->i_zone[block]=new_block(inode->i_dev))) {
				inode->i_ctime=CURRENT_TIME;
				inode->i_dirt=1;
			}
		return inode->i_zone[block];
	}
	block -= 7;
	if (block<512) {
		if (create && !inode->i_zone[7])
			if ((inode->i_zone[7]=new_block(inode->i_dev))) {
				inode->i_dirt=1;
				inode->i_ctime=CURRENT_TIME;
			}
		if (!inode->i_zone[7])
			return 0;
		if (!(bh = bread(inode->i_dev,inode->i_zone[7])))
			return 0;
		i = ((unsigned short *) (bh->b_data))[block];
		if (create && !i)
			if ((i=new_block(inode->i_dev))) {
				((unsigned short *) (bh->b_data))[block]=i;
				bh->b_dirt=1;
			}
		brelse(bh);
		return i;
	}
	block -= 512;
	if (create && !inode->i_zone[8])
		if ((inode->i_zone[8]=new_block(inode->i_dev))) {
			inode->i_dirt=1;
			inode->i_ctime=CURRENT_TIME;
		}
	if (!inode->i_zone[8])
		return 0;
	if (!(bh=bread(inode->i_dev,inode->i_zone[8])))
		return 0;
	i = ((unsigned short *)bh->b_data)[block>>9];
	if (create && !i)
		if ((i=new_block(inode->i_dev))) {
			((unsigned short *) (bh->b_data))[block>>9]=i;
			bh->b_dirt=1;
		}
	brelse(bh);
	if (!i)
		return 0;
	if (!(bh=bread(inode->i_dev,i)))
		return 0;
	i = ((unsigned short *)bh->b_data)[block&511];
	if (create && !i)
		if ((i=new_block(inode->i_dev))) {
			((unsigned short *) (bh->b_data))[block&511]=i;
			bh->b_dirt=1;
		}
	brelse(bh);
	return i;
}

int bmap(struct m_inode * inode,int block)
{
	return _bmap(inode,block,0);
}

int create_block(struct m_inode * inode, int block)
{
	return _bmap(inode,block,1);
}
// 放置一个i节点（回写入设备）。
void iput(struct m_inode * inode)
{
	if (!inode)	/// 判断有效性
		return;
	wait_on_inode(inode);	// 等待解锁
	if (!inode->i_count)
		panic("iput: trying to free free inode");
	if (inode->i_pipe) {	// 如果是个管道i节点
		wake_up(&inode->i_wait);	// 唤醒等地啊的进程
		if (--inode->i_count)	// 将引用递减
			return;				// 如果还有其他进程引用则退出
		free_page(inode->i_size);	// 否则释放管道占用的内存页， （对于管道节点i_size存放着内存页地址）
		inode->i_count=0;
		inode->i_dirt=0;
		inode->i_pipe=0;
		return;
	}
	if (!inode->i_dev) {	// 如果节点的设备号为0，则将该节点引用减1并返回。（用于管道操作的接地，设备号为0）
		inode->i_count--;
		return;
	}
	if (S_ISBLK(inode->i_mode)) {	// 判断是否是块设备
		sync_dev(inode->i_zone[0]);
		wait_on_inode(inode);
	}
repeat:
	if (inode->i_count>1) {		// 如果i节点的引用数大于1
		inode->i_count--;
		return;
	}
	if (!inode->i_nlinks) {		// 如果i节点的文件目录链接数为0说明文件已经被删除，则是方法i节点对应的所有逻辑块和i节点
		truncate(inode);
		free_inode(inode);
		return;
	}
	if (inode->i_dirt) {
		write_inode(inode);	/* we can sleep - so do again */
		wait_on_inode(inode);
		goto repeat;
	}
	inode->i_count--;
	return;
}


struct m_inode * get_empty_inode(void)
{
	struct m_inode * inode;
	static struct m_inode * last_inode = inode_table;
	int i;

	do {
		inode = NULL;
		for (i = NR_INODE; i ; i--) {
			if (++last_inode >= inode_table + NR_INODE)
				last_inode = inode_table;
			if (!last_inode->i_count) {
				inode = last_inode;
				if (!inode->i_dirt && !inode->i_lock)
					break;
			}
		}
		if (!inode) {
			for (i=0 ; i<NR_INODE ; i++)
				printk("%04x: %6d\t",inode_table[i].i_dev,
					inode_table[i].i_num);
			panic("No free inodes in mem");
		}
		wait_on_inode(inode);
		while (inode->i_dirt) {
			write_inode(inode);
			wait_on_inode(inode);
		}
	} while (inode->i_count);
	memset(inode,0,sizeof(*inode));
	inode->i_count = 1;
	return inode;
}
// 获取管道节点
struct m_inode * get_pipe_inode(void){
	struct m_inode * inode;
	if(!(inode = get_empty_inode()))						// 没有获取到空节点退出
		return NULL;
	if(!(inode->i_size = get_free_page())){					// 将申请的缓冲区地址赋给i_size
		inode->i_count = 0;
		return NULL;
	}
	inode->i_count = 2;				// 将i节点的引用计数设为2 读/写
	PIPE_HEAD(*inode) = PIPE_TAIL(*inode) = 0;			// 复位管道头尾
	inode->i_pipe = 1;									// 置节点为管道使用标记
	return inode;
}
// 获取一个i节点
struct m_inode * iget(int dev,int nr)
{
	struct m_inode * inode, * empty;

	if (!dev)
		panic("iget with dev==0");
	empty = get_empty_inode();
	inode = inode_table;
	while (inode < NR_INODE+inode_table) {
		if (inode->i_dev != dev || inode->i_num != nr) {
			inode++;
			continue;
		}
		wait_on_inode(inode);
		if (inode->i_dev != dev || inode->i_num != nr) {
			inode = inode_table;
			continue;
		}
		inode->i_count++;
		if (inode->i_mount) {
			int i;

			for (i = 0 ; i<NR_SUPER ; i++)
				if (super_block[i].s_imount==inode)
					break;
			if (i >= NR_SUPER) {
				printk("Mounted inode hasn't got sb\n");
				if (empty)
					iput(empty);
				return inode;
			}
			iput(inode);
			dev = super_block[i].s_dev;
			nr = ROOT_INO;
			inode = inode_table;
			continue;
		}
		if (empty)
			iput(empty);
		return inode;
	}
	if (!empty)
		return (NULL);
	inode=empty;
	inode->i_dev = dev;
	inode->i_num = nr;
	read_inode(inode);
	return inode;
}


static void read_inode(struct m_inode * inode)
{
	struct super_block * sb;
	struct buffer_head * bh;
	int block;

	lock_inode(inode);
	if (!(sb=get_super(inode->i_dev)))
		panic("trying to read inode without dev");
	block = 2 + sb->s_imap_blocks + sb->s_zmap_blocks +
		(inode->i_num-1)/INODES_PER_BLOCK;
	if (!(bh=bread(inode->i_dev,block)))
		panic("unable to read i-node block");
	*(struct d_inode *)inode =
		((struct d_inode *)bh->b_data)
			[(inode->i_num-1)%INODES_PER_BLOCK];
	brelse(bh);
	unlock_inode(inode);
}
// 将i节点信息写入缓冲区中 待缓冲区刷新时会写入盘中
static void write_inode(struct m_inode * inode)
{
	struct super_block * sb;
	struct buffer_head * bh;
	int block;

	lock_inode(inode);
	if (!inode->i_dirt || !inode->i_dev) {  // 如果该节点 没有被修改过，或者该节点所在的设备没有被使用，则不必写回数据
		unlock_inode(inode);
		return;
	}
	if (!(sb=get_super(inode->i_dev)))		//获取该缓冲区所在设备的超级块
		panic("trying to write inode without device");
	// i节点所在的设备逻辑块号 = （启动块+超级快）+ i节点位图所占用的块数 + 逻辑块位图所在的块数 + （i节点号-1）/每块含有的i节点数
	// 此处取出的缓冲块中的内容为文件元数据信息
	block = 2 + sb->s_imap_blocks + sb->s_zmap_blocks +
		(inode->i_num-1)/INODES_PER_BLOCK;
	if (!(bh=bread(inode->i_dev,block)))
		panic("unable to read i-node block");
	
	// 将该i节点信息复制到逻辑块对应该i节点的向位置处
	((struct d_inode *)bh->b_data)[(inode->i_num-1)%INODES_PER_BLOCK] =	*(struct d_inode *)inode;
	bh->b_dirt=1;
	inode->i_dirt=0;
	brelse(bh);
	unlock_inode(inode);
}

