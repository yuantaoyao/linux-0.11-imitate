/*
 *  linux/fs/ioctl.c
 *
 *  (C) 1991  Linus Torvalds
 */

/* #include <string.h>*/
#include <errno.h>
#include <sys/stat.h>

#include <linux/sched.h>

extern int tty_ioctl(int dev, int cmd, int arg);				// 终端设备处理函数

typedef int (*ioctl_ptr) (int dev, int cmd, int arg);			// 定义一个指向设备处理函数的指针

#define NRDEVS ((sizeof (ioctl_table))/(sizeof(ioctl_ptr)))		// 系统中设备的数量

static ioctl_ptr ioctl_table[]={
	NULL,		/* nodev */
	NULL,		/* /dev/mem */
	NULL,		/* /dev/fd */
	NULL,		/* /dev/hd */
	tty_ioctl,	/* /dev/ttyx */
	tty_ioctl,	/* /dev/tty */
	NULL,		/* /dev/lp */
	NULL		/* named pipes */
};
// fd 为设备索引（文件描述符）
int sys_ioctl(unsigned int fd, unsigned int cmd, unsigned long arg)			// 定义一个系统调用函数，用户态可以通过系统调用的方式控制设备
{	
	struct file * filp;						// 系统中一切皆文件各种设备也不例外，这里定义一个文件结构体用户句柄与设备文件之间的联系
	int dev, mode;

	if (fd >= NR_OPEN || !(filp = current->filp[fd]))
		return -EBADF;
	mode=filp->f_inode->i_mode;
	if (!S_ISCHR(mode) && !S_ISBLK(mode))
		return -EINVAL;
	dev = filp->f_inode->i_zone[0];
	if (MAJOR(dev) >= NRDEVS)
		return -ENODEV;
	if (!ioctl_table[MAJOR(dev)])
		return -ENOTTY;
	return ioctl_table[MAJOR(dev)](dev,cmd,arg);
}
