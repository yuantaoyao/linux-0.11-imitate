/*
 *  linux/fs/open.c
 *
 *  (C) 1991  Linus Torvalds
 */

/* #include <string.h> */
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <utime.h>
#include <sys/stat.h>

#include <linux/sched.h>
#include <linux/tty.h>
#include <linux/kernel.h>
#include <asm/segment.h>
// 读取文件系统信息
int sys_ustat(int dev, struct ustat * ubuf){
	return -ENOSYS;			// 功能未实现
}
// 设置文件访问和修改时间
// 如果times为空则取系统当前时间设置文件访问时间和修改时间
int sys_utime(char * filename, struct utimbuf * times){
	struct m_inode * inode;
	long actime, modtime;
	//根据文件名取得对应的i节点
	if(!(inode = namei(filename)))
		return -ENOENT;
	if(times){
		actime = get_fs_long((unsigned long *) &times->actime);
		modtime = get_fs_long((unsigned long *) &times->modtime);
	}else
		actime = modtime = CURRENT_TIME;

	inode->i_atime = actime;
	inode->i_mtime = modtime;
	inode->i_dirt = 1;
	iput(inode);			// 将i节点回写回设备
	return 0;
}

/*
 * XXX should we use the real or effective uid?  BSD uses the real uid,
 * so as to make this call useful to setuid programs.
 */
// 检查文件的访问权限， mode 为检查的访问属性，它有三个有效比特位 R_OK(4)W_OK(2)X_OK(1)F_OK(0) 可读、可写、可执行、是否存在
// 如果访问允许的话返回0否则返回出错码
int sys_access(const char * filename,int mode){
	struct m_inode * inode;
	int res, i_mode;
	mode &= 0007;
	if(!(inode = namei(filename)))
		return -EACCES;
	i_mode = res = inode->i_mode & 0777;
	iput(inode);
	if(current->uid = inode->i_uid)
		res >>= 6;
	else if(current->gid == inode->i_gid)
		res>>= 6;
	if((res & 0007 & mode) == mode)
		return 0;
	/*
	 * XXX we are doing this test last because we really should be
	 * swapping the effective with the real user id (temporarily),
	 * and then calling suser() routine.  If we do call the
	 * suser() routine, it needs to be called last. 
	 */
	// 如果当前用户ID为0（超级用户）并且屏蔽码执行位是0或者文件可以被任何人执行，搜索，则返回0
	if ((!current->uid) &&
	    (!(mode & 1) || (i_mode & 0111)))
		return 0;
	return -EACCES;
	return -1;
}
// 改变当前工作目录系统调用
int sys_chdir(const char * filename){
	struct m_inode * inode;
	if(!(inode = namei(filename)))
		return -ENOENT;
	if(!S_ISDIR(inode->i_mode)){
		iput(inode);
		return -ENOTDIR;
	}
	// 释放原工作目录i节点，并使其指向新的工作目录
	iput(current->pwd);
	current->pwd = inode;
	return (0);
}
// 改变根目录系统调用
int sys_chroot(const char * filename){
	struct m_inode * inode;
	if(!(inode = namei(filename)))
		return -ENOENT;
	if(!S_ISDIR(inode->i_mode)){
		iput(inode);
		return -ENOTDIR;
	}
	// 释放原工作目录i节点，并使其指向新的工作目录
	iput(current->root);
	current->root = inode;
	return (0);
}
// 修改文件属性系统调用
int sys_chmod(const char * filename,int mode){
	struct m_inode * inode;
	if(!(inode = namei(filename)))
		return -ENOENT;
	if((current->euid != inode->i_uid) && !suser()){
		iput(inode);
		return -EACCES;
	}
	inode->i_mode = (mode & 07777) | (inode->i_mode & ~07777);
	inode->i_dirt = 1;
	iput(inode);
	return 0;
}
// 修改文件所有者系统调用
int sys_chown(const char * filename,int uid,int gid){
	struct m_inode * inode;
	if(!(inode = namei(filename)))
		return -ENOENT;
	if(!suser()){
		iput(inode);
		return -EACCES;
	}
	inode->i_uid = uid;
	inode->i_gid = gid;
	inode->i_dirt = 1;
	iput(inode);
	return 0;

	return -1;
}
// 打开或创建文件系统调用
int sys_open(const char * filename,int flag,int mode){
	struct m_inode * inode;
	struct file * f;
	int i, fd;
	
	mode &=0777 & ~current->umask;
	for(fd=0;fd<NR_OPEN;fd++)
		if(!current->filp[fd])
			break;
	if(fd>=NR_OPEN)
		return -EINVAL;
	
	current->close_on_exec &= ~(1<<fd);
	f = 0+file_table;
	for(i=0;i<NR_FILE;i++,f++)
		if(!f->f_count) break;
	if(i>=NR_FILE)
		return -EINVAL;
	(current->filp[fd]=f)->f_count++;
	if((i=open_namei(filename,flag,mode,&inode))<0){
		current->filp[fd] = NULL;
		f->f_count = 0;
		return i;
	}
	if(S_ISCHR(inode->i_mode))
		if(MAJOR(inode->i_zone[0]) == 4) {		// 如果打开的是字符设备文件（/dev/tty0）
			if(current->leader && current->tty<0){	// 如果当前进程是进程组首领并且当且进程没有终端
				current->tty = MINOR(inode->i_zone[0]);		// 则设置当前进程的tty为该i节点的子设备号
				tty_table[current->tty].pgrp = current->pgrp;	// 并设置当前进程tty对应的tty表项的父进程组号等于当前进程的进程组号。表示为该进程组（会话期）分配控制终端
			}
		}else if(MAJOR(inode->i_zone[0]) == 5)	// 对于主设备号为5的字符文件（/dev/tty）
			if(current->tty<0) {				// 若当前进程没有tty则出错
				iput(inode);					// 于是放回i节点和申请到的文件结构并返回错误码
				current->filp[fd] = NULL;
				f->f_count = 0;
				return -EPERM;
			}
	if(S_ISBLK(inode->i_mode))			// 对于块设备文件需要检查盘片是否被更换
		check_disk_change(inode->i_zone[0]);
	f->f_mode = inode->i_mode;
	f->f_flags = flag;
	f->f_count = 1;
	f->f_inode = inode;
	f->f_pos = 0;
	
	return (fd);
}
// 创建文件系统调用
int sys_creat(const char * pathname, int mode){
	return sys_open(pathname, O_CREAT|O_TRUNC, mode);
}
// 关闭文件系统调用
int sys_close(unsigned int fd){
	struct file * filp;
	if(fd >= NR_OPEN)
		return -EINVAL;
	current->close_on_exec &= ~(1<<fd);
	if(!(filp = current->filp[fd]))
		return -EINVAL;
	current->filp[fd] = NULL;
	if(filp->f_count == 0)
		panic("Close: file count is 0");
	if(--filp->f_count)
		return (0);
	iput(filp->f_inode);
	return (0);
}

