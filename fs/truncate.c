/*
 *  linux/fs/truncate.c
 *
 *  (C) 1991  Linus Torvalds
 */

#include <linux/sched.h>

#include <sys/stat.h>
/**
 * i节点包括7个直接快，1个一次间接块，1个二次间接块
 * 一共占有9 + 1024 + 1024* 1024个逻辑块
 *
**/

// 释放所有一次间接块
static void free_ind(int dev,int block)
{
	struct buffer_head * bh;
	unsigned short * p;
	int i;

	if (!block)
		return;
	if ((bh=bread(dev,block))) {
		p = (unsigned short *) bh->b_data;
		for (i=0;i<512;i++,p++)		// 每块占1024个字节，shout长度为2所以循环1024/2=512次
			if (*p)
				free_block(dev,*p);		// 释放逻辑块
		brelse(bh);						// 释放缓冲块
	}
	free_block(dev,block);
}
// 释放所有二次间接块
static void free_dind(int dev,int block)
{
	struct buffer_head * bh;
	unsigned short * p;
	int i;

	if (!block)
		return;
	if ((bh=bread(dev,block))) {
		p = (unsigned short *) bh->b_data;
		for (i=0;i<512;i++,p++)
			if (*p)
				free_ind(dev,*p);
		brelse(bh);
	}
	free_block(dev,block);
}
// 释放inode上的所有逻辑块空间
void truncate(struct m_inode * inode)
{
	int i;

	if (!(S_ISREG(inode->i_mode) || S_ISDIR(inode->i_mode)))
		return;
	for (i=0;i<7;i++)			// i节点上一共有7个直接逻辑块，将这7个直接逻辑块项置零，函数free_block用于释放指定设备指定逻辑块的磁盘块
		if (inode->i_zone[i]) {
			free_block(inode->i_dev,inode->i_zone[i]);
			inode->i_zone[i]=0;
		}
	free_ind(inode->i_dev,inode->i_zone[7]);	// 索引7为一级间接块
	free_dind(inode->i_dev,inode->i_zone[8]);	// 索引8为二级间接块
	inode->i_zone[7] = inode->i_zone[8] = 0;
	inode->i_size = 0;
	inode->i_dirt = 1;
	inode->i_mtime = inode->i_ctime = CURRENT_TIME;
}


