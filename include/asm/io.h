/*
 *	该头文件定义了对硬件IO访问的嵌入式汇编宏函数
 *	前两个宏与后两个宏的区别是 后两个宏使用了cpu等待延迟
 */

// 向端口port写入value值 注意寄存器的位数 
#define outb(value,port) \
__asm__ ("outb %%al,%%dx"::"a" (value),"d" (port));

// 从端口port中读取值 返回的寄存器为半个字节
#define inb(port) ({ \
unsigned char _v; \
__asm__ volatile ("inb %%dx,%%al":"=a" (_v):"d" (port)); \
_v; \
})
// 向端口port写入value值	多了一个短跳转 没有操作意义 只是为了cpu暂停一下
#define outb_p(value,port) \
__asm__ ("outb %%al,%%dx\n\t" \
		"jmp 1f\n\t" \
		"1:jmp 1f\n" \
		"1:"::"a" (value),"d" (port));

// 从端口port中读取值	多了一个短跳转 没有操作意义 只是为了cpu暂停一下
#define inb_p(port) ({ \
unsigned char _v; \
__asm__ volatile ("inb %%dx,%%al\n\t" \
	"jmp 1f\n\t" \
	"1:jmp 1f\n\t" \
	"1:":"=a" (_v):"d" (port)); \
_v; \
})

