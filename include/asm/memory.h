/*
 *  NOTE!!! memcpy(dest,src,n) assumes ds=es=normal data segment. This
 *  goes for all kernel functions (ds=es=kernel space, fs=local data,
 *  gs=null), as well as for all well-behaving user programs (ds=es=
 *  user data space). This is NOT a bug, as any user program that changes
 *  es deserves to die if it isn't careful.

 *  注意!!!memcpy(dest,src,n)假设段寄存器ds=es=通常数据段。内核中使用的所有函数都基于这个假设（ds=es=内核空间，fs=局部数据空间，gs=null），具有良好行为的
 *  应用程序也是这样（ds=es=用户数据空间）。如果任何用户层序随意改动了es寄存器而出错，并不是由于系统程序错误造成的
 *
 */
// src源地址    dest目标地址 n复制字节个数
#define memcpy(dest,src,n)	({ \
void * _res = dest; \
__asm__ ("cld;rep;movsb" \
	::"D"((long)(_res)), "S" ((long)(src)), "c" ((long) (n)) \
	); \
_res; \
})


