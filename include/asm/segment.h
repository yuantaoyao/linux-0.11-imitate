static inline unsigned char get_fs_byte(const char * addr)		// 从内存中读取一个字节
{
	unsigned register char _v;

	__asm__ ("movb %%fs:%1,%0":"=r" (_v):"m" (*addr));
	return _v;
}

static inline unsigned short get_fs_word(const unsigned short *addr)// 从内存中读取一个字
{
	unsigned short _v;

	__asm__ ("movw %%fs:%1,%0":"=r" (_v):"m" (*addr));
	return _v;
}

static inline unsigned long get_fs_long(const unsigned long *addr)// 从内存中读取一个4个字节
{
	unsigned long _v;

	__asm__ ("movl %%fs:%1,%0":"=r" (_v):"m" (*addr)); \
	return _v;
}

static inline void put_fs_byte(char val,char *addr)// 保存一个字节到内存
{
__asm__ ("movb %0,%%fs:%1"::"q" (val),"m" (*addr));
}

static inline void put_fs_word(short val,short * addr)// 保存一个字到内存
{
__asm__ ("movw %0,%%fs:%1"::"q" (val),"m" (*addr));
}

static inline void put_fs_long(unsigned long val,unsigned long * addr)// 保存一个4个字节到内存
{
__asm__ ("movl %0,%%fs:%1"::"q" (val),"m" (*addr));
}

/*
 * Someone who knows GNU asm better than I should double check the followig.
 * It seems to work, but I don't know if I'm doing something subtly wrong.
 * --- TYT, 11/24/91
 * [ nothing wrong here, Linus ]
 */

static inline unsigned long get_fs() // 取fs段寄存器的值
{
	unsigned short _v;
	__asm__("mov %%fs,%%ax":"=a" (_v):);
	return _v;
}

static inline unsigned long get_ds() 	// 取ds段寄存器的值
{
	unsigned short _v;
	__asm__("mov %%ds,%%ax":"=a" (_v):);
	return _v;
}

static inline void set_fs(unsigned long val) // 设置 fs段寄存器的值
{
	__asm__("mov %0,%%fs"::"a" ((unsigned short) val));
}


