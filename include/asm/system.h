/*
 * 从特权级0代码移动到特权级3的代码中运行。所使用的方法是模拟中断调用返回过程，即利用iret指令来实现特权级变更和堆栈的切换
 * 这种方法进行控制权的转移是由CPU保护机制造成的，cpu允许低特权级通过调用门，陷阱门，来调用或转移到高级别代码中，反之不允许，因此模拟iret返回低级别代码方法
 * 1、选择子 0x17 = 0x00010111即以RPL=3选择LDT的第3项
 * 2、0x0f = 0x00001111段选择子表示以RPL=3去选择LDT里面的第2项。第一项为空，第二项为进程0代码段，第3项为进程0的数据段
 *
 */
#define move_to_user_mode() \
__asm__ ("movl %%esp,%%eax\n\t" \
	"pushl $0x17\n\t" \
	"pushl %%eax\n\t" \
	"pushfl\n\t" \
	"pushl $0x0f\n\t" \
	"pushl $1f\n\t" \
	"iret\n" \
	"1:\tmovl $0x17,%%eax\n\t" \
	"movw %%ax,%%ds\n\t" \
	"movw %%ax,%%es\n\t" \
	"movw %%ax,%%fs\n\t" \
	"movw %%ax,%%gs" \
	:::"ax")

#define sti() __asm__ ("sti"::)
#define cli() __asm__ ("cli"::)
#define nop() __asm__ ("nop"::)

#define iret() __asm__ ("iret"::)

/*
 * 根据参数中的中断或异常处理地址addr、门描述符类型type和特权级dpl设置位于gate_addr处的门描述符
 * 
 * 1、将偏移地址低字与选择符组合成描述符低4字节（eax）
 * 2、将类型标志字与偏移高字组合成描述符高4字节（ebx）
 * 3、分别设置门描述符的低4字节和高4字节
 */
#define _set_gate(gate_addr,type,dpl,addr) \
__asm__ ("movw %%dx,%%ax\n\t" \
	"movw %0,%%dx\n\t" \
	"movl %%eax,%1\n\t" \
	"movl %%edx,%2" \
	: \
	: "i" ((short) (0x8000+(dpl<<13)+(type<<8))), \
	"o" (*((char *) (gate_addr))), \
	"o" (*(4+(char *) (gate_addr))), \
	"d" ((char *) (addr)),"a" (0x00080000))

#define set_intr_gate(n,addr) \
	_set_gate(&idt[n],14,0,addr);

#define set_trap_gate(n,addr) \
	_set_gate(&idt[n],15,0,addr);

#define set_system_gate(n,addr) \
	_set_gate(&idt[n],15,3,addr);

#define _set_seg_desc(gate_addr,type,dpl,base,limit) {\
	*(gate_addr) = ((base) & 0xff000000) | \
		(((base) & 0x00ff0000)>>16) | \
		((limit) & 0xf0000) | \
		((dpl)<<13) | \
		(0x00408000) | \
		((type)<<8); \
	*((gate_addr)+1) = (((base) & 0x0000ffff)<<16) | \
		((limit) & 0x0ffff); }
/*
 * 在局部表中设置任务状态段/局部表描述符，状态段与局部表段均被设置成104个字节
 * n - 全局描述符中该描述符的指针；addr - 描述符中段的基地址值；type - 描述符的类型 ：任务状态段描述符0x89、局部表段描述符0x82
 * 1、将tss(ldt)长度放入描述符长度域（第0-1字节）
 * 2、将要指向的地址低字放入描述符基地址（第2-3字节）
 * 3、将基地址高字右移循环进入ax中（低地址进入高字处）
 * 4、将高字中的低字节存入描述符第4字节
 * 5、将属性存入第5字节
 * 6、描述符第6字节置0 G D 0 AVL limit(4位)
 * 7、将高字中的高字节存入第7字节
 */
#define _set_tssldt_desc(n,addr,type) \
__asm__ ("movw $104,%1\n\t" \
	"movw %%ax,%2\n\t" \
	"rorl $16,%%eax\n\t" \
	"movb %%al,%3\n\t" \
	"movb $" type ",%4\n\t" \
	"movb $0x00,%5\n\t" \
	"movb %%ah,%6\n\t" \
	"rorl $16,%%eax" \
	::"a" (addr), "m" (*(n)), "m" (*(n+2)), "m" (*(n+4)), \
	 "m" (*(n+5)), "m" (*(n+6)), "m" (*(n+7)) \
	)
// 在全局变量表中设置任务状态段描述符
#define set_tss_desc(n,addr) _set_tssldt_desc(((char *) (n)),((addr)),"0x89")
// 在全局变量表中设置局部表描述符
#define set_ldt_desc(n,addr) _set_tssldt_desc(((char *) (n)),((addr)),"0x82")

