/*
 * This file has definitions for some important file table
 * structures etc.
 */

#ifndef _FS_H
#define _FS_H

#include <sys/types.h>

/* devices are as follows: (same as minix, so we can use the minix
 * file system. These are major numbers.)
 *
 * 0 - unused (nodev)
 * 1 - /dev/mem
 * 2 - /dev/fd
 * 3 - /dev/hd													此处使用了minix文件系统 可见当年linus编写linux操作系统是通过学习了minix操作系统
 * 4 - /dev/ttyx
 * 5 - /dev/tty
 * 6 - /dev/lp
 * 7 - unnamed pipes
 */

#define IS_SEEKABLE(x) ((x)>=1 && (x)<=3)

#define READ 0
#define WRITE 1
// 预读写
#define READA 2		/* read-ahead - don't pause */
#define WRITEA 3	/* "write-ahead" - silly, but somewhat useful */

// 主设备号和此设备号低8位是次设备号 高8位位主设备号
#define MAJOR(a) (((unsigned)(a))>>8)
#define MINOR(a) ((a)&0xff)
// 文件名长度
#define NAME_LEN 14
// 文件系统的根inode节点
#define ROOT_INO 1
// 块位图和inode位图占据的最大硬盘块数
#define I_MAP_SLOTS 8
#define Z_MAP_SLOTS 8
// 超级块的魔数，说明是有效的超级快
#define SUPER_MAGIC 0x137F

// 一个进程打开文件数量大小
#define NR_OPEN 20
// 系统同时打开inode数大小，即系统同时只能打开32个文件
#define NR_INODE 32
// file结构体，进程间共享
#define NR_FILE 64
// 超级快 即文件系统的个数
#define NR_SUPER 8
// 高速缓冲中项的块数
#define NR_HASH 307
// 缓存文件系统数据的buffer个数，操作系统启动的时候初始化该变量
#define NR_BUFFERS nr_buffers
// 一块缓冲区对应的字节数
#define BLOCK_SIZE 1024
// 2>>10即字节数 用于计算
#define BLOCK_SIZE_BITS 10
#ifndef NULL
#define NULL ((void *) 0)
#endif
// 每个硬盘块有几个inode节点，即块大小除以每个inode机构大小，硬盘是d_inode，内存是m_inode
#define INODES_PER_BLOCK ((BLOCK_SIZE)/(sizeof (struct d_inode)))
// 每个硬盘块包含的目录项数
#define DIR_ENTRIES_PER_BLOCK ((BLOCK_SIZE)/(sizeof (struct dir_entry)))

#define PIPE_HEAD(inode) ((inode).i_zone[0])// 管道头
#define PIPE_TAIL(inode) ((inode).i_zone[1])	// 管道尾
#define PIPE_SIZE(inode) ((PIPE_HEAD(inode)-PIPE_TAIL(inode))&(PAGE_SIZE-1))// 管道长度
#define PIPE_EMPTY(inode) (PIPE_HEAD(inode)==PIPE_TAIL(inode))	// 管道是否为空
#define PIPE_FULL(inode) (PIPE_SIZE(inode)==(PAGE_SIZE-1))		// 管道满
#define INC_PIPE(head) \
__asm__("incl %0\n\tandl $4095,%0"::"m" (head))

typedef char buffer_block[BLOCK_SIZE];

//管理文件系统数据缓存的结构
struct buffer_head {
	char * b_data;					// 指针
	unsigned long b_blocknr;		// 块号
	unsigned short b_dev;			// 数据源的设备号
	unsigned char b_uptodate;		// 更新标记，数据是否已更新
	unsigned char b_dirt;			// 修改标记， 0未修改1已修改
	unsigned char b_count;			// 使用的用户数
	unsigned char b_lock;			// 缓冲区是否被锁 主要用于blk_drv/ll_rw_block.c更新缓冲块中数据时锁定缓冲块
	struct task_struct * b_wait;	// 指向等待该缓冲区解锁的任务
	struct buffer_head * b_prev;	// hash 队列上前一块
	struct buffer_head * b_next;	// hash 队列上后一块
	struct buffer_head * b_prev_free;	// 空闲表上前一块
	struct buffer_head * b_next_free;	// 空闲表上后一块
};
// 文件系统在硬盘里的inode节点
struct d_inode { 
	unsigned short i_mode;		// 各种标记位读写执行等 ls命令输出的内容
	unsigned short i_uid;
	unsigned long i_size;
	unsigned long i_time;
	unsigned char i_gid;
	unsigned char i_nlinks;		// 文件入度即有多少个文件指向它
	unsigned short i_zone[9];	// 存储文件内容对应的硬盘块号
};

// 这是在内存中的i 节点结构。前7 项与d_inode 完全一样。每个文件对应一个inode
// 记录了文件的元数据unix/linux一切皆文件的思想与此有关
struct m_inode {		// 文件相关的属性信息
	unsigned short i_mode;	// 文件类型属性（rwx位）
	unsigned short i_uid;	// 用户id(文件拥有者标识符)
	unsigned long i_size;	// 文件大小
	unsigned long i_mtime;	// 修改时间（自1970.1.1.0 算起 秒）
	unsigned char i_gid;	// 组id（文件拥有者所在的组）
	unsigned char i_nlinks;	// 文件目录项连接数
	unsigned short i_zone[9];	// 直接（0-6）、间接（7）、或双重间接（8）逻辑块
/* these are in memory also */
	struct task_struct * i_wait;	// 等待该i节点的进程
	unsigned long i_atime;		// 最后访问时间
	unsigned long i_ctime;		// i节点自身修改时间
	unsigned short i_dev;		// i节点所在设备号
	unsigned short i_num;		// i节点号
	unsigned short i_count;		// i节点被使用的次数，0表示该节点空闲
	unsigned char i_lock;		// 锁定标志
	unsigned char i_dirt;		// 已修改（脏）标记
	unsigned char i_pipe;		// 管道标记
	unsigned char i_mount;		// 安装标志
	unsigned char i_seek;		// 收寻标志
	unsigned char i_update;		// 更新标志
};

// 文件结构（用于在文件句柄与i 节点之间建立关系）
// 在不同的进程下文件展现的方式是不一样的，对于inode而言在任何地方都是相同的存储文件的元数据，
// 但在不同的进程中又有各自的特性 比如文件的偏移 操作方式等
struct file {
	unsigned short f_mode;		// 文件操作模式（RW位）
	unsigned short f_flags;		// 文件打开或控制标志
	unsigned short f_count;		// 对应文件句柄（文件描述符数）
	struct m_inode * f_inode;	// 指向i节点
	off_t f_pos;				// 文件位置（读写偏移值）
};
// 超级块管理文件系统元数据的结构
struct super_block {
	unsigned short s_ninodes;			// inode节点个数
	unsigned short s_nzones;			// 数据块占据的逻辑块总块数
	unsigned short s_imap_blocks;		// inode和数据块位图占据的硬盘数据块，位图记录着哪个块或者inode节点被使用了
	unsigned short s_zmap_blocks;
	/*
		第一块在硬盘的块号，一个硬盘可以有几个文件系统，
 		每个文件系统占据一部分，所以要记录开始的块号和总块数
	*/
	unsigned short s_firstdatazone;
	/*
		用于计算文件系统块等于多少个硬盘块，硬盘块大小乘以2
		的s_log_zone_size次方等于文件系统的块大小（硬盘块的
		大小和文件系统块的大小不是一回事，比如硬盘块的大小是1kb，
		文件系统是4kb）
	*/ 
	unsigned short s_log_zone_size;
	unsigned long s_max_size;				// 文件名最大字节数
	unsigned short s_magic;					// 魔数
/* These are only in memory */
	struct buffer_head * s_imap[8];			// 缓存inode 位图内容
	struct buffer_head * s_zmap[8];			// 缓存数据库位图内容
	unsigned short s_dev;					// 设备号
	struct m_inode * s_isup;				// 挂载到哪个文件的inode下
	struct m_inode * s_imount;
	unsigned long s_time;
	struct task_struct * s_wait;			// 等待使用该超级块的队列
	unsigned char s_lock;					// 互斥访问变量
	unsigned char s_rd_only;				// 文件系统是否只读不能写
	unsigned char s_dirt;					// 是否需要回写到硬盘
};
// 超级块在硬盘的结构
struct d_super_block {
	unsigned short s_ninodes;				// inode 节点数量
	unsigned short s_nzones;				// 硬盘块数量
	unsigned short s_imap_blocks;			// inode位图数量
	unsigned short s_zmap_blocks;			// 数据块位图数量
	unsigned short s_firstdatazone;			// 文件系统第一块块号，不是数据块第一块块号
	unsigned short s_log_zone_size;
	unsigned long s_max_size;				// 最大文件长度
	unsigned short s_magic;					// 判断是否是超级块
};

// 文件目录结构
struct dir_entry {
	unsigned short inode;	// 节点
	char name[NAME_LEN];	// 文件名
};

extern struct m_inode inode_table[NR_INODE];
extern struct file file_table[NR_FILE];
extern struct super_block super_block[NR_SUPER];
extern struct buffer_head * start_buffer;
extern int nr_buffers;

extern void check_disk_change(int dev);
extern int floppy_change(unsigned int nr);
extern int ticks_to_floppy_on(unsigned int dev);
extern void floppy_on(unsigned int dev);
extern void floppy_off(unsigned int dev);
extern void truncate(struct m_inode * inode);
extern void sync_inodes(void);
extern void wait_on(struct m_inode * inode);
extern int bmap(struct m_inode * inode,int block);
extern int create_block(struct m_inode * inode,int block);
extern struct m_inode * namei(const char * pathname);
extern int open_namei(const char * pathname, int flag, int mode,
	struct m_inode ** res_inode);
extern void iput(struct m_inode * inode);
extern struct m_inode * iget(int dev,int nr);
extern struct m_inode * get_empty_inode(void);
extern struct m_inode * get_pipe_inode(void);
extern struct buffer_head * get_hash_table(int dev, int block);
extern struct buffer_head * getblk(int dev, int block);
extern void ll_rw_block(int rw, struct buffer_head * bh);
extern void brelse(struct buffer_head * buf);
extern struct buffer_head * bread(int dev,int block);
extern void bread_page(unsigned long addr,int dev,int b[4]);
extern struct buffer_head * breada(int dev,int block,...);
extern int new_block(int dev);
extern void free_block(int dev, int block);
extern struct m_inode * new_inode(int dev);
extern void free_inode(struct m_inode * inode);
extern int sync_dev(int dev);
extern struct super_block * get_super(int dev);
extern int ROOT_DEV;
#endif

