#ifndef _SCHED_H
#define _SCHED_H

#define NR_TASKS 64				// linux 内核最多允许有64个任务
#define HZ 100

#define FIRST_TASK task[0]
#define LAST_TASK task[NR_TASKS-1]

#include <linux/head.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <signal.h>

#if (NR_OPEN > 32)
#error "Currently the close-on-exec-flags are in one word, max 32 files/proc"
#endif

#define TASK_RUNNING		0				// 就绪态
#define TASK_INTERRUPTIBLE	1				// 可中断
#define TASK_UNINTERRUPTIBLE	2			// 不可中断
#define TASK_ZOMBIE		3					// 挂起
#define TASK_STOPPED		4				// 已结束

#ifndef NULL
#define NULL ((void *) 0)
#endif


extern void trap_init(void);
#ifndef PANIC
volatile void panic(const char * str);
#endif

extern int copy_page_tables(unsigned long from, unsigned long to, long size);
extern int free_page_tables(unsigned long from, unsigned long size);

extern void sched_init(void);
extern void schedule(void);
extern void trap_init(void);
#ifndef PANIC
volatile void panic(const char * str);
#endif
extern int tty_write(unsigned minor,char * buf,int count);


typedef int (*fn_ptr)();


struct i387_struct {
	long	cwd;
	long	swd;
	long	twd;
	long	fip;
	long	fcs;
	long	foo;
	long	fos;
	long	st_space[20];	/* 8*10 bytes for each FP-reg = 80 bytes */
};

struct tss_struct {      // 任务切换时现场记录结构体 此结构体中记录着一条任务失去时间片时运行的现场环境数据，以便该任务获取时间片时恢复运行
	long	back_link;	/* 16 high bits zero */ // 任务链接域，前一个任务的tss描述符的gdt选择子
	long	esp0;
	long	ss0;		/* 16 high bits zero */
	long	esp1;
	long	ss1;		/* 16 high bits zero */
	long	esp2;
	long	ss2;		/* 16 high bits zero */
	long	cr3;
	long	eip;
	long	eflags;
	long	eax,ecx,edx,ebx;
	long	esp;
	long	ebp;
	long	esi;
	long	edi;
	long	es;		/* 16 high bits zero */
	long	cs;		/* 16 high bits zero */
	long	ss;		/* 16 high bits zero */
	long	ds;		/* 16 high bits zero */
	long	fs;		/* 16 high bits zero */
	long	gs;		/* 16 high bits zero */
	long	ldt;		/* 16 high bits zero */ //当前任务的ldt描述符的gdt选择子
	long	trace_bitmap;	/* bits: trace 0, bitmap 16-31 */ // 栈中的数据
	struct i387_struct i387;
};


struct task_struct {// 此结构体保存着进程的所有信息
/* these are hardcoded - don't touch */
	long state;	/* -1 unrunnable, 0 runnable, >0 stopped */
	long counter; // 进程可运行的时间量(时间片)
	long priority; // 任务的优先级 越大运行时间越长
	long signal;	// 信号位图，每个bit代表一个信号， 信号值=位偏移值+1
	struct sigaction sigaction[32];	// 信号执行属性结构，对应信号将要执行的操作和标志信息
	long blocked;	/* bitmap of masked signals */ // 进程信号屏蔽位 对应位图
/* various fields */
	int exit_code;		// 任务执行停止的退出码其父进程会取
	unsigned long start_code,end_code,end_data,brk,start_stack;// 代码段地址、代码长度（字节数）、代码长度+数据长度（字节数）、总长度、堆栈地址
	long pid, father, pgrp, session,leader;//进程标识号、父进程号、组进程号、会话号、会话首领
	unsigned short uid, euid, suid;	// 用户标识号（用户ID）、有效用户ID、保存的用户ID
	unsigned short gid, egid, sgid;	// 组标识号（组ID）、有效组ID、保存的组ID
	long alarm;					// 报警定时器
	long utime, stime, cutime, cstime,start_time;	// 用户态运行时间、系统态运行时间、子进程用户态运行时间、子进程系统态运行时间、进程开始运行时刻
	unsigned short used_math;	// 是否使用了协处理器
/* file system info */
	int tty; /* -1 if no tty, so it must be signed */ // 进程使用tty的子设备号
	unsigned short umask;			// 文件创建属性屏蔽位
	struct m_inode * pwd;			// 当前工作目录i节点结构
	struct m_inode * root;			// 根目录的i节点结构
	struct m_inode * executable;	// 执行文件i节点结构
	unsigned long close_on_exec;	// 执行时关闭文件句柄位图
	struct file * filp[NR_OPEN];	// 进程使用的文件表结构
/* ldt for this task 0 - zero 1 - cs 2 - ds&ss */
	struct desc_struct ldt[3];		// 本任务的局部变量表0-空 1-代码段 2-数据段
/* tss for this task */
	struct tss_struct tss;			// 本任务的任务状态段信息结构
};

/*
 *  INIT_TASK is used to set up the first task table, touch at
 * your own risk!. Base=0, limit=0x9ffff (=640kB)
 * 初始化第一个任务实例
 */
#define INIT_TASK \
/* state etc */	{ 0,15,15, \
/* signals */	0,{{},},0, \
/* ec,brk... */	0,0,0,0,0,0, \
/* pid etc.. */	0,-1,0,0,0, \
/* uid etc */	0,0,0,0,0,0, \
/* alarm */	0,0,0,0,0,0, \
/* math */	0, \
/* fs info */	-1,0022,NULL,NULL,NULL,0, \
/* filp */	{NULL,}, \
	{ \
		{0,0}, \
/* ldt */	{0x9f,0xc0fa00}, \
		{0x9f,0xc0f200}, \
	}, \
/*tss*/	{0,PAGE_SIZE+(long)&init_task,0x10,0,0,0,0,(long)&pg_dir,\
	 0,0,0,0,0,0,0,0, \
	 0,0,0x17,0x17,0x17,0x17,0x17,0x17, \
	 _LDT(0),0x80000000, \
		{} \
	}, \
}

extern long startup_time;
extern struct task_struct *task[NR_TASKS];
extern struct task_struct *last_task_used_math;
extern struct task_struct *current;
extern long volatile jiffies;
extern long startup_time;

#define CURRENT_TIME (startup_time+jiffies/HZ)

extern void add_timer(long jiffies, void (*fn)(void));
extern void sleep_on(struct task_struct ** p);
extern void interruptible_sleep_on(struct task_struct ** p);
extern void wake_up(struct task_struct ** p);


/*
 * Entry into gdt where to find first TSS. 0-nul, 1-cs, 2-ds, 3-syscall
 * 4-TSS0, 5-LDT0, 6-TSS1 etc ...
 */
#define FIRST_TSS_ENTRY 4
#define FIRST_LDT_ENTRY (FIRST_TSS_ENTRY+1)

// 宏定义计算全局表中第n个任务的tss段描述符的选择符值（偏移量）
// 因每个描述符占8字节因此用FIRST_TSS_ENTRY << 3表示该描述符在GDT表中的起始偏移位置
// 因为每个任务使用1个tss和1个ldt 共占16字节，因此需要n << 4来表示对应的TSS位置
#define _TSS(n) ((((unsigned long) n)<<4)+(FIRST_TSS_ENTRY<<3))
#define _LDT(n) ((((unsigned long) n)<<4)+(FIRST_LDT_ENTRY<<3))
#define ltr(n) __asm__("ltr %%ax"::"a" (_TSS(n)))
#define lldt(n) __asm__("lldt %%ax"::"a" (_LDT(n)))
#define str(n) \
__asm__("str %%ax\n\t" \
	"subl %2,%%eax\n\t" \
	"shrl $4,%%eax" \
	:"=a" (n) \
	:"a" (0),"i" (FIRST_TSS_ENTRY<<3))


/*
 *	switch_to(n) should switch tasks to task nr n, first
 * checking that n isn't the current task, in which case it does nothing.
 * This also clears the TS-flag if the task we switched to has used
 * tha math co-processor latest.
 *
 * swuitch_to(n) 将切换当前任务切换到任务nr即n。首先检测任务n是不是当前任务，
 * 如果是则什么也不做如果我们切换到的任务最近（上次运行）使用过数学协处理器的话，则还需要复位控制寄存器cr0中的TS标志
 * 输入：%0-指向tmp.a %2-指向tmp.b  edx-任务n的tss的段选择符 ecx-任务n的结构指针
 * 1、 任务是当前任务吗？test[n] == current ?
 * 2、如果是则退出
 * 3、将新任务TSS的16位选择符存入tmp.b
 * 4、交换两个操作数的值 将current指向将要执行的进程，将ecx指向当前进程 从而实现了进程切换
 * 5、ljmp %0 此处执行了一个长跳转（far jmp） 长跳转指令需要6字节操作数作为跳转目的地址的长指针(tmp.a 4字节，tmp.b 2字节 
 *	内存中操作数的顺序正好相反则位 b:a	即b的低2字节:a的4字节 由于第3步把tss段选择符地址存在b的低2字节，所以正好跳转到新的tss处)
 *	当跳转到一个新的任务TSS段选择符组成的地址处会照成CPU进行任务切换
 * 6、任务切换回来 判断上次任务是否使用了协处理器
 * 7、如果没有则退出
 * 8、如果使用了则清cr0中的任务切换标志TS
 */

#define switch_to(n){	\
struct {long a,b} __tmp;	\
	__asm__("cmpl %%ecx, current\n\t"	\
		"je 1f\n\t"					\
		"movw %%dx, %1\n\t"			\
		"xchgl %%ecx, current\n\t"	\
		"ljmp %0\n\t"				\
		"cmpl %%ecx, last_task_used_math\n\t"	\
		"jne 1f\n\t"	\
		"clts\n\t"	\
		"1:"	\
		::"m" (*&__tmp.a), "m" (*&__tmp.b),		\
		"d" (_TSS(n)), "c" ((long) task[n]));	\
}
#define PAGE_ALIGN(n) (((n)+0xfff)&0xfffff000)

#define _set_base(addr,base)  \
__asm__ ("push %%edx\n\t" \
	"movw %%dx,%0\n\t" \
	"rorl $16,%%edx\n\t" \
	"movb %%dl,%1\n\t" \
	"movb %%dh,%2\n\t" \
	"pop %%edx" \
	::"m" (*((addr)+2)), \
	 "m" (*((addr)+4)), \
	 "m" (*((addr)+7)), \
	 "d" (base) \
	)

#define _set_limit(addr,limit) \
__asm__ ("push %%edx\n\t" \
	"movw %%dx,%0\n\t" \
	"rorl $16,%%edx\n\t" \
	"movb %1,%%dh\n\t" \
	"andb $0xf0,%%dh\n\t" \
	"orb %%dh,%%dl\n\t" \
	"movb %%dl,%1\n\t" \
	"pop %%edx" \
	::"m" (*(addr)), \
	 "m" (*((addr)+6)), \
	 "d" (limit) \
	)

#define set_base(ldt,base) _set_base( ((char *)&(ldt)) , (base) )
#define set_limit(ldt,limit) _set_limit( ((char *)&(ldt)) , (limit-1)>>12 )

#define get_base(ldt) _get_base(((char *)&(ldt)))

static inline unsigned long _get_base(char * addr)	//获取描述符基址
{
         unsigned long __base;
         __asm__("movb %3,%%dh\n\t"
                 "movb %2,%%dl\n\t"
                 "shll $16,%%edx\n\t"
                 "movw %1,%%dx"
                 :"=&d" (__base)
                 :"m" (*((addr)+2)),
                  "m" (*((addr)+4)),
                  "m" (*((addr)+7)));
         return __base;
}

// lsll 是加载段界限的指令 即把segment段描述符中断的段界限字段装入某个寄存器
#define get_limit(segment) ({			\
unsigned long __limit;					\
__asm__ ("lsll %1,%0\n\t"				\
		"incl %0"						\
		:"=r" (__limit)					\
		:"r" (segment));					\
__limit;})

#endif
