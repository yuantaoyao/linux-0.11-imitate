/*
 * 'tty.h' defines some structures used by tty_io.c and some defines.
 *
 * NOTE! Don't touch this without checking that nothing in rs_io.s or
 * con_io.s breaks. Some constants are hardwired into the system (mainly
 * offsets into 'tty_queue'
 */

#ifndef _TTY_H
#define _TTY_H

#include <termios.h>

#define TTY_BUF_SIZE 1024	// tty 缓冲区（缓冲队列）大小

struct tty_queue{		//tty 字符缓冲队列数据结构，用于tty_struct结构中读、写和辅助缓冲队列
	unsigned long data;	// 队列缓冲区含有字符行数值（不是当前字符数） 对于串口终端则存放串口地址
	unsigned long head;	// 缓冲区中数据头指针
	unsigned long tail;	// 缓冲区中数据尾指针
	struct task_struct * proc_list;	// 等待进程列表
	char buf[TTY_BUF_SIZE];	// 队列缓冲区
};

#define INC(a) ((a) = ((a)+1) & (TTY_BUF_SIZE-1))			// a+1 & 11 1111 1111 缓冲区a指针向前移动1位 如果超过了缓冲区大小则指针循环
#define DEC(a) ((a) = ((a)-1) & (TTY_BUF_SIZE-1))			// 缓冲区指针后退一针 如果超过了缓冲区大小 指针同样循环
#define EMPTY(a) ((a).head == (a).tail)						// 清空缓冲区
#define LEFT(a) (((a).tail-(a).head-1)&(TTY_BUF_SIZE-1))	// 空闲缓冲区大小
#define LAST(a) ((a).buf[(TTY_BUF_SIZE-1)&((a).head-1)])	// 缓冲区数据段最后一个位置字符值
#define FULL(a) (!LEFT(a))									// 缓冲区是否满 1 满
#define CHARS(a) (((a).head-(a).tail)&(TTY_BUF_SIZE-1))		// 已用缓冲区大小
#define GETCH(queue,c) \
(void)({c=(queue).buf[(queue).tail];INC((queue).tail);})	// 从数据尾部取出一个数 并且尾指针+1
#define PUTCH(c,queue) \
(void)({(queue).buf[(queue).head]=(c);INC((queue).head);})	// 从队列头部放入一个数，并且头指针+1
// 判断终端键盘字符类型
#define INTR_CHAR(tty) ((tty)->termios.c_cc[VINTR])			// 发终端信号SIGNINT
#define QUIT_CHAR(tty) ((tty)->termios.c_cc[VQUIT])			// 发退出信号SIGQUIT
#define ERASE_CHAR(tty) ((tty)->termios.c_cc[VERASE])		// 擦除一个字符
#define KILL_CHAR(tty) ((tty)->termios.c_cc[VKILL])			// 擦除一行字符
#define EOF_CHAR(tty) ((tty)->termios.c_cc[VEOF])			// 文件结束符
#define START_CHAR(tty) ((tty)->termios.c_cc[VSTART])		// 恢复输出
#define STOP_CHAR(tty) ((tty)->termios.c_cc[VSTOP])			// 停止输出
#define SUSPEND_CHAR(tty) ((tty)->termios.c_cc[VSUSP])		// 发送挂起信号 SIGTSTP


// tty的结构体
struct tty_struct{
	struct termios termios;						// 终端IO属性和控制字符数据结构
	int pgrp;									// 所属进程组
	int stopped;								// 停止标记
	void (*write)(struct tty_struct * tty);		// tty 写函数指针
	struct tty_queue read_q;					// 读队列
	struct tty_queue write_q;					// 写队列
	struct tty_queue secondary;					// 辅助队列（存放规范模式字符序列）
};

extern struct tty_struct tty_table[];

/*	intr=^C	中断			quit=^|	退出			erase=del 删除			kill=^U 终止
	eof=^D	文件结束		vtime=\0	定时器		vmin=\1	定时器				sxtc=\0	交换字符
	start=^Q	开始		stop=^S	停止			susp=^Z	挂起				eol=\0	行结束
	reprint=^R	重显		discard=^U	丢弃		werase=^W	单词擦除		lnext=^V	下一行
	eol2=\0
*/

#define INIT_C_CC "\003\034\177\025\004\0\1\0\021\023\032\0\022\017\027\026\0"


void rs_init(void);
void con_init(void);
void tty_init(void);							// tty初始化

int tty_read(unsigned c, char * buf, int n);
int tty_write(unsigned c, char * buf, int n);

void con_write(struct tty_struct * tty);
void rs_write(struct tty_struct * tty);

void copy_to_cooked(struct tty_struct * tty);


#endif
