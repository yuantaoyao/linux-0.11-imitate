#ifndef _SIGNAL_H
#define _SIGNAL_H

#include <sys/types.h>

typedef int sig_atomic_t;
typedef unsigned int sigset_t;		/* 32 bits */

#define _NSIG             32
#define NSIG		_NSIG
// 以下是linux0.11定义的信号，其中包括POSIX.1要求的所有20个信号
#define SIGHUP		 1					// hand up --挂断控制终端或进程
#define SIGINT		 2					// interrupt --来自键盘中断
#define SIGQUIT		 3					// quit    --来自键盘退出
#define SIGILL		 4					// illeagle --非法指令
#define SIGTRAP		 5					// trap --跟踪断点
#define SIGABRT		 6					// abort --异常结束
#define SIGIOT		 6					// io trap
#define SIGUNUSED	 7					// unused --没有使用
#define SIGFPE		 8					// fpe --协处理器出错
#define SIGKILL		 9					// kill --强迫进程终止
#define SIGUSR1		10					// user1 --用户1进程可用
#define SIGSEGV		11					// segment Violation --无效内存引用
#define SIGUSR2		12					// user2 --用户信号2进程可用
#define SIGPIPE		13					// pipe --管道写出错，无读者
#define SIGALRM		14					// alerm --实时定时器报警
#define SIGTERM		15					// terminate --进程终止
#define SIGSTKFLT	16					// stack fault --栈出错
#define SIGCHLD		17					// child --子进程停止或终止
#define SIGCONT		18					// continue --恢复进程继续执行
#define SIGSTOP		19					// stop --停止进程执行
#define SIGTSTP		20					// tty stop -- tty发出停止进程，可忽略
#define SIGTTIN		21					// tty in --后台进程请求写入
#define SIGTTOU		22					// tty out --后台进程请求输出

/* Ok, I haven't implemented sigactions, but trying to keep headers POSIX */
#define SA_NOCLDSTOP	1					// 当子进程处于停止转台就不对SIGCHLD处理
#define SA_NOMASK	0x40000000				// 不阻止在指定信号处理程序中再收到该信号
#define SA_ONESHOT	0x80000000				// 信号句柄一旦被调过就恢复默认处理句柄

#define SIG_BLOCK          0	/* for blocking signals */
#define SIG_UNBLOCK        1	/* for unblocking signals */
#define SIG_SETMASK        2	/* for setting the signal mask */

#define SIG_DFL		((void (*)(int))0)	/* default signal handling */
#define SIG_IGN		((void (*)(int))1)	/* ignore signal */

struct sigaction {
	void (*sa_handler)(int);// 新的信号处理函数
	sigset_t sa_mask;		// 处理该信号时暂时将sa_mask搁置
	int sa_flags;			// 用来设置信号处理的其他相关操作
	void (*sa_restorer)(void);
};

#endif /* _SIGNAL_H */
