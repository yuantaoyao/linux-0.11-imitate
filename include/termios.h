/*
 * 该文档定义了一些对终端接口设置的函数原型
 * 主要定义异步通信口的终端接口
 */

#ifndef _TERMIOS_H
#define _TERMIOS_H

#define TTY_BUF_SIZE 1024

/* 0x54 is just a magic number to make these relatively uniqe ('T') */
// 以下名称TC*的含义是tty控制命令
#define TCGETS		0x5401
#define TCSETS		0x5402
#define TCSETSW		0x5403
#define TCSETSF		0x5404
#define TCGETA		0x5405
#define TCSETA		0x5406
#define TCSETAW		0x5407
#define TCSETAF		0x5408
#define TCSBRK		0x5409
#define TCXONC		0x540A
#define TCFLSH		0x540B
// 以下名称TIOC*的含义是tty输入输出控制命令
#define TIOCEXCL	0x540C
#define TIOCNXCL	0x540D
#define TIOCSCTTY	0x540E
#define TIOCGPGRP	0x540F
#define TIOCSPGRP	0x5410
#define TIOCOUTQ	0x5411
#define TIOCSTI		0x5412
#define TIOCGWINSZ	0x5413
#define TIOCSWINSZ	0x5414
#define TIOCMGET	0x5415
#define TIOCMBIS	0x5416
#define TIOCMBIC	0x5417
#define TIOCMSET	0x5418
#define TIOCGSOFTCAR	0x5419
#define TIOCSSOFTCAR	0x541A
#define TIOCINQ		0x541B

struct winsize {
	unsigned short ws_row;			// 窗口字符行数
	unsigned short ws_col;			// 窗口字符列数
	unsigned short ws_xpixel;		// 窗口宽度 像素
	unsigned short ws_ypixel;		// 窗口高度 像素
};

// termio 和 termios结构体的功能相同 同时定义两个的目的是考虑系统的兼容性
#define NCC 8
struct termio {		// 该结构体是在AT&T系统V中定义的
	unsigned short c_iflag;		/* input mode flags */
	unsigned short c_oflag;		/* output mode flags */
	unsigned short c_cflag;		/* control mode flags */
	unsigned short c_lflag;		/* local mode flags */
	unsigned char c_line;		/* line discipline */
	unsigned char c_cc[NCC];	/* control characters */
};


#define NCCS 17
struct termios {// 该结构是POSIX标准定义的 其体持有当前所有线路的设置，这些线路控制着当前波特率，数据大小，数据流设置，以及许多其他值
	unsigned long c_iflag;		/* input mode flags */ // 可以在输入值传给程序之前控制其处理的方式，其输入值可以是有序列埠或键盘的终端驱动程序收到的字元
	unsigned long c_oflag;		/* output mode flags */ // 输出字元在传到序列埠或显示器之前时如何被程序处理的
	unsigned long c_cflag;		/* control mode flags */ // 主要用于控制终端设备的硬件设备
	unsigned long c_lflag;		/* local mode flags */	// 主要用于控制终端设备不同的特色
	unsigned char c_line;		/* line discipline */ // 线路规则类型
	unsigned char c_cc[NCCS];	/* control characters */ // 控制字符数组 可以提供一些特殊的功能，如Ctrl+C的字元组合 该数组初始值在tty.h中初始化
};

/* c_cc characters */
// c_cc 数组中的值
#define VINTR 0		// 中断
#define VQUIT 1		// 退出
#define VERASE 2	// 擦除
#define VKILL 3		// 终止
#define VEOF 4		// 文件终结符
#define VTIME 5		// 定时器
#define VMIN 6		// 定时器
#define VSWTC 7		// 交换字符
#define VSTART 8	// 开始字符
#define VSTOP 9		// 停指字符
#define VSUSP 10	// 挂起字符
#define VEOL 11		// 行结束
#define VREPRINT 12	// 重显
#define VDISCARD 13	// 丢弃
#define VWERASE 14	// 单词擦除
#define VLNEXT 15	// 下一行
#define VEOL2 16	// 行结束字符

/* c_iflag bits */// 输入模式控制输入数据再传递给程序之前的处理方式
#define IGNBRK	0000001	// 输入时囫囵BREAK条件
#define BRKINT	0000002	// 在BREAK时产生中断信号
#define IGNPAR	0000004	// 忽略奇偶校验位
#define PARMRK	0000010	// 标记奇偶校验位
#define INPCK	0000020	// 允许输入奇偶校验
#define ISTRIP	0000040	// 屏蔽字符第八位
#define INLCR	0000100	// 输入时将转换行符NL映射成回车符CR
#define IGNCR	0000200	// 忽略回车符
#define ICRNL	0000400	// 输入时将回车符CR映射成NL
#define IUCLC	0001000	// 大写转小写
#define IXON	0002000	// 允许开始/停止输入控制
#define IXANY	0004000	// 允许任何字符重启输入
#define IXOFF	0010000	// 允许开始/停指输入控制
#define IMAXBEL	0020000	// 输入队列满时响铃

/* c_oflag bits */ // 输出模式控制方式，即由程序发出的字符在传递到串行接口或屏幕之前的处理方式
#define OPOST	0000001
#define OLCUC	0000002
#define ONLCR	0000004
#define OCRNL	0000010
#define ONOCR	0000020
#define ONLRET	0000040
#define OFILL	0000100
#define OFDEL	0000200
#define NLDLY	0000400
#define   NL0	0000000
#define   NL1	0000400
#define CRDLY	0003000
#define   CR0	0000000
#define   CR1	0001000
#define   CR2	0002000
#define   CR3	0003000
#define TABDLY	0014000
#define   TAB0	0000000
#define   TAB1	0004000
#define   TAB2	0010000
#define   TAB3	0014000
#define   XTABS	0014000
#define BSDLY	0020000
#define   BS0	0000000
#define   BS1	0020000
#define VTDLY	0040000
#define   VT0	0000000
#define   VT1	0040000
#define FFDLY	0040000
#define   FF0	0000000
#define   FF1	0040000

/* c_cflag bit meaning */ // c_cflag 标志符号常数（8进制）
#define CBAUD	0000017	// 传输速率位掩码
#define  B0	0000000		/* hang up */
#define  B50	0000001	//波特率 50
#define  B75	0000002
#define  B110	0000003
#define  B134	0000004
#define  B150	0000005
#define  B200	0000006
#define  B300	0000007
#define  B600	0000010
#define  B1200	0000011
#define  B1800	0000012
#define  B2400	0000013
#define  B4800	0000014
#define  B9600	0000015
#define  B19200	0000016
#define  B38400	0000017
#define EXTA B19200		// 扩展波特率A
#define EXTB B38400
#define CSIZE	0000060	// 字符宽度码
#define   CS5	0000000	// 每字符5比特位
#define   CS6	0000020
#define   CS7	0000040
#define   CS8	0000060
#define CSTOPB	0000100
#define CREAD	0000200
#define CPARENB	0000400
#define CPARODD	0001000
#define HUPCL	0002000
#define CLOCAL	0004000
#define CIBAUD	03600000		/* input baud rate (not used) */
#define CRTSCTS	020000000000		/* flow control */

#define PARENB CPARENB
#define PARODD CPARODD

/* c_lflag bits */  // c_lflag 比特位
#define ISIG	0000001
#define ICANON	0000002
#define XCASE	0000004
#define ECHO	0000010
#define ECHOE	0000020
#define ECHOK	0000040
#define ECHONL	0000100
#define NOFLSH	0000200
#define TOSTOP	0000400
#define ECHOCTL	0001000
#define ECHOPRT	0002000
#define ECHOKE	0004000
#define FLUSHO	0010000
#define PENDIN	0040000
#define IEXTEN	0100000

/* modem lines */ // c_line 线路信号
#define TIOCM_LE	0x001	// line enable
#define TIOCM_DTR	0x002	// data terminal ready
#define TIOCM_RTS	0x004	// request to sent
#define TIOCM_ST	0x008	// serial transfar 串行数据发送
#define TIOCM_SR	0x010	// serial request
#define TIOCM_CTS	0x020	// clear to send
#define TIOCM_CAR	0x040	// carrier detect 载波检测
#define TIOCM_RNG	0x080	// ring indicate 响铃指示
#define TIOCM_DSR	0x100	// data set ready
#define TIOCM_CD	TIOCM_CAR
#define TIOCM_RI	TIOCM_RNG

/* tcflow() and TCXONC use these */
#define	TCOOFF		0
#define	TCOON		1
#define	TCIOFF		2
#define	TCION		3

/* tcflush() and TCFLSH use these */
#define	TCIFLUSH	0
#define	TCOFLUSH	1
#define	TCIOFLUSH	2

/* tcsetattr uses these */
#define	TCSANOW		0	// 立刻对值进行修改
#define	TCSADRAIN	1	// 等当前输出完成后再对值进行修改
#define	TCSAFLUSH	2	// 等当前的输出完成之后，再对值进行修改，但丢弃还未从read调用返回的当前的可用的任何输入。

typedef int speed_t;

// 以下这些函数在编译环境的 libc.a 中实现 
extern speed_t cfgetispeed(struct termios *termios_p);
extern speed_t cfgetospeed(struct termios *termios_p);
extern int cfsetispeed(struct termios *termios_p, speed_t speed);
extern int cfsetospeed(struct termios *termios_p, speed_t speed);
extern int tcdrain(int fildes);
extern int tcflow(int fildes, int action);
extern int tcflush(int fildes, int queue_selector);
extern int tcgetattr(int fildes, struct termios *termios_p);
extern int tcsendbreak(int fildes, int duration);
extern int tcsetattr(int fildes, int optional_actions,
	struct termios *termios_p);

#endif

