/*
 *  linux/init/main.c
 *
 *  (C) 1991  Linus Torvalds
 */

#define __LIBRARY__
#include <unistd.h>
#include <time.h>
/*
 * we need this inline - forking from kernel space will result
 * in NO COPY ON WRITE (!!!), until an execve is executed. This
 * is no problem, but for the stack. This is handled by not letting
 * main() use the stack at all after fork(). Thus, no function
 * calls - which means inline code for fork too, as otherwise we
 * would use the stack upon exit from 'fork()'.
 *
 * Actually only pause and fork are needed inline, so that there
 * won't be any messing with the stack from main(), but we define
 * some others too.
 */

inline _syscall0(int, fork)
inline _syscall0(int, pause)
inline _syscall1(int,setup,void *,BIOS)
inline _syscall0(int, sync)

#include <linux/tty.h>
#include <linux/sched.h>
#include <linux/head.h>
#include <asm/system.h>
#include <asm/io.h>

#include <stddef.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

#include <linux/fs.h>

static char printbuf[1024];

extern int vsprintf();
extern void init(void);
extern void blk_dev_init(void);
extern void chr_dev_init(void);
extern void hd_init(void);
extern void floppy_init(void);
extern void mem_init(long start, long end);
extern long rd_init(long mem_start, int length);
extern long kernel_mktime(struct tm * tm);
extern long startup_time;

/* 以下地址是在setup.s中存储了一些设备信息 */
#define EXT_MEM_K (*(unsigned short *)0x90002)			// 1M以后的空间为扩展内存大小（KB）
#define DRIVE_INFO	(*(struct drive_info *)0x90080)		// 硬盘参数表32字节内容
#define ORIG_ROOT_DEV (*(unsigned short *)0x901FC)		// 根文件系统所在的设备号
/*
 * Yeah, yeah, it's ugly, but I cannot find how to do this correctly
 * and this seems to work. I anybody has more info on the real-time
 * clock I'd be interested. Most of this was trial and error, and some
 * bios-listing reading. Urghh.
 */

#define CMOS_READ(addr) ({		\
	outb_p(0x80|addr, 	0x70);	\
	inb_p(0x71);					\
})

#define BCD_TO_BIN(val) ((val) = ((val)&15) + ((val)>>4) * 10)

static void time_init (void){
	struct tm time;
	do{									// 获取当前时间都是BCB码值
		time.tm_sec = CMOS_READ(0);		// 秒
		time.tm_min = CMOS_READ(2);		// 分钟
		time.tm_hour = CMOS_READ(4);	// 小时
		time.tm_mday = CMOS_READ(7);	// 天
		time.tm_mon = CMOS_READ(8);		// 月
		time.tm_year = CMOS_READ(9);	// 年
	}while(time.tm_sec != CMOS_READ(0));
	BCD_TO_BIN(time.tm_sec);
	BCD_TO_BIN(time.tm_min);
	BCD_TO_BIN(time.tm_hour);
	BCD_TO_BIN(time.tm_mday);
	BCD_TO_BIN(time.tm_mon);
	BCD_TO_BIN(time.tm_year);
	time.tm_mon--;
	startup_time = kernel_mktime(&time);
}

//下面定义一些局部变量
static long memory_end = 0;				// 机器具有的物理内存容量
static long buffer_memory_end = 0;		// 高速缓冲区末端地址
static long main_memory_start = 0;		// 主内存（将用于分页）开始位置
static long kernel_mem = 0;

struct drive_info {
	char dummy[32];
} drive_info;

void main(void)	/* This really IS void, no error here. */		//0x6768
{				/* The startup routine assumes (well, ...) this */
/*
 * Interrupts are still disabled. Do necessary setups, then
 * enable them
 */
	ROOT_DEV = ORIG_ROOT_DEV;
	drive_info = DRIVE_INFO; // 复制0x90080处的硬盘参数
	memory_end = (1<<20) + (EXT_MEM_K<<10);	// 获取扩展内存
	memory_end &= 0xffffff000;				// 忽略不到4k(1页)的内存
	if (memory_end > 16 * 1024 * 1024)		// 如果内存超过16MB 则按16MB计
		memory_end = 16 * 1024 * 1024;
	if (memory_end > 12 * 1024 * 1024)		// 如果内存>12M,设置缓冲区末端=4M
		buffer_memory_end = 4 * 1024 * 1024;
	else if (memory_end > 6 * 1024 * 1024)	// 如果内存>6M,设置缓冲区末端=2M
		buffer_memory_end = 2 * 1024 * 1024;
	else
		buffer_memory_end = 1 * 1024 * 1024;// 否则缓冲区末端=1M
	main_memory_start = buffer_memory_end;	// 主内存起始位置 = 缓冲区末端
// 如果在Makefile中定义了内存虚拟盘号RAMDISK,则初始化虚拟盘，此时主内存将减少
#ifdef RAMDISK
	main_memory_start += rd_init(main_memory_start, RAMDISK*1024);
#endif
	mem_init(main_memory_start, memory_end);
	trap_init();
	blk_dev_init();
	chr_dev_init();
	tty_init();
	time_init();
	sched_init();
	buffer_init(buffer_memory_end);
	hd_init();
	floppy_init();
	sti();
	move_to_user_mode();
	if (!fork()) {		/* we count on this going ok */
		init();
	}
/*
 *   NOTE!!   For any other task 'pause()' would mean we have to get a
 * signal to awaken, but task0 is the sole exception (see 'schedule()')
 * as task 0 gets activated at every idle moment (when no other tasks
 * can run). For task0 'pause()' just means we go check if some other
 * task can run, and if not we return here.
 */
	for(;;) pause();
	
}

static int printf(const char *fmt, ...){
	va_list args;
	int i;
	
	va_start(args, fmt);	
	write(1, printbuf, i=vsprintf(printbuf, fmt, args));	
	va_end(args);
	return i;

}
//读取并执行/etc/rc文件时所使用的命令行参数和环境变量
static char * argv_rc[] = {"/bin/sh", NULL};	// 调用执行程序时参数的字符串数组
static char * envp_rc[] = {"HOME=/", NULL};		// 调用执行程序时的环境字符串数组
// 运行shell时所使用的命令行参数和环境参数
static char * argv[] = {"-/bin/sh", NULL};		//"-"是传给shell程序sh的一个标志，通过识别该标志，sh程序会作为登录shell执行，其执行过程与在shell提示符下执行sh不一样
static char * envp[] = {"HOME=/usr/root", NULL};

void init(void){
	int pid, i;
	setup((void *) &drive_info);

	(void) open("/dev/tty0", O_RDWR, 0);
	(void) dup(0);			// 复制句柄，产生句柄1号--stdout标准输出设备
	(void) dup(0);			// 复制句柄，产生句柄2号--stderr标准出错输出设备
	// 创建一个子进程（任务2）, 对于被创建的子进程fork将返回0值，对于原进程（父进程）返回其进程号
	if(!(pid=fork())){
		// 此处执行的是子进程的代码
		close(0);					// 关闭句柄0（stdin）
		if(open("/etc/rc", O_RDONLY, 0))	// 以只读的方式打开
			_exit(1);						// 失败则退出
		execve("/bin/sh",argv_rc, envp_rc);
		_exit(2);							// 若execve执行失败则退出
	}
	if (pid>0)
		while (pid != wait(&i))
			/* nothing */;
	while (1) {
		if ((pid=fork())<0) {
			printf("Fork failed in init\r\n");
			continue;
		}
		if (!pid) {
			close(0);close(1);close(2);
			setsid();
			(void) open("/dev/tty0",O_RDWR,0);
			(void) dup(0);
			(void) dup(0);
			_exit(execve("/bin/sh",argv,envp));
		}
		while (1)
			if (pid == wait(&i))
				break;
		printf("\n\rchild %d died with code %04x\n\r",pid,i);
		sync();
	}
	_exit(0);	/* NOTE! _exit, not exit() */
}