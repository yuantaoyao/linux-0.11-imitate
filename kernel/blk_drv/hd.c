/*
 *  linux/kernel/hd.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 * This is the low-level hd interrupt support. It traverses the
 * request-list, using interrupts to jump between functions. As
 * all the functions are called within interrupts, we may not
 * sleep. Special care is recommended.
 * 
 *  modified by Drew Eckhardt to check nr of hd's from the CMOS.
 */

#include <linux/config.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/hdreg.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/segment.h>

#define MAJOR_NR 3									// 硬盘主设备号为3
#include "include/blk.h"

extern void hd_interrupt(void);
extern void rd_load(void);

static int recalibrate = 1;
static int reset = 1;							// 磁盘复位标志

static void recal_intr(void);

/*
 *  This struct defines the HD's and their types.
 */
struct hd_i_struct {
	int head,sect,cyl,wpcom,lzone,ctl;
	};


// 读CMOS宏用于读取硬盘信息
#define CMOS_READ(addr)({	\
outb_p(0x80|addr, 0x70);	\
inb_p(0x71);				\
})

/* Max read/write errors/sector */
#define MAX_ERRORS	7				// 读写一个扇区时允许的最多出错次数
#define MAX_HD		2				// 系统支持的最多硬盘数

#ifdef HD_TYPE
struct hd_i_struct hd_info[] = { HD_TYPE };
#define NR_HD ((sizeof (hd_info))/(sizeof (struct hd_i_struct)))
#else
struct hd_i_struct hd_info[] = { {0,0,0,0,0,0},{0,0,0,0,0,0} };
static int NR_HD = 0;
#endif

static struct hd_struct {
	long start_sect;
	long nr_sects;
} hd[5*MAX_HD]={{0,0},};
// 从端口port中读取nr长度的数据到buf中
#define port_read(port, buf, nr)	\
	__asm__ ("cld;rep;insw"::"d" (port), "D" (buf), "c" (nr))
// 把buf缓冲区中的数据写入端口port长度为nr
#define port_write(port, buf, nr)	\
	__asm__ ("cld;rep;outsw":: "d" (port), "S" (buf), "c" (nr))

/* This may be used only once, enforced by 'static int callable' */
int sys_setup(void * BIOS)
{
	static int callable = 1;
	int i, drive;
	unsigned char cmos_disks;
	struct partition * p;
	struct buffer_head * bh;
	if(!callable)
		return -1;
	callable = 0;
#ifndef HD_TYPE												// 如果没有定义HD_TYPE, 则读取
	for (drive = 0; drive<2;drive++) {
		hd_info[drive].cyl = *(unsigned short *) BIOS;
		hd_info[drive].head = *(unsigned char *) (2+BIOS);
		hd_info[drive].wpcom = *(unsigned short *) (5+BIOS);
		hd_info[drive].ctl = *(unsigned char *) (8+BIOS);
		hd_info[drive].lzone = *(unsigned short *) (12+BIOS);
		hd_info[drive].sect = *(unsigned char *) (14+BIOS);
		BIOS += 16;											// 每个硬盘参数表长16字节，这里BIOS将指向下一表
	}
	if (hd_info[1].ctl)
		NR_HD = 2;
	else
		NR_HD = 1;
#endif
	for (i=0;i<NR_HD;i++) {
		hd[i*5].start_sect = 0;													// 硬盘起始扇区
		hd[i*5].nr_sects = hd_info[i].head * hd_info[i].sect * hd_info[i].cyl;	// 硬盘总扇区数
	}
	/*
		We querry CMOS about hard disks : it could be that 
		we have a SCSI/ESDI/etc controller that is BIOS
		compatable with ST-506, and thus showing up in our
		BIOS table, but not register compatable, and therefore
		not present in CMOS.

		Furthurmore, we will assume that our ST-506 drives
		<if any> are the primary drives in the system, and 
		the ones reflected as drive 1 or 2.

		The first drive is stored in the high nibble of CMOS
		byte 0x12, the second in the low nibble.  This will be
		either a 4 bit drive type or 0xf indicating use byte 0x19 
		for an 8 bit type, drive 1, 0x1a for drive 2 in CMOS.

		Needless to say, a non-zero value means we have 
		an AT controller hard disk for that drive.
		
	*/
	if ((cmos_disks = CMOS_READ(0x12)) & 0xf0)
		if (cmos_disks & 0x0f)
			NR_HD= 2;
		else NR_HD = 1;
	else NR_HD = 0;
	for (i = NR_HD; i<2; i++) {	// 如果NR_HD为0的话则清空两个硬盘数据，如果为1的话清空第二块硬盘数据
		hd[i*5].start_sect = 0;
		hd[i*5].nr_sects = 0;
	}
	// NR_HD 为系统中硬盘个个数
	for (drive = 0;drive< NR_HD;drive++) {
		if (!(bh = bread(0x300 + drive*5, 0))) {	// 0x300、0x305是设备号分别为第一块和第二块硬盘， 0是指第一个扇区
			printk("Unable to read partition table of drive %d\n\r",
				drive);
			panic("");
		}
		if(bh->b_data[510] != 0x55 || (unsigned char) bh->b_data[511] != 0xAA){
			printk("Bad partition table on drive %d\n\r", drive);
			panic("");
		}
		p = 0x1BE + (void *)bh->b_data; // 分区表位于第一个扇区的0x1BE处
		for(i=1;i<5;i++,p++) {
			hd[i+5*drive].start_sect = p->start_sect;
			hd[i+5*drive].nr_sects = p->nr_sects;
		}
		brelse(bh);
	}
	
	//if(NR_HD)
	//	printk("Partition table%s ok.\n\r", (NR_HD>1)?"s":"");
	rd_load();					// 尝试创建并加载虚拟盘 
	mount_root();
	return (0);
}
static int controller_ready(void) {
	int retries = 100000;
	while(--retries && (inb_p(HD_STATUS)&0xc0) != 0x40);	// 循环等待控制器是否就绪     驱动器就绪比特位6 控制器忙比特位7 （0100 0000 表示驱动器就绪）
	return (retries);
}


// 获取硬盘的执行状态
static int win_result(void)
{
	int i=inb_p(HD_STATUS);

	if ((i & (BUSY_STAT | READY_STAT | WRERR_STAT | SEEK_STAT | ERR_STAT))
		== (READY_STAT | SEEK_STAT))
		return(0); /* ok */
	if (i&1) i=inb(HD_ERROR);
	return (1);
}
/*
 * 向硬盘控制器发送命令块 参数：drive - 硬盘号（0-1）；nsect - 读写扇区数； sect - 起始扇区；head - 磁头号；cyl - 柱面号； cmd - 命令码
 * intr_addr - 硬盘中断处理程序中将要调用的C处理函数指针
 *
 */
static void hd_out(unsigned int drive,unsigned int nsect,unsigned int sect,
		unsigned int head,unsigned int cyl,unsigned int cmd,
		void (*intr_addr)(void))
{
	register int port asm("dx");
	if (drive>1 || head>15)
		panic("Trying to write bad sector");
	if (!controller_ready())
		panic("HD controller not ready");
	do_hd = intr_addr;
	outb_p(hd_info[drive].ctl,HD_CMD);				// 向控制器寄存器发送控制字节
	port=HD_DATA;									// 置dx为硬盘数据寄存器0x1f0
	outb_p(hd_info[drive].wpcom>>2,++port);			// 写补偿柱面号（需要除以4）
	outb_p(nsect,++port);							// 读/写扇区的总数
	outb_p(sect,++port);							// 起始扇区
	outb_p(cyl,++port);								// 柱面号低8位
	outb_p(cyl>>8,++port);							// 柱面号高8位
	outb_p(0xA0|(drive<<4)|head,++port);			// 驱动器号+磁头号
	outb(cmd,++port);								// 硬盘控制命令
}
// 判断当前硬盘设备是否繁忙 1 是 0 否
static int drive_busy(void)
{
	unsigned int i;

	for (i = 0; i < 10000; i++)
		if (READY_STAT == (inb_p(HD_STATUS) & (BUSY_STAT|READY_STAT)))
			break;
	i = inb(HD_STATUS);
	i &= BUSY_STAT | READY_STAT | SEEK_STAT;
	if (i == (READY_STAT | SEEK_STAT))
		return(0);
	printk("HD controller times out\n\r");
	return(1);
}

// 复位硬盘控制器
static void reset_controller(void)
{
	int	i;

	outb(4,HD_CMD);	// 首先向（0x3f6）端口发送允许复位（4）控制字节
	for(i = 0; i < 100; i++) nop();		// 等待一段时间
	outb(hd_info[0].ctl & 0x0f ,HD_CMD);	// 发送正常控制字节
	if (drive_busy())
		printk("HD-controller still busy\n\r");
	if ((i = inb(HD_ERROR)) != 1)
		printk("HD-controller reset failed: %02x\n\r",i);
}


static void reset_hd(int nr)
{
	reset_controller();
	hd_out(nr,hd_info[nr].sect,hd_info[nr].sect,hd_info[nr].head-1,
		hd_info[nr].cyl,WIN_SPECIFY,&recal_intr);
}

void unexpected_hd_interrupt(void)
{
	printk("Unexpected HD interrupt\n\r");
}

// 硬盘执行出错时的处理
static void bad_rw_intr(void) {
	if (++CURRENT->errors >= MAX_ERRORS)				// 错误次数大于7次时，结束当前请求并唤醒等待该请求的进程
		end_request(0);
	if (CURRENT->errors > MAX_ERRORS / 2)				// 错误大于3次，则执行复位硬盘控制器操作
		reset = 1;
	port_read(HD_DATA, CURRENT->buffer, 256);			// 读数据到请求结构缓冲区
}
// 读操作中断调用函数
static void read_intr(void) {
	// 首先判断命令是否合法，
	if (win_result()) {		// 若控制器忙、读写错或命令执行错
		bad_rw_intr();		// 进行读写盘失败处理
		do_hd_request();	// 再次请求硬盘作相应（复位）处理
		return;
	}
	port_read(HD_DATA,CURRENT->buffer,256);// 从数据端口把1个扇区的数据读到请求项的缓冲区中
	CURRENT->errors = 0;
	CURRENT->buffer += 512;
	CURRENT->sector++;
	if (--CURRENT->nr_sectors) {// 需要处理的扇区数递减后依然有待处理扇区数
		do_hd = &read_intr;
		return;
	}
	end_request(1);				// 数据已更新标志位置1
	do_hd_request();
}

// 写扇区中断调用函数
static void write_intr(void) {
	if(win_result()) {							// 如果硬盘控制器返回错误信息
		bad_rw_intr();
		do_hd_request();							// 再次请求硬盘做相应（复位）处理
		return;
	}
	if(--CURRENT->nr_sectors) {
		CURRENT->sector++;
		CURRENT->buffer += 512;
		do_hd = &write_intr;
		port_write(HD_DATA, CURRENT->buffer, 256);
		return ;
	}
	end_request(1);
	do_hd_request();

}

// 硬盘从新复位中断调用函数
static void recal_intr(void)
{
	if (win_result())
		bad_rw_intr();
	do_hd_request();
}

// 执行硬盘读写请求
void do_hd_request(void){
	int i,r;
	unsigned int block,dev;					// 定义请求中的设备号和扇区号
	unsigned int sec,head,cyl;				// 定义与硬盘对应的扇区磁头和柱面
	unsigned int nsect;						// 预读写的总扇区数
	INIT_REQUEST;			// 验证请求项的合法性
	dev = MINOR(CURRENT->dev);				// 获取发送请求的子设备号
	block = CURRENT->sector;				// 请求的起始扇区
	
	if(dev >= 5*NR_HD || block + 2 > hd[dev].nr_sects) {	// 如果子设备不存在或起始扇区大于该设备分区数-2
		end_request(0);
		goto repeat;
	}
	block += hd[dev].start_sect;
	dev /= 5;						// 此时dev代表硬盘号
	__asm__ ("divl %4"
		:"=a" (block), "=d" (sec)
		:"0" (block), "1" (0), "r" (hd_info[dev].sect));	//block 为绝对扇区号，sec 相对扇区号
	__asm__ ("divl %4"
		:"=a" (cyl),"=d"(head)								//cyl 柱面号 head 磁头号
		:"0" (block), "1" (0), "r" (hd_info[dev].head));
	sec++;													// 调整当前磁道扇区号
	nsect = CURRENT->nr_sectors;							// 预读/写的扇区数
	if(reset) {												// 磁盘需要复位
		reset = 0;
		recalibrate = 1;									// 置需要重新校正标志。
		reset_hd(CURRENT_DEV);
		return;
	}
	// 如果此时从新校正标志是置位的，则先复位 再向硬盘控制器发送重新校正命令
	if(recalibrate) {
		recalibrate = 0;
		hd_out(dev, hd_info[CURRENT_DEV].sect, 0, 0, 0, WIN_RESTORE, &recal_intr);
		return;
	}

	if(CURRENT->cmd == WRITE) {
		hd_out(dev, nsect, sec, head, cyl, WIN_WRITE, &write_intr);	// 向硬盘控制块发送调用指令
		for(i=0;i<3000 && !(r=inb_p(HD_STATUS)&DRQ_STAT); i++) 
			/* nothing */ ;
		if (!r) {
			bad_rw_intr();
			goto repeat;
		}
		port_write(HD_DATA, CURRENT->buffer, 256);				// 向硬盘控制器数据寄存器端口写入一个扇区数据
	} else if(CURRENT->cmd == READ) {
		hd_out(dev, nsect, sec, head, cyl, WIN_READ, &read_intr);
	} else {
		panic("unknown hd-command");
	}
}

// 硬盘系统初始化
void hd_init(void){
	blk_dev[MAJOR_NR].request_fn = DEVICE_REQUEST;		// 设置硬盘设备请求处理函数指针
	set_intr_gate(0x2E, &hd_interrupt);					// 设置中断门描述符
	outb_p(inb_p(0x21)&0xfb, 0x21);						// 复位主8259A int2的屏蔽位
	outb(inb_p(0xA1)&0xbf, 0xA1);						// 复位硬盘中断请求屏蔽位（从片上）允许硬盘控制器发送中断请求
}

