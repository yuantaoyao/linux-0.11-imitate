/*
 *  linux/kernel/blk_dev/ll_rw.c
 *
 * (C) 1991 Linus Torvalds
 */

/*
 * This handles all read/write requests to block devices
 */
#include <errno.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/system.h>

#include "include/blk.h"

/*
 * The request-struct contains all necessary data
 * to load a nr of sectors into memory
 */
struct request request[NR_REQUEST];

/*
 * used to wait on when there are no free requests
 */
struct task_struct * wait_for_request = NULL;

/* blk_dev_struct is:
 *	do_request-address
 *	next-request
 */
 // 各个设备的请求队列及操作函数组成的数组
struct blk_dev_struct blk_dev[NR_BLK_DEV] = {
	{ NULL, NULL },		/* no_dev */
	{ NULL, NULL },		/* dev mem */
	{ NULL, NULL },		/* dev fd */
	{ NULL, NULL },		/* dev hd */
	{ NULL, NULL },		/* dev ttyx */
	{ NULL, NULL },		/* dev tty */
	{ NULL, NULL }		/* dev lp */
};

/*
 * add-request adds a request to the linked list.
 * It disables interrupts so that it can muck with the
 * request-lists in peace.
 */
 // 将已经设置好的请求项req添加到指定设备的请求项链表中，如果该设备的当前请求项指针为空则可以设置req为该设备的当前请求项
static void add_request(struct blk_dev_struct * dev, struct request * req) {
	struct request * tmp;
	// 再一次对请求项的指针和标志做初始设置
	req->next = NULL;		// 置空请求的下一项请求指针
	cli();					// 关中断
	if (req->bh)			// 清空请求项相关的缓存头指针
		req->bh->b_dirt = 0;
	if (!(tmp = dev->current_request)) {// 查看指定设备是否有当前请求项 即设备是否忙 如果不忙则说明当前设备没有请求项，本次是第一个请求项，也是唯一一个
		dev->current_request = req;		// 因此可以把设备的当前请求指针指向本请求
		sti();							// 开中断 
		(dev->request_fn)();			// 执行请求函数，对于硬盘是do_hd_request()
		return;
	}
	// 如果目前设备忙 则通过电梯算法搜索最佳插入位置，然后将当前请求插入到最佳位置 开中断 退出
	for ( ; tmp->next ; tmp=tmp->next)
		if ((IN_ORDER(tmp,req) || 
		    !IN_ORDER(tmp,tmp->next)) &&
		    IN_ORDER(req,tmp->next))
			break;
	req->next=tmp->next;
	tmp->next=req;
	sti();
}

static inline void lock_buffer(struct buffer_head * bh) {
	cli();
	while (bh->b_lock)
		sleep_on(&bh->b_wait);
	bh->b_lock = 1;
	sti();

}

static inline void unlock_buffer(struct buffer_head * bh)
{
	if (!bh->b_lock)
		printk("ll_rw_block.c: buffer not locked\n\r");
	bh->b_lock = 0;
	wake_up(&bh->b_wait);
}

// 创建请求项并添加到请求队列
static void make_request(int major, int rw, struct buffer_head * bh) {
	struct request * req;
	int rw_ahead;	//逻辑值，用于判断是否为READA或WRITEA命令
	
	/* WRITEA/READA is special case - it is not really needed, so if the */
	/* buffer is locked, we just forget about it, else it's a normal read */

	// write/head后面的A是ahead，表示提前预读/写数据块的意思
	if(rw_ahead = (rw == READA|| rw == WRITEA)) {
		if(bh->b_lock)
			return ;
		if(rw == READA)
			rw = READ;
		else
			rw = WRITE;
	}
	if(rw !=READ && rw!= WRITE)
		panic("Bad block dev command, must be R/W/RA/WA");
	lock_buffer(bh);
	if((rw == WRITE && !bh->b_dirt) || (rw == READ && bh->b_uptodate)) {//这两种情况不用添加请求项，一是写操作时，缓冲区的数据在读入之后没有被修改过;二是读操作但缓冲区的数据已经是更新的
		unlock_buffer(bh);
		return ;
	}
repeat:
/* we don't allow the write-requests to fill up the queue completely:
 * we want some room for reads: they take precedence. The last third
 * of the requests are only for reads.
 *
 * 请求的后3分之一空间仅用于读请求（读请求优先原则）
 */
 	if(rw == READ)
		req = request + NR_REQUEST;				// 读请求末尾指针指向队列末尾
	else
		req = request + ((NR_REQUEST*2)/3);		// 写请求末尾指针指向2/3处
/* find an empty request 搜索一个空请求项 当请求结构中的dev值为-1时表示该项未被占用*/
	while (--req >= request) {
		if (req->dev < 0)
			break;
	}
/* if none found, sleep on new requests: check for rw_ahead */
	if(req < request) {		// 如果已经搜索到头（说明没有空项）
		if (rw_ahead) {		// 如果提前读写则退出
			unlock_buffer(bh);
			return ;
		}
		sleep_on(&wait_for_request);// 否则睡眠
		goto repeat;
	}
/* fill up the request-info, and add it to the queue */
	req->dev = bh->b_dev;
	req->cmd = rw;
	req->errors=0;
	req->sector = bh->b_blocknr<<1;
	req->nr_sectors = 2;
	req->buffer = bh->b_data;
	req->waiting = NULL;
	req->bh = bh;
	req->next = NULL;
	add_request(blk_dev + major, req);
}

/*
 * 其他程序通过调用该函数实现最底层的块设备读写，主要功能是建立块设备读写请求项并插入到指定的块设备请求队列中，
 * 实际的读写操作是由request_fn（add_request方法中调用）函数来完成，对于硬盘操作，该函数是do_hd_request(),对于软盘操作，该函数是do_fd_request()
 * 对于虚拟盘则是do_rd_request()
 */
void ll_rw_block(int rw, struct buffer_head * bh){
	unsigned int major;			// 主设备号（硬盘是3）
	// 如果主设备号不存在或该设备的请求操作函数不存在直接返回
	if((major = MAJOR(bh->b_dev)) >= NR_BLK_DEV || !(blk_dev[major].request_fn)){
		printk("Trying to read nonexistent block-device\n\r");
		return;
	}
	make_request(major, rw, bh);
}
void blk_dev_init(void) {
	int i;
	// 将所有请求项置空
	for (i=0; i<NR_REQUEST; i++) {
		request[i].dev = -1;
		request[i].next = NULL;
	}
}

