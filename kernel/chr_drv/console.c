/*
 *  linux/kernel/console.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 *	console.c
 *
 * This module implements the console io functions
 *	'void con_init(void)'
 *	'void con_write(struct tty_queue * queue)'
 * Hopefully this will be a rather complete VT102 implementation.
 *
 * Beeping thanks to John T Kohl.
 */

/*
 *  NOTE!!! We sometimes disable and enable interrupts for a short while
 * (to put a word in video IO), but this will work even for keyboard
 * interrupts. We know interrupts aren't enabled when getting a keyboard
 * interrupt, as we use trap-gates. Hopefully all is well.
 */

/*
 * Code to check for different video-cards mostly by Galen Hunt,
 * <g-hunt@ee.utah.edu>
 */

#include <linux/sched.h>
#include <linux/tty.h>
#include <asm/io.h>
#include <asm/system.h>

/*
 * These are set up by the setup-routine at boot-time:
 */

#define ORIG_X			(*(unsigned char *)0x90000)							// 在setup.asm中我们把光标的初始位置x、y坐标放到了0x90000处
#define ORIG_Y			(*(unsigned char *)0x90001)							// 所以可以从这两个字节中获取光标初始位置
#define ORIG_VIDEO_PAGE		(*(unsigned short *)0x90004)					// 同上此处的一个字节地址中存储页码
#define ORIG_VIDEO_MODE		((*(unsigned short *)0x90006) & 0xff)			// 此处存储屏幕列数和显示模式，显示列被置空 
#define ORIG_VIDEO_COLS 	(((*(unsigned short *)0x90006) & 0xff00) >> 8)	// 此处获取显示列数 具体参数请看中断10h的00功能号
#define ORIG_VIDEO_LINES	(25)											// 显示行数
#define ORIG_VIDEO_EGA_AX	(*(unsigned short *)0x90008)					// 同上 但这里没有任何定义
#define ORIG_VIDEO_EGA_BX	(*(unsigned short *)0x9000a)					// 0x9000b 视频状态  0x9000a 已安装显存内存大小
#define ORIG_VIDEO_EGA_CX	(*(unsigned short *)0x9000c)					// 0x9000d 特性连接器信息 0x9000c 视频开关设置

// 配置显示器单色/彩色显示模式类型符号常量
#define VIDEO_TYPE_MDA		0x10	/* Monochrome Text Display	*/ // 单色
#define VIDEO_TYPE_CGA		0x11	/* CGA Display 			*/
#define VIDEO_TYPE_EGAM		0x20	/* EGA/VGA in Monochrome Mode	*/ // EGA/VGA单色
#define VIDEO_TYPE_EGAC		0x21	/* EGA/VGA in Color Mode	*/ // EGA/VGA彩色

#define NPAR 16						// 转义字符序列中最大参数个数

extern void keyboard_interrupt(void);
// 定义一些静态全局属性
static unsigned char	video_type;		/* Type of display being used	*/
static unsigned long	video_num_columns;	/* Number of text columns	*/
static unsigned long	video_size_row;		/* Bytes per row		每行字节数*/
static unsigned long	video_num_lines;	/* Number of test lines		*/
static unsigned char	video_page;		/* Initial video page		*/
static unsigned long	video_mem_start;	/* Start of video RAM		*/
static unsigned long	video_mem_end;		/* End of video RAM (sort of)	*/
static unsigned short	video_port_reg;		/* Video register select port	显示控制索引寄存器端口*/
static unsigned short	video_port_val;		/* Video register value port	显示控制数据寄存器端口*/
static unsigned short	video_erase_char;	/* Char+Attrib to erase with	擦除字符属性及字符*/
// 以下变量定义了屏幕卷屏操作（origin 用于定位移动的虚拟窗口左上角原点内存地址）
static unsigned long	origin;		/* Used for EGA/VGA fast scroll	*/	//滚屏内存起始位置
static unsigned long	scr_end;	/* Used for EGA/VGA fast scroll	*/	//滚屏末端内存位置
static unsigned long	pos;		// 当前光标对应的显示内存位置
static unsigned long	x,y;		// 当前光标位置
static unsigned long	top,bottom;	// 滚动时顶行行号，底行行号
// state 用于明确处理esc转义序列时的当前步骤,par[]用于存放ESC序列的中间处理参数
static unsigned long	state=0;			// ANSI 转义字符序列处理状态
static unsigned long	npar,par[NPAR];		// ANSI 转义字符序列参数个数和参数数组
static unsigned long	ques=0;				// 收到问号字符标志
static unsigned char	attr=0x07;			// 字符属性 （黑底白字）

static void sysbeep(void);				// 系统蜂鸣函数

/*
 * this is what the terminal answers to a ESC-Z or csi0c
 * query (= vt100 response).
 */
#define RESPONSE "\033[?1;2c" // 表示终端是高级视频终端

static inline void gotoxy(unsigned int new_x, unsigned int new_y){		// 将光标自定到特定位置， 并将光标对应到pos显示内存位置
	if (new_x > video_num_columns || new_y > video_num_lines)			// 如果预设光标位置大于屏幕行数和列数说明设置错误 退出
		return ;

	x = new_x;
	y = new_y;
	pos = origin + y * video_size_row + (x << 1);						// 一列用两个字节表示 所以 x<<1
}
/*
 * 设置卷屏起始显示内存地址
 */
static inline void set_origin(void) {
	cli();
	outb_p(12, video_port_reg);
	outb_p(0xff&((origin - video_mem_start) >> 9), video_port_val);
	outb_p(13, video_port_reg);
	outb_p(0xff&((origin - video_mem_start) >> 1), video_port_val);
	sti();
}
/*
 * 向上卷动一行 将屏幕滚动区域的内容向下移动一行，并在区域顶出现新行上添加空格字符
 */
static void scrup(void) {		// 向上卷动一行
	if (video_type == VIDEO_TYPE_EGAC || video_type == VIDEO_TYPE_EGAM) {	// 判断是否是EGA/VGA卡
		if(!top && bottom == video_num_lines) {								// 如果起始行为0 bottom 指向最低端 表示整屏移动
			origin += video_size_row;										// 滚屏位置下移一行
			pos += video_size_row;											// 指针内存下移一行
			scr_end += video_size_row;										// 滚屏末端下移一行
			if (scr_end > video_mem_end) {									// 滚屏末端已经超过了最大显示内存
				__asm__	("cld\n\t"
						"rep \n\t"
						"movsl\n\t"
						"movl video_num_columns, %1\n\t"
						"rep\n\t"
						"stosw"
						:: "a" (video_erase_char),
						"c" ((video_num_lines - 1) * video_num_columns >> 1),
						"D" (video_mem_start),
						"S" (origin));
				scr_end -= origin - video_mem_start;
				pos -= origin - video_mem_start;
				origin = video_mem_start;
			} else {														// 如果末端对应的内存地址没有超出显示区域
				 __asm__ ("cld\n\t"
						"rep\n\t"
						"stosw"
						:: "a" (video_erase_char),
						"c" (video_num_columns),
						"D" (scr_end - video_size_row));
			}
			set_origin();													// 将origin写入显示控制器中
		} else {															// 滚屏末端没有超过最大显示内存 top到bottom区域向上移动
			__asm__ ("cld\n\t"
					"rep\n\t"
					"movsl\n\t"
					"movl video_num_columns, %%ecx\n\t"
					"rep\n\t"
					"stosw"
					::"a" (video_erase_char),
					"c" ((bottom -top -1) * video_num_columns >> 1),
					"D" (origin + video_size_row * top),
					"S" (origin + video_size_row * (top + 1))
					);
		}

	} else {																// 如果不是EGA/VGA卡 只能整屏移动 并且会自动调整超出范围，即会自动翻卷指针
		__asm__ ("cld\n\t"
				"rep\n\t"
				"movsl\n\t"
				"movl video_num_columns, %%ecx\n\t"
				"rep\n\t"
				"stosw"
				::"a" (video_erase_char),
				"c" ((bottom -top -1) * video_num_columns >> 1),
				"D" (origin + video_size_row * top),
				"S" (origin + video_size_row * (top + 1))
				);
	}

}
/*
 * 向下卷动一行 将屏幕窗口向上移动一行，相应屏幕滚动区域内容向下移动一行并在移动开始行的上方出现一新行
 */
static void scrdown(){
	if (video_type == VIDEO_TYPE_EGAC || video_type == VIDEO_TYPE_EGAM) {	// 判断是否是EGA/VGA卡			其实两段程序完全相同
		__asm__ ("std\n\t"
				"rep\n\t"
				"movsl\n\t"
				"addl $2, %%edi\n\t"
				"movl video_num_columns, %%ecx\n\t"
				"rep\n\t"
				"stosw"
				:: "a" (video_erase_char),
				"c" ((bottom-top-1) * video_num_columns >> 1),
				"D" (origin + video_size_row * bottom -4),
				"S" (origin + video_size_row * (bottom - 1) -4));
	} else {
		__asm__ ("std\n\t"
				"rep\n\t"
				"movsl\n\t"
				"addl $2, %%edi\n\t"
				"movl video_num_columns, %%ecx\n\t"
				"rep\n\t"
				"stosw"
				:: "a" (video_erase_char),
				"c" ((bottom-top-1) * video_num_columns >> 1),
				"D" (origin + video_size_row * bottom -4),
				"S" (origin + video_size_row * (bottom - 1) -4));
	}
	
}

static void lf(void) {		// 光标下移一行（line feed 换行）
	if (y+1<bottom) {		// 光标下移后没有超过当前页
		y++;
		pos += video_size_row;	// 当前内存位置加一行
		return;
	}
	scrup();					// 将屏幕内容上移一行

}

static void cr(void) {
	pos -= x<<1;				// 减去光标x轴上的内存值
	x=0;						// x轴指向0列
}

static void del(void) {
	if (x) {
		pos -= 2;
		x--;
		*(unsigned short *)pos = video_erase_char;
	}
}

static void ri(void) {			// 光标上移一行 （reverse index）
	if (y > top) {
		y--;
		pos -= video_size_row;	// 减去屏幕一行占用的字节数
		return ;
	}
	scrdown();					//将屏幕窗口下移一行
}
 
 static void csi_J(int par)
 {
	 long count;
	 long start;
 
	 switch (par) {
		 case 0: /* erase from cursor to end of display */
			 count = (scr_end-pos)>>1;
			 start = pos;
			 break;
		 case 1: /* erase from start to cursor */
			 count = (pos-origin)>>1;
			 start = origin;
			 break;
		 case 2: /* erase whole display */
			 count = video_num_columns * video_num_lines;
			 start = origin;
			 break;
		 default:
			 return;
	 }
	 __asm__("cld\n\t"
		 "rep\n\t"
		 "stosw\n\t"
		 ::"c" (count),
		 "D" (start),"a" (video_erase_char)
		 );
 }
 
 static void csi_K(int par)
 {
	 long count;
	 long start;
 
	 switch (par) {
		 case 0: /* erase from cursor to end of line */
			 if (x>=video_num_columns)
				 return;
			 count = video_num_columns-x;
			 start = pos;
			 break;
		 case 1: /* erase from start of line to cursor */
			 start = pos - (x<<1);
			 count = (x<video_num_columns)?x:video_num_columns;
			 break;
		 case 2: /* erase whole line */
			 start = pos - (x<<1);
			 count = video_num_columns;
			 break;
		 default:
			 return;
	 }
	 __asm__("cld\n\t"
		 "rep\n\t"
		 "stosw\n\t"
		 ::"c" (count),
		 "D" (start),"a" (video_erase_char)
		 );
 }
 
 void csi_m(void)
 {
	 int i;
 
	 for (i=0;i<=npar;i++)
		 switch (par[i]) {
			 case 0:attr=0x07;break;
			 case 1:attr=0x0f;break;
			 case 4:attr=0x0f;break;
			 case 7:attr=0x70;break;
			 case 27:attr=0x07;break;
		 }
 }

 
 static inline void set_cursor(void)
 {
	 cli();
	 outb_p(14, video_port_reg);
	 outb_p(0xff&((pos-video_mem_start)>>9), video_port_val);
	 outb_p(15, video_port_reg);
	 outb_p(0xff&((pos-video_mem_start)>>1), video_port_val);
	 sti();
 }

 static void respond(struct tty_struct * tty) {
	char * p = RESPONSE;
	cli();
	while (*p) {
		PUTCH(*p, tty->read_q);
		p++;
	}
	sti();
	copy_to_cooked(tty);
 }

 
static void insert_char(void)
{
	int i=x;
	unsigned short tmp, old = video_erase_char;
	unsigned short * p = (unsigned short *) pos;

	while (i++<video_num_columns) {
		tmp=*p;
		*p=old;
		old=tmp;
		p++;
	}
}

static void insert_line(void)
{
	int oldtop,oldbottom;

	oldtop=top;
	oldbottom=bottom;
	top=y;
	bottom = video_num_lines;
	scrdown();
	top=oldtop;
	bottom=oldbottom;
}

static void delete_char(void)
{
	int i;
	unsigned short * p = (unsigned short *) pos;

	if (x>=video_num_columns)
		return;
	i = x;
	while (++i < video_num_columns) {
		*p = *(p+1);
		p++;
	}
	*p = video_erase_char;
}

static void delete_line(void)
{
	int oldtop,oldbottom;

	oldtop=top;
	oldbottom=bottom;
	top=y;
	bottom = video_num_lines;
	scrup();
	top=oldtop;
	bottom=oldbottom;
}

static void csi_at(unsigned int nr)
{
	if (nr > video_num_columns)
		nr = video_num_columns;
	else if (!nr)
		nr = 1;
	while (nr--)
		insert_char();
}

static void csi_L(unsigned int nr)
{
	if (nr > video_num_lines)
		nr = video_num_lines;
	else if (!nr)
		nr = 1;
	while (nr--)
		insert_line();
}

static void csi_P(unsigned int nr)
{
	if (nr > video_num_columns)
		nr = video_num_columns;
	else if (!nr)
		nr = 1;
	while (nr--)
		delete_char();
}

static void csi_M(unsigned int nr)
{
	if (nr > video_num_lines)
		nr = video_num_lines;
	else if (!nr)
		nr=1;
	while (nr--)
		delete_line();
}

static int saved_x=0;
static int saved_y=0;

static void save_cur(void)		// 保存当前光标
{
	saved_x=x;
	saved_y=y;
}

void restore_cur(void) {		// 恢复保存的光标
	gotoxy(saved_x, saved_y);
}
 
/*
 * 控制台写函数
 */
void con_write(struct tty_struct * tty){
	int nr;
	char c;
	
	nr = CHARS(tty->write_q);		// 从队列中获取现有字符数
	while(nr--) {					// 循环处理待写字符
		GETCH(tty->write_q, c); 	// 从数据尾部取出一个数 放入c 并且尾指针+1
 		switch (state) {
			case 0:
				if (c>31 && c<127) {		// 普通显示字符
					if (x>= video_num_columns) {			// 判断是否要换行
						x -=video_num_columns;
						pos -= video_size_row;
						lf();								// 光标下移
					}
				
					__asm__ ("movb attr,%%ah\n\t"
						"movw %%ax, %1\n\t"
						::"a" (c), "m" (*(short *) pos));
					pos += 2;
					x++;
				} else if (c == 27)
					state = 1;
				else if (c == 10 || c == 11 || c == 12)							// 跳到下一行
					lf();
				else if (c == 13)												// 跳到本行首部
					cr();
				else if (c == ERASE_CHAR(tty))									// 删除一个字符
					del();
				else if (c == 8) {												// 后退一格
					if(x){
						x--;
						pos -= 2;
					}
				} else if (c == 9){												// 水平制表符
					c = 8- (x&7);
					x += c;
					pos += c<<1;
					if(x>video_num_columns){
						x -= video_num_columns;
						pos -= video_size_row;
						lf();
					}
					c = 9;
				} else if (c == 7)
					sysbeep();
				break;
 			case 1:
				state = 0;
				if (c == '[')
					state = 2;
				else if (c == 'E')
					gotoxy(0, y+1);
				else if (c == 'M')
					ri();
				else if (c == 'D')
					lf();
				else if (c == 'Z')
					respond(tty);
				else if (c == '7')
					save_cur();
				else if (c == '8')
					restore_cur();
				break;
			case 2:
				for (npar = 0;npar < NPAR;npar++)
					par[npar] = 0;
				npar = 0;
				state = 3;
				if ((ques =(c == '?')))
					break;
			case 3:
				if (c == ';' && npar < NPAR - 1) {
					npar++;
					break;
				} else if (c >= '0' && c <= '9') {
					par[npar] = 10 * par[npar] +c - '0';
					break;
				} else state = 4;
			case 4:
				state = 0;
				switch (c){
					case 'G': case '`':
						if (par[0]) par[0]--;
						gotoxy(par[0],y);
						break;
					case 'A':
						if (!par[0]) par[0]++;
						gotoxy(x,y-par[0]);
						break;
					case 'B': case 'e':
						if (!par[0]) par[0]++;
						gotoxy(x,y+par[0]);
						break;
					case 'C': case 'a':
						if (!par[0]) par[0]++;
						gotoxy(x+par[0],y);
						break;
					case 'D':
						if (!par[0]) par[0]++;
						gotoxy(x-par[0],y);
						break;
					case 'E':
						if (!par[0]) par[0]++;
						gotoxy(0,y+par[0]);
						break;
					case 'F':
						if (!par[0]) par[0]++;
						gotoxy(0,y-par[0]);
						break;
					case 'd':
						if (par[0]) par[0]--;
						gotoxy(x,par[0]);
						break;
					case 'H': case 'f':
						if (par[0]) par[0]--;
						if (par[1]) par[1]--;
						gotoxy(par[1],par[0]);
						break;
					case 'J':
						csi_J(par[0]);
						break;
					case 'K':
						csi_K(par[0]);
						break;
					case 'L':
						csi_L(par[0]);
						break;
					case 'M':
						csi_M(par[0]);
						break;
					case 'P':
						csi_P(par[0]);
						break;
					case '@':
						csi_at(par[0]);
						break;
					case 'm':
						csi_m();
						break;
					case 'r':
						if (par[0]) par[0]--;
						if (!par[1]) par[1] = video_num_lines;
						if (par[0] < par[1] &&
						    par[1] <= video_num_lines) {
							top=par[0];
							bottom=par[1];
						}
						break;
					case 's':
						save_cur();
						break;
					case 'u':
						restore_cur();
						break;
				}
				
		}
	}
	set_cursor();
}

void con_init(void){		// 0x7ca3
	register unsigned char a;	// 查漏补缺：register 关键字是指将定义的变量（一定是局部变量）请求放入寄存器 目的是加快计算 但只是请求不一定成功
	char *display_des = "????";
	char *display_ptr;

	// 获取我们在setup.asm中寄存的显示器参数
	video_num_columns = ORIG_VIDEO_COLS;	// 获取显示器列数
	video_size_row = video_num_columns * 2;	// 显示器每行有多少字节
	video_num_lines = ORIG_VIDEO_LINES;		// 每页显示器显示字符行数
	video_page = ORIG_VIDEO_PAGE;			// 当前显示页
	video_erase_char = 0x0720;			// 擦除字符（0x20是字符, 0x07是属性）
	if (ORIG_VIDEO_MODE == 7){				// 表示单色（黑白两色）显示器
		video_mem_start = 0xb0000;
		video_port_reg = 0x3b4;
		video_port_val = 0x3b5;
		if ((ORIG_VIDEO_EGA_BX & 0xff) != 0x10) {	// bx值不等于0x10说明是一般单色显卡
			video_type = VIDEO_TYPE_EGAM;	// EGA 单色
			video_mem_end = 0xb8000;
			display_des = "EGAm";
		} else {									// 否则是单色显卡MDA
			video_type = VIDEO_TYPE_MDA;
			video_mem_end = 0xb2000;
			display_des = "*MDA";
		}
	} else {								// 否则是多色显示器
		video_mem_start = 0xb8000;			
		video_port_reg = 0x3d4;
		video_port_val = 0x3d5;
		if ((ORIG_VIDEO_EGA_BX & 0xff) != 0x10) {	// bx值不等于0x10说明是一般彩色显卡
				video_type = VIDEO_TYPE_EGAC;	// EGA 单色
				video_mem_end = 0xbc000;
				display_des = "EGAc";
			} else {									// 否则是彩色显卡MDA
				video_type = VIDEO_TYPE_CGA;
				video_mem_end = 0xba000;
				display_des = "*CGA";
			}
	}
	
	
	/* Initialize the variables used for scrolling (mostly EGA/VGA)	*/
	
	origin = video_mem_start;														// 显示器开始内存地址
	scr_end = video_mem_start + video_num_lines * video_size_row;					// 显示器结束内存地址
	top = 0;																		// 最顶行号
	bottom = video_num_lines;														// 最低行号

	gotoxy(ORIG_X, ORIG_Y);						// 0x7de7
	set_trap_gate(0x21, &keyboard_interrupt);
	outb_p(inb_p(0x21)&0xfd,0x21);
	a=inb_p(0x61);
	outb_p(a|0x80,0x61);
	outb(a,0x61);
}


/* from bsd-net-2: */

void sysbeepstop(void)
{
	/* disable counter 2 */
	outb(inb_p(0x61)&0xFC, 0x61);
}

int beepcount = 0;

static void sysbeep(void)
{
	/* enable counter 2 */
	outb_p(inb_p(0x61)|3, 0x61);
	/* set command for counter 2, 2 byte write */
	outb_p(0xB6, 0x43);
	/* send 0x637 for 750 HZ */
	outb_p(0x37, 0x42);
	outb(0x06, 0x42);
	/* 1/8 second */
	beepcount = HZ/8;	
}

