/*
 *  linux/kernel/tty_io.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 * 'tty_io.c' gives an orthogonal feeling to tty's, be they consoles
 * or rs-channels. It also implements echoing, cooked mode etc.
 *
 * Kill-line thanks to John T Kohl.
 */
#include <ctype.h>
#include <errno.h>
#include <signal.h>

#define ALRMMASK (1<<(SIGALRM-1))
#define KILLMASK (1<<(SIGKILL-1))
#define INTMASK (1<<(SIGINT-1))
#define QUITMASK (1<<(SIGQUIT-1))
#define TSTPMASK (1<<(SIGTSTP-1))

#include <linux/sched.h>
#include <linux/tty.h>
#include <asm/segment.h>
#include <asm/system.h>

//获取三种模式标志集之一，或者判断一个标志集是否有置位标志
#define _L_FLAG(tty,f)	((tty)->termios.c_lflag & f)	// 本地模式标志
#define _I_FLAG(tty,f)	((tty)->termios.c_iflag & f)	// 输入模式标志
#define _O_FLAG(tty,f)	((tty)->termios.c_oflag & f)	// 输出模式标志
// 本地模式标志集
#define L_CANON(tty)	_L_FLAG((tty),ICANON)			// 取规范模式标志
#define L_ISIG(tty)	_L_FLAG((tty),ISIG)					// 去信号标志
#define L_ECHO(tty)	_L_FLAG((tty),ECHO)					// 取回显字符标志
#define L_ECHOE(tty)	_L_FLAG((tty),ECHOE)			// 规范模式时取回显擦除标志
#define L_ECHOK(tty)	_L_FLAG((tty),ECHOK)			// 规范模式时取KILL擦除当前行标志
#define L_ECHOCTL(tty)	_L_FLAG((tty),ECHOCTL)			// 取回显控制字符标志
#define L_ECHOKE(tty)	_L_FLAG((tty),ECHOKE)			// 规范模式时取KILL擦除行并回显标志
// 输入模式标志集
#define I_UCLC(tty)	_I_FLAG((tty),IUCLC)		// 取大写到小写换行符标志
#define I_NLCR(tty)	_I_FLAG((tty),INLCR)		// 取换行符NR转回车符CR标志
#define I_CRNL(tty)	_I_FLAG((tty),ICRNL)		// 取回车符CR转换行符NL标志
#define I_NOCR(tty)	_I_FLAG((tty),IGNCR)		// 取回车换行符标志
// 输出模式标志集
#define O_POST(tty)	_O_FLAG((tty),OPOST)		// 取执行输出处理标志
#define O_NLCR(tty)	_O_FLAG((tty),ONLCR)		// 取换行符NL转回车换行符CR-NL标志
#define O_CRNL(tty)	_O_FLAG((tty),OCRNL)		// 取回车符CR转换行符NL
#define O_NLRET(tty)	_O_FLAG((tty),ONLRET)	// 取换行符NL执行回车换行的标志
#define O_LCUC(tty)	_O_FLAG((tty),OLCUC)		// 取小写转大写字符标志
// tty 数据结构tty_struct数组，包含控制台，串口1和串口2数据初始化
struct tty_struct tty_table[] = {
	{
		{ICRNL,		/* change incoming CR to NL 将输入的CR转换成NL*/
		OPOST|ONLCR,	/* change outgoing NL to CRNL 将输入的NL转换成CTRL*/
		0,				// 控制模式标志集
		ISIG | ICANON | ECHO | ECHOCTL | ECHOKE,	// 本地模式
		0,		/* console termio */				// 线路规程，0 -- TTY
		INIT_C_CC},									// 控制字符数组
		0,			/* initial pgrp */				// 初始进程组
		0,			/* initial stopped */			// 初始停止标志
		con_write,									// 控制台写函数
		{0,0,0,0,""},		/* console read-queue */	// 控制台读缓冲队列
		{0,0,0,0,""},		/* console write-queue */	// 控制台写缓冲队列
		{0,0,0,0,""}		/* console secondary queue */	// 控制台辅助缓冲队列
	},{
		{0, /* no translation */
		0,  /* no translation */
		B2400 | CS8,		// 控制模式标志集 2400bps,8位数据位
		0,
		0,
		INIT_C_CC},
		0,
		0,
		rs_write,
		{0x3f8,0,0,0,""},		/* rs 1 */	// 串行终端1读缓冲队列
		{0x3f8,0,0,0,""},					// 串行终端1写缓冲队列
		{0,0,0,0,""}						// 串行终端1辅助缓冲队列
	},{
		{0, /* no translation */
		0,  /* no translation */
		B2400 | CS8,			// 控制模式标志集 2400bps,8位数据位
		0,
		0,
		INIT_C_CC},
		0,
		0,
		rs_write,
		{0x2f8,0,0,0,""},		/* rs 2 */	// 串行终端2读缓冲队列
		{0x2f8,0,0,0,""},					// 串行终端2写缓冲队列
		{0,0,0,0,""}						// 串行终端2辅助缓冲队列
	}
};


/*
 * these are the tables used by the machine code handlers.
 * you can implement pseudo-tty's or something by changing
 * them. Currently not done.
 *
 * 下面是汇编程序使用的缓冲队列结构地址表。通过修改这个表，你可以实现伪tty终端或其他类型，现在还没有实现
 * tty读写缓冲队列表，用于rs_io.s汇编程序使用用于取得读写缓冲地址
 */

struct tty_queue * table_list[]={
	&tty_table[0].read_q, &tty_table[0].write_q,
	&tty_table[1].read_q, &tty_table[1].write_q,
	&tty_table[2].read_q, &tty_table[2].write_q
	};

void tty_init(){
	rs_init();
	con_init();
}

void tty_intr(struct tty_struct * tty, int mask)
{
	int i;

	if (tty->pgrp <= 0)
		return;
	for (i=0;i<NR_TASKS;i++)
		if (task[i] && task[i]->pgrp==tty->pgrp)
			task[i]->signal |= mask;
}

static void sleep_if_empty(struct tty_queue * queue){
	cli();
	while(!current->signal && EMPTY(*queue))
		interruptible_sleep_on(&queue->proc_list);
	sti();
}

static void sleep_if_full(struct tty_queue * queue) {
	if(!FULL(*queue))							// 如果队列没有满则退出
		return;
	cli();										// 如果满了 则先关闭中断
	while (!current->signal && LEFT(*queue)<128)
		interruptible_sleep_on(&queue->proc_list);
	sti();										// 恢复中断
}

void wait_for_keypress(void)
{
	sleep_if_empty(&tty_table[0].secondary);
}


void copy_to_cooked(struct tty_struct * tty) {
	signed char c;
	
	while (!EMPTY(tty->read_q) && !FULL(tty->secondary)) {
		GETCH(tty->read_q,c);
		if (c==13)
			if (I_CRNL(tty))
				c=10;
			else if (I_NOCR(tty))
				continue;
			else ;
		else if (c==10 && I_NLCR(tty))
			c=13;
		if (I_UCLC(tty))
			c=tolower(c);
		if (L_CANON(tty)) {
			if (c==KILL_CHAR(tty)) {
				/* deal with killing the input line */
				while(!(EMPTY(tty->secondary) ||
						(c=LAST(tty->secondary))==10 ||
						c==EOF_CHAR(tty))) {
					if (L_ECHO(tty)) {
						if (c<32)
							PUTCH(127,tty->write_q);
						PUTCH(127,tty->write_q);
						tty->write(tty);
					}
					DEC(tty->secondary.head);
				}
				continue;
			}
			if (c==ERASE_CHAR(tty)) {
				if (EMPTY(tty->secondary) ||
				   (c=LAST(tty->secondary))==10 ||
				   c==EOF_CHAR(tty))
					continue;
				if (L_ECHO(tty)) {
					if (c<32)
						PUTCH(127,tty->write_q);
					PUTCH(127,tty->write_q);
					tty->write(tty);
				}
				DEC(tty->secondary.head);
				continue;
			}
			if (c==STOP_CHAR(tty)) {
				tty->stopped=1;
				continue;
			}
			if (c==START_CHAR(tty)) {
				tty->stopped=0;
				continue;
			}
		}
		if (L_ISIG(tty)) {
			if (c==INTR_CHAR(tty)) {
				tty_intr(tty,INTMASK);
				continue;
			}
			if (c==QUIT_CHAR(tty)) {
				tty_intr(tty,QUITMASK);
				continue;
			}
		}
		if (c==10 || c==EOF_CHAR(tty))
			tty->secondary.data++;
		if (L_ECHO(tty)) {
			if (c==10) {
				PUTCH(10,tty->write_q);
				PUTCH(13,tty->write_q);
			} else if (c<32) {
				if (L_ECHOCTL(tty)) {
					PUTCH('^',tty->write_q);
					PUTCH(c+64,tty->write_q);
				}
			} else
				PUTCH(c,tty->write_q);
			tty->write(tty);
		}
		PUTCH(c,tty->secondary);
	}
	wake_up(&tty->secondary.proc_list);

}

// tty读函数
int tty_read(unsigned channel, char * buf, int nr){
	struct tty_struct * tty;
	char c, * b=buf;
	int minimum,time,flag=0;
	long oldalarm;

	if (channel>2 || nr<0) return -1;			// 参数无效
	tty = &tty_table[channel];
	oldalarm = current->alarm;					// 保存旧的报警定时器
	time = 10L*tty->termios.c_cc[VTIME];		// 设置读操作超时定时值
	minimum = tty->termios.c_cc[VMIN];			// 最少需要读取的字符个数
	if(time && !minimum){
		minimum = 1;
		if(flag=(!oldalarm || time+jiffies<oldalarm))
			current->alarm = time+jiffies;
	}
	if(minimum>nr)
		minimum = nr;			// 最多读取要求的字符数

	// 从辅助队列中循环取字符放入用户缓冲区中
	while(nr>0){
		if(flag && (current->signal & ALRMMASK)){
			current->signal &= ~ALRMMASK;
			break;
		}
		if(current->signal)						// 若进程原定时到或收到其他信号
			break;
		if(EMPTY(tty->secondary) || (L_CANON(tty) && !tty->secondary.data && LEFT(tty->secondary)>20)){	// 如果辅助队列为空或者设置了规范模式标志并且辅助队列字符行数为0以及辅助模式缓冲队列空闲空间>20
			sleep_if_empty(&tty->secondary);															// 进程休眠并返回继续处理
			continue;
		}
		//下面开始执行取字符操作
		do{
			GETCH(tty->secondary, c);					// 从队列尾部取出一个字符
			if(c==EOF_CHAR(tty) || c==10)				// 如果取出文件结束符或者取完一行(10为换行符NR)
				tty->secondary.data--;					// 行数递减
			if(c==EOF_CHAR(tty) && L_CANON(tty))		// 如果取出文件结束符并且规范模式标志置位状态
				return (b-buf);							// 返回已读数据并退出
			else {
				put_fs_byte(c, b++);					// 说明数据还在以流的形式读取中，于是将数据直接放入缓冲区buf中
				if(!--nr)
					break;
			}
		}while(nr>0 && !EMPTY(tty->secondary));			// 当所有字符都取出或者辅助队列为空时循环结束
		// 数据已经读完后续处理
		if(time && !L_CANON(tty))
			if(flag = (!oldalarm || time+jiffies<oldalarm))
				current->alarm = time+jiffies;
			else
				current->alarm = oldalarm;
		if(L_CANON(tty)){
			if(b-buf)
				break;
		}else if(b-buf>=minimum)
			break;
	}
	// 此时读取tty字符循环操作结束，程序恢复原值
	current->alarm = oldalarm;
	if(current->signal && !(b-buf))
		return -EINTR;
	return (b-buf);
}

/*
 * tty 写函数
 * 把用户缓冲区中的字符写入tty写队列缓冲区
 * channel 子设备号 buf 缓冲区指针 nr写字节数
 */
int tty_write(unsigned channel, char * buf, int nr){
	static int cr_flag=0;
	struct tty_struct * tty;			//定义tty结构体指针
	char c, *b=buf;

	if (channel>2 || nr<0) return -1;			// 参数无效
	tty = channel + tty_table;					// tty指向tty_table表中的tty结构
	while (nr>0) {								// 字符设备逐一处理
		sleep_if_full(&tty->write_q);
		if (current->signal)
			break;
		while (nr>0 && !FULL(tty->write_q)) {
			c=get_fs_byte(b);
			if (O_POST(tty)) {
				if (c=='\r' && O_CRNL(tty))
					c='\n';
				else if (c=='\n' && O_NLRET(tty))
					c='\r';
				if (c=='\n' && !cr_flag && O_NLCR(tty)) {
					cr_flag = 1;
					PUTCH(13,tty->write_q);
					continue;
				}
				if (O_LCUC(tty))
					c=toupper(c);
			}
			b++; nr--;								// 数据缓冲区指针迁移1字节 写字节数减1
			cr_flag = 0;
			PUTCH(c,tty->write_q);	// 将数据插入队列
		}
		tty->write(tty);
		if (nr>0)
			schedule();
	}
	return (b-buf);
}
/*
 * Jeh, sometimes I really like the 386.
 * This routine is called from an interrupt,
 * and there should be absolutely no problem
 * with sleeping even in an interrupt (I hope).
 * Of course, if somebody proves me wrong, I'll
 * hate intel for all time :-). We'll have to
 * be careful and see to reinstating the interrupt
 * chips before calling this, though.
 *
 * I don't think we sleep here under normal circumstances
 * anyway, which is good, as the task sleeping might be
 * totally innocent.
 */

void do_tty_interrupt(int tty)
{
	copy_to_cooked(tty_table+tty);
}


void chr_dev_init(void){
}

