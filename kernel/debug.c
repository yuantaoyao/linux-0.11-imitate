#define ZEROPAD	1		/* pad with zero 填充0*/ 
#define SIGN	2		/* unsigned/signed long 无符号/符号长整型*/
#define PLUS	4		/* show plus 显示加*/
#define SPACE	8		/* space if plus 如果加则置空格*/
#define LEFT	16		/* left justified 左调整*/
#define SPECIAL	32		/* 0x */
#define SMALL	64		/* use 'abcdef' instead of 'ABCDEF' 使用小写*/

#define do_div(n, base) ({ \
	int __res; \
	__asm__("divl %4":"=a" (n),"=d" (__res):"0" (n),"1" (0),"r" (base)); \
	__res; \
})


static char* number(int num, int size){
	int i = 0;
	char * str;
	char tmp[36];
	const char *digits = "0123456789abcdefghijklmnopqrstuvwxyz";
	if (num == 0)					//如果num为0 则直接把0放入字符串缓冲区
		tmp[i++] = '0';
	else while (num != 0)
		tmp[i++] = digits[do_div(num, 10)]; // 将数值转换成特定进制字符串
//	printk("u is %c\n", tmp[i]);
	while(i-->0)			////将转好的字符传入str
		*str++ = tmp[i];
	return str;
}


void debug_show_str(char * str){
	char *display_des = str;
	unsigned long video_mem_start = 0xb8000;
	char * display_ptr_t = ((char *)video_mem_start);
	while (*display_des) {					// 循环将display_des的值写入显存区
		*display_ptr_t++ = *display_des++;	
		display_ptr_t++;						// 空开属性字节
	}
}

void debug_show_int(long i){
	char * str;
	str = number(0, 10);
	debug_show_str(str);
}

