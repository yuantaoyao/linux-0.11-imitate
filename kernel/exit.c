/*
 *  linux/kernel/exit.c
 *
 *  (C) 1991  Linus Torvalds
 */

#include <errno.h>
#include <signal.h>
#include <sys/wait.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/tty.h>
#include <asm/segment.h>

int sys_pause(void);
int sys_close(int fd);
// 释放进程的函数
void release(struct task_struct * p){
	int i;
	if (!p)				// 有效性验证
		return ;
	for(i=1;i<NR_TASKS;i++)		// 扫描任务数组
		if(task[i] == p){			// 找到指定任务
			task[i] = NULL;			// 置空任务项
			free_page((long) p);		// 释放相应内存页
			schedule();					// 重新调度
			return ;
		}
	panic("trying to release non-existent task");		// 指定任务不存在则死机
}
// 向指定任务p发送信号sig，权值为priv（强制发送信号，即不考虑进程用户属性或级别）
static inline int send_sig(long sig,struct task_struct * p,int priv){
	if(!p || sig <1 || sig>32)				// 有效性验证
		return -EINVAL;
	if(priv || (current->euid==p->euid) || suser())			// 如果强制发送标志置位或当前进程的有效用户位就是指定进程的有效用户位或者当前进程为超级用户
		p->signal |= (1<<(sig-1));
	else
		return -EPERM;
	return 0;
}
// 终止会话
static void kill_session(void){
	struct task_struct **p = NR_TASKS + task;			// 指针*p首先指向任务数组最末端
	//扫描任务数组（除0任务外）
	while(--p>&FIRST_TASK){
		if(*p && (*p)->session == current->session)			// 如果会话号等于当前进程会话号
			(*p)->signal |= 1<<(SIGHUP-1);					// 发送挂断进程信号
	}
}

/*
 * XXX need to check permissions needed to send signals to process
 * groups, etc. etc.  kill() permissions semantics are tricky!
 */
 // 系统调用kill可用于向进程发送任何信号，而并非杀死进程
 // pid 进程号 sig需要发送的信号
 // pid>0 发送给指定进程
 // pid=0 发送给当前进程的进程组中的所有进程（表明当前进程是进程组组长）
 // pid=-1 发送给除第一个进程（初始进程init）外的所有进程
 // pid<-1 发送给进程组-pid的所有进程
 // sig=0n 不发送信号，但仍然进行错误检查，如果成功返回0
int sys_kill(int pid,int sig){
	struct task_struct **p = NR_TASKS + task;
	int err, retval = 0;

	if(!pid) while(--p > &FIRST_TASK) {					// 发送给组内所有成员
		if(*p && (*p)->pgrp == current->pid)
			if(err=send_sig(sig, * p, 1))			// 强制发送信号
				retval = err;
	} else if(pid>0) while(--p > &FIRST_TASK) {
		if(*p && (*p)->pid == pid)
			if(err = send_sig(sig, * p, 0))
				retval = err;
	} else if(pid == -1) while(--p > &FIRST_TASK) {
		if(err = send_sig(sig, * p, 0))
			retval = err;
	}else while(--p > &FIRST_TASK) {
		if (*p && (*p)->pgrp == -pid)
			if(err = send_sig(sig, * p, 0))
				retval = err;
	}
	return retval;
}
// 通知父进程 -- 向进程pid发送SIGCHLD 默认情况下子进程将停止或终止
static void tell_father(int pid){
	int i;
	if(pid)
		for(i=0;i<NR_TASKS;i++){
			if(!task[i])
				continue;
			if(task[i]->pid != pid)
				continue;
			task[i]->signal |= (1<<(SIGCHLD-1));
			return ;
		}
	/* if we don't find any fathers, we just release ourselves */
	/* This is not really OK. Must change it to make father 1 */
	// 如果没有找到父进程，则进程就自己释放
	printk("BAD BAD - no father found\n\r");
	release(current);
}
// 程序退出处理函数
int do_exit(long code){
	int i;
	// 释放当前进程代码段和数据段
	free_page_tables(get_base(current->ldt[1]), get_limit(0x0f));// 0x0f进程代码段选择符
	free_page_tables(get_base(current->ldt[2]), get_limit(0x17));// 0x17进程数据段选择符
	// 如果当前进程有子进程 将子进程的父进程改成1（init进程）
	for(i=0;i<NR_TASKS;i++)
		if(task[i] && task[i]->father == current->pid){
			task[i]->father = 1;
			if(task[i]->state == TASK_ZOMBIE)
				/* assumption task[1] is always init */
				(void) send_sig(SIGCHLD, task[1], 1);	// 如果子进程是僵尸状态则向进程1发送子进程终止信号

		}
	for(i=0;i<NR_OPEN;i++)			//关闭当前进程打开的多有文件
		if(current->filp[i])
			sys_close(i);

	// 对当前进程的工作目录pwd、根目录root以及执行程序文件的i节点进行同步操作，放回各个i节点并分别置空
	iput(current->pwd);
	current->pwd = NULL;
	iput(current->root);
	current->root = NULL;
	iput(current->executable);
	current->executable = NULL;
	// 如果当前进程是会话头领（leader）进程并且具有控制终端，则释放终端
	if(current->leader && current->tty >= 0)
		tty_table[current->tty].pgrp = 0;
	if(last_task_used_math == current)
		last_task_used_math = NULL;
	if(current->leader)				// 如果当前进程是leader进程，则终止会话的所有进程
		kill_session();
	current->state = TASK_ZOMBIE;			// 把当前进程置位僵死状态，表明资源已释放
	current->exit_code = code;
	tell_father(current->father);			// 通知父进程，子进程将终止或结束
	schedule();
	return (-1);
}
// 系统调用终止进程
// error_code - 状态信息，低位有效
int sys_exit(int error_code){
	return do_exit((error_code&0xff)<<8);
}
// 系统调用 挂起当前进程
// pid 进程号
// pid>0 指定进程
// pid=0 当前进程所在组的任何进程
// pid<-1 进程组号等于pid绝对值的任何子进程
// pid=-1 任何子进程
// options 操作
// options=WUNTRACED 如果子进程处于停止状态，马上返回
// options=WNOHANG 如果没有子进程退出或终止就马上返回
// stat_addr 保存状态信息位置的指针
int sys_waitpid(pid_t pid,unsigned long * stat_addr, int options){
	int flag, code;					// flag 
	struct task_struct ** p;

	verify_area(stat_addr, 4);
repeat:
	flag = 0;
	for(p=&LAST_TASK;p>&FIRST_TASK;--p){			// 从进程末尾扫描
		if(!*p || *p == current)					// 跳过空项和本进程
			continue;
		if((*p)->father != current->pid)			// 跳过非子进程
			continue;
		// 此时的进程一定是当前进程的子进程
		if(pid>0){
			if((*p)->pid != pid)
				continue;
		} else if(!pid){
			if((*p)->pgrp != current->pgrp)
				continue;
		} else if(pid!=-1){
			if((*p)->pgrp != -pid)
				continue;
		}
		// 如果前3个判断都不符合，说明该进程正在等待其任何子进程
		switch ((*p)->state){
			case TASK_STOPPED:
				if(!(options & WUNTRACED))
					continue;
				put_fs_long(0x7f, stat_addr);
				return (*p)->pid;
			case TASK_ZOMBIE:
				current->cutime += (*p)->utime;
				current->cstime += (*p)->stime;
				flag = (*p)->pid;							// 临时保存子进程
				code = (*p)->exit_code;						// 取子进程退出码
				release(* p);								// 释放该子进程
				put_fs_long(code, stat_addr);				// 置状态位为退出码
				return flag;
			default:
				flag = 1;
				continue;
		}
	}
	if(flag)
		if(options & WNOHANG)
			return 0;
		current->state = TASK_INTERRUPTIBLE;
		schedule();
		if(!(current->signal &= ~(1<<(SIGCHLD-1))))
			goto repeat;
		else
			return -EINTR;
	return -ECHILD;
}


