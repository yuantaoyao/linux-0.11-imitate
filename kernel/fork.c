/*
 *  linux/kernel/fork.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 *  'fork.c' contains the help-routines for the 'fork' system call
 * (see also system_call.s), and some misc functions ('verify_area').
 * Fork is rather simple, once you get the hang of it, but the memory
 * management can be a bitch. See 'mm/mm.c': 'copy_page_tables()'
 */
#include <errno.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/segment.h>
#include <asm/system.h>

long last_pid=0;


// 对addr到addr+size这段内存执行写操作前检测
void verify_area(void * addr,int size){
	unsigned long start;

	start = (unsigned long) addr;
	size += start & 0xfff;				// 起始位置所在页面中的偏移量 因为每页有4k内存即0x1000bit的大小所以查找某个地址在一页中的偏移量只需保留后12位 加上size即size此时为所在页面起始位置开始的范围值
	start &= 0xfffff000;				// 此时start为当前页面在当前进程空间中的逻辑地址
	start += get_base(current->ldt[2]);	// 此时的start加上进程数据段在线性地址空间中的起始地址 start变成真个系统整个线性地址空间中的地址位置
	while (size > 0) {					// 循环进行写页面验证
		size -= 4096;
		write_verify(start);
		start += 4096;
	}
}
// 获取一个闲置的进程号
int find_empty_process(void){
	int i;
repeat:
	if((++last_pid) < 0) last_pid = 1;
	for (i=0;i<NR_TASKS;i++)
		if(task[i] && task[i]->pid == last_pid) goto repeat;
	for (i=1;i<NR_TASKS;i++)
		if(!task[i]) return i;
	return -EAGAIN;
}

int copy_mem(int nr, struct task_struct * p) {
	unsigned long old_data_base, new_data_base, data_limit;
	unsigned long old_code_base, new_code_base, code_limit;

	code_limit = get_limit(0x0f);
	data_limit = get_limit(0x17);
	
	old_code_base = get_base(current->ldt[1]);
	old_data_base = get_base(current->ldt[2]);
	
	if (old_data_base != old_code_base)
		panic("We don't support separate I&D");
	if (data_limit < code_limit)
		panic("Bad data_limit");

	new_data_base = new_code_base = nr * 0x4000000;
	p->start_code = new_code_base;
	set_base(p->ldt[1], new_code_base);
	set_base(p->ldt[2], new_data_base);
	if (copy_page_tables(old_data_base, new_data_base, data_limit)) {
		printk("free_page_tables: from copy_mem\n");
		free_page_tables(new_data_base,data_limit);
		return -ENOMEM;
	}
	return 0;
}

/*
 *  Ok, this is the main fork-routine. It copies the system process
 * information (task[nr]) and sets up the necessary registers. It
 * also copies the data segment in it's entirety.
 */
int copy_process(int nr,long ebp,long edi,long esi,long gs,long none,
		long ebx,long ecx,long edx,
		long fs,long es,long ds,
		long eip,long cs,long eflags,long esp,long ss){

	struct task_struct *p;
	int i;
	struct file *f;

	p = (struct task_struct *) get_free_page();
	task[nr] = p;								// 将新任务指向新开辟空间
	if(!p)
		return -EAGAIN;
	//*p=*current并没有将进程0的进程描述体赋值给进程p地址指向的内存，而是赋给了以p地址为末尾、以sizeof(task_struct)为大小的一段内存。
	//这句c语句一定是用了rep指令，df位为向下拷贝（之前某个地方使用了std指令）。而新编译器编译c代码时，会认为已经cld过了，因此没加修改df位的指令。
	// 此处有两种解决方法1、手动清除df指令2、通过memcpy内存复制方式
	__asm__ volatile ("cld");	// 添加df清除指令
	*p = *current;  // 复制当前进程结构给新进程
	// memcpy(p,current, sizeof(struct task_struct));// 通过内存拷贝函数
	p->state = TASK_UNINTERRUPTIBLE;
	p->pid = last_pid;
	p->father = current->pid;
	p->counter = p->priority;
	p->signal = 0;
	p->alarm = 0;
	p->leader = 0;		/* process leadership doesn't inherit */
	p->utime = p->stime = 0;
	p->cutime = p->cstime = 0;
	p->start_time = jiffies;
	p->tss.back_link = 0;
	p->tss.esp0 = PAGE_SIZE + (long) p;
	p->tss.ss0 = 0x10;
	p->tss.eip = eip;
	p->tss.eflags = eflags;
	p->tss.eax = 0;
	p->tss.ecx = ecx;
	p->tss.edx = edx;
	p->tss.ebx = ebx;
	p->tss.esp = esp;
	p->tss.ebp = ebp;
	p->tss.esi = esi;
	p->tss.edi = edi;
	p->tss.es = es & 0xffff;
	p->tss.cs = cs & 0xffff;
	p->tss.ss = ss & 0xffff;
	p->tss.ds = ds & 0xffff;
	p->tss.fs = fs & 0xffff;
	p->tss.gs = gs & 0xffff;
	p->tss.ldt = _LDT(nr);
	p->tss.trace_bitmap = 0x80000000;
	
	// 如果当前任务使用协处理器，就保存上下文
	if(last_task_used_math == current)
		__asm__ ("clts; fnsave %0"::"m" (task[nr]->tss.i387));

	// 接下来复制进程页表
	if(copy_mem(nr, task[nr])){
		task[nr] = NULL;
		free_page((long) p);
		return -EAGAIN;
	}

	// 如果进程中有文件是打开的，则将对应的文件打开次数加1
	for (i=0;i<NR_OPEN;i++)
		if (f = task[nr]->filp[i])
			f->f_count++;
	if (current->pwd)
		current->pwd->i_count++;
	if (current->root)
		current->root->i_count++;
	if (current->executable)
		current->executable->i_count++;
	set_tss_desc(gdt+(nr<<1)+FIRST_TSS_ENTRY,&(task[nr]->tss));
	set_ldt_desc(gdt+(nr<<1)+FIRST_LDT_ENTRY,&(task[nr]->ldt));
	task[nr]->state = TASK_RUNNING;	/* do this last, just in case */
	return last_pid;
}
