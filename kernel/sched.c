/*
 *  linux/kernel/sched.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 * 'sched.c' is the main kernel file. It contains scheduling primitives
 * (sleep_on, wakeup, schedule etc) as well as a number of simple system
 * call functions (type getpid(), which just extracts a field from
 * current-task
 */
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/sys.h>
#include <linux/fdreg.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/segment.h>

#include <signal.h>
// 该宏取信号nr在信号位图中对应位的二进制数值，信号编号1-32.
#define _S(nr) (1<<((nr)-1))
// 除了SIGKILL和SIGSTOP信号都是可以阻塞的
#define _BLOCKABLE (~(_S(SIGKILL) | _S(SIGSTOP)))

void show_task(int nr,struct task_struct * p)
{
	int i,j = 4096-sizeof(struct task_struct);

	printk("%d: pid=%d, state=%d, ",nr,p->pid,p->state);
	i=0;
	while (i<j && !((char *)(p+1))[i])
		i++;
	printk("%d (of %d) chars free in kernel stack\n\r",i,j);
}

void show_stat(void)
{
	int i;

	for (i=0;i<NR_TASKS;i++)
		if (task[i])
			show_task(i,task[i]);
}

#define LATCH (1193180/HZ)

extern void mem_use(void);			// 一个无用的函数

extern int timer_interrupt(void);	// 时钟中断处理程序（kernel/system.call.s）
extern int system_call(void);		// 系统调用中断处理程序（kernel/system.call.s）
// 每个任务在内核运行时都有自己的内核态堆栈，这里定义了任务内核态的堆栈结构
union task_union {						// 联合结构体 任务数据结构与其堆栈数据
	struct task_struct task;			// 因为一个任务的数据结构与其内核态堆栈在同一内存页中，所以从堆栈寄存器ss可以获得其数据段选择符
	char stack[PAGE_SIZE];
};

static union task_union init_task = {INIT_TASK,};	// 定义初始任务的数据

long volatile jiffies=0;			// 从系统开机算起滴答数时间值的全局变量（10ms/滴答）
long startup_time=0;				// 开机时间 从1970：0：0：0开始计时的秒数
struct task_struct *current = &(init_task.task);				// 当前系统中正在运行的任务指针 
struct task_struct *last_task_used_math = NULL;

struct task_struct * task[NR_TASKS] = {&(init_task.task), };				// 系统中定义一个任务指针数组并将第一个任务放入数组0位置

long user_stack [ PAGE_SIZE>>2 ] ;		// 定义用户堆栈 共1k项 容量4k字节 在内核初始化操作过程中被用作内核栈，初始化完成后将被用作任务0的用户态堆栈

// 该结构用于设置堆栈ss:sp ss被设置为内核数据段选择符（0x10），指针esp指向user_stack数组最后一项后面 因为intel cpu执行堆栈操作先递减sp 然后在sp指针处保存入栈内容
struct {
	long * a;
	short b;
	} stack_start = { & user_stack [PAGE_SIZE>>2] , 0x10 };
/*
 *  'math_state_restore()' saves the current math information in the
 * old math state array, and gets the new ones from the current task
 */
void math_state_restore()
{
	if (last_task_used_math == current)
		return;
	__asm__("fwait");
	if (last_task_used_math) {
		__asm__("fnsave %0"::"m" (last_task_used_math->tss.i387));
	}
	last_task_used_math=current;
	if (current->used_math) {
		__asm__("frstor %0"::"m" (current->tss.i387));
	} else {
		__asm__("fninit"::);
		current->used_math=1;
	}
}

/*
 *  'schedule()' is the scheduler function. This is GOOD CODE! There
 * probably won't be any reason to change this, as it should work well
 * in all circumstances (ie gives IO-bound processes good response etc).
 * The one thing you might take a look at is the signal-handler code here.
 *
 *   NOTE!!  Task 0 is the 'idle' task, which gets called when no other
 * tasks can run. It can not be killed, and it cannot sleep. The 'state'
 * information in task[0] is never used.
 *
 * schedule() 是调度函数，这是一个好的代码！没有任何理由去修改它，因为它可以在所有环境下运行（比如能够对IO边界进行很好的相应），只有一件事值得注意就是这里的信号处理代码
 * 注意！！任务0是个闲置（‘idle’）任务只有当没有其他任务运行时才被调用，它不能被杀掉且不能随眠。 任务0的状态标记state是从来不用的
 *
 *
 */
void schedule(void)
{
	int i,next,c;
	struct task_struct **p;			// 任务机构指针的指针
/* check alarm, wake up any interruptible tasks that have got a signal */
	// 检查alarm(进程报警定时值)，唤醒任何已得到信号的可中断任务
	for(p = &LAST_TASK;p>&FIRST_TASK;--p)				// 遍历所有任务
		if(*p){					// 如果任务存在
			//检查是否设置过任务的alarm,并且已经过时（alarm<jiffies），则在信号位图中置SIGALARM信号，同时清除alarm
			if((*p)->alarm && (*p)->alarm < jiffies) {
				(*p)->signal |= (1 << (SIGALRM - 1));
				(*p)->alarm = 0;
			}
			//如果信号位图中除了被阻塞的信号还有其他信号，并且任务处于可中断状态，则信号任务为就绪状态
			if(((*p)->signal & ~(_BLOCKABLE & (*p)->blocked)) && ((*p)->state == TASK_INTERRUPTIBLE))
				(*p)->state = TASK_RUNNING;								// 置位就绪状态
		}
	/* this is the scheduler proper: */
		// 这里是调度的主要部分
	while(1){
		c = -1;
		next = 0;
		i = NR_TASKS;
		p = &task[NR_TASKS];	// p 指向最后一个任务的结构化地址
		while(--i){
			if(!*--p)		// 如果任务不存在则退出
				continue;
			if((*p)->state == TASK_RUNNING && (*p)->counter > c)		//选取一个就绪任务并且任务递减滴答数值最大                                            ？
				c = (*p)->counter,next = i;			// 确定下一个要运行的任务
		}
		if(c) break;				// 如果找出系统大于等于0的结果则跳出整个循环
		for (p = &LAST_TASK;p>&FIRST_TASK;--p)	// 如果没有一个可运行的程序
			if(*p)
				(*p)->counter = ((*p)->counter >> 1) + (*p)->priority;		// 就根据任务的优先级更新每一个任务的counter 并继续循环
	}
	switch_to(next);
}

int sys_pause(void)
{
	current->state = TASK_INTERRUPTIBLE;
	schedule();
	return 0;
}

void sleep_on(struct task_struct **p){
	struct task_struct *tmp;
	if(!p)
		return;
	if(current == &(init_task.task))
		panic("task[0] trying to sleep");
	tmp = *p;
	*p = current;
	current->state = TASK_UNINTERRUPTIBLE;
	schedule();
	if(tmp)
		tmp->state = 0;
}

void interruptible_sleep_on(struct task_struct **p){
	struct task_struct *tmp;
	if(!p)
		return;
	if(current == &(init_task.task))
		panic("task[0] trying to sleep");
	tmp = *p;
	*p = current;
repeat:current->state = TASK_INTERRUPTIBLE;
	schedule();
	if(*p && *p != current){
		(**p).state = 0;
		goto repeat;
	}
	*p = NULL;
	if(tmp)
		tmp->state = 0;
}

void wake_up(struct task_struct **p){
	if(p && *p){
		(**p).state=0;
		*p = NULL;
	}
}

/*
 * OK, here are some floppy things that shouldn't be in the kernel
 * proper. They are here because the floppy needs a timer, and this
 * was the easiest way of doing it.
 */
static struct task_struct * wait_motor[4] = {NULL,NULL,NULL,NULL};
static int  mon_timer[4]={0,0,0,0};
static int moff_timer[4]={0,0,0,0};
unsigned char current_DOR = 0x0C;

int ticks_to_floppy_on(unsigned int nr)
{

	extern unsigned char selected;
	unsigned char mask = 0x10 << nr;

	if (nr>3)
		panic("floppy_on: nr>3");
	moff_timer[nr]=10000;		/* 100 s = very big :-) */
	cli();				/* use floppy_off to turn it off */
	mask |= current_DOR;
	if (!selected) {
		mask &= 0xFC;
		mask |= nr;
	}
	if (mask != current_DOR) {
		outb(mask,FD_DOR);
		if ((mask ^ current_DOR) & 0xf0)
			mon_timer[nr] = HZ/2;
		else if (mon_timer[nr] < 2)
			mon_timer[nr] = 2;
		current_DOR = mask;
	}
	sti();
	return mon_timer[nr];
}

void floppy_on(unsigned int nr)
{
	cli();
	while (ticks_to_floppy_on(nr))
		sleep_on(nr+wait_motor);
	sti();
}

void floppy_off(unsigned int nr)
{
	moff_timer[nr]=3*HZ;
}

void do_floppy_timer(void){
	int i;
	unsigned char mask = 0x10;

	for (i=0 ; i<4 ; i++,mask <<= 1) {
		if (!(mask & current_DOR))
			continue;
		if (mon_timer[i]) {
			if (!--mon_timer[i])
				wake_up(i+wait_motor);
		} else if (!moff_timer[i]) {
			current_DOR &= ~mask;
			outb(current_DOR,FD_DOR);
		} else
			moff_timer[i]--;
	}
}

#define TIME_REQUESTS 64

static struct timer_list {
	long jiffies;
	void (*fn)();
	struct timer_list * next;
} timer_list[TIME_REQUESTS], * next_timer = NULL;

void add_timer(long jiffies, void (*fn)(void))
{
	struct timer_list * p;

	if (!fn)
		return;
	cli();
	if (jiffies <= 0)
		(fn)();
	else {
		for (p = timer_list ; p < timer_list + TIME_REQUESTS ; p++)
			if (!p->fn)
				break;
		if (p >= timer_list + TIME_REQUESTS)
			panic("No more time requests free");
		p->fn = fn;
		p->jiffies = jiffies;
		p->next = next_timer;
		next_timer = p;
		while (p->next && p->next->jiffies < p->jiffies) {
			p->jiffies -= p->next->jiffies;
			fn = p->fn;
			p->fn = p->next->fn;
			p->next->fn = fn;
			jiffies = p->jiffies;
			p->jiffies = p->next->jiffies;
			p->next->jiffies = jiffies;
			p = p->next;
		}
	}
	sti();
}

void do_timer(long cpl)
{
	extern int beepcount;
	extern void sysbeepstop(void);

	if (beepcount)
		if (!--beepcount)
			sysbeepstop();

	if (cpl)
		current->utime++;
	else
		current->stime++;

	if (next_timer) {
		next_timer->jiffies--;
		while (next_timer && next_timer->jiffies <= 0) {
			void (*fn)(void);
			
			fn = next_timer->fn;
			next_timer->fn = NULL;
			next_timer = next_timer->next;
			(fn)();
		}
	}
	if (current_DOR & 0xf0)
		do_floppy_timer();
	if ((--current->counter)>0) return;
	current->counter=0;
	if (!cpl) return;
	//schedule();

}

int sys_alarm(long seconds)
{
	int old = current->alarm;

	if (old)
		old = (old - jiffies) / HZ;
	current->alarm = (seconds>0)?(jiffies+HZ*seconds):0;
	return (old);

}

int sys_getpid(void)
{
	return current->pid;
}

int sys_getppid(void)
{
	return current->father;
}

int sys_getuid(void)
{
	return current->uid;
}

int sys_geteuid(void)
{
	return current->euid;
}

int sys_getgid(void)
{
	return current->gid;
}

int sys_getegid(void)
{
	return current->egid;
}

int sys_nice(long increment)
{
	
	return 0;
}

void sched_init(void)
{
	int i;
	struct desc_struct * p;						// 描述符表结构指针
	if (sizeof(struct sigaction) != 16)		// 根据POSIX标准信号状态结构体的长度必须是16位
		panic("Struct sigaction MUST be 16 bytes!!");
	set_ldt_desc(gdt + FIRST_LDT_ENTRY, &(init_task.task.ldt));
	set_tss_desc(gdt + FIRST_TSS_ENTRY, &(init_task.task.tss));		// 在全局变量表中分别设置初始任务的任务状态描述符和局部数据表述符
	p = gdt +2 +FIRST_TSS_ENTRY;
	for (i=1;i<NR_TASKS;i++){
		task[i] = NULL;						// 清空任务数组
		p->a=p->b=0;						// 每一个任务分辨占用一个tss和ldt
		p++;
		p->a=p->b=0;
		p++;
	}
	/* Clear NT, so that we won't have troubles with that later on */
	// 清除NT位 EFLAGES寄存器中的第14位位NT位 0、中断门或陷阱门的返回 1、任务的嵌套返回
	__asm__("pushfl ; andl $0xffffbfff,(%esp) ; popfl");
	ltr(0);
	lldt(0);

	outb_p(0x36, 0x43);
	outb_p(LATCH & 0xff, 0x40);		// 定时值低字节
	outb(LATCH >> 8, 0x40);			// 定时值高字节
	set_intr_gate(0x20, &timer_interrupt);	// 设置时钟中断处理程序
	outb(inb(0x21)&~0x01, 0x21);			// 修改时钟控制器编码允许时钟中断
	set_system_gate(0x80, &system_call);	// 设置系统调用门
}
