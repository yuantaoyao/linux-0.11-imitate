/*
 *  linux/kernel/signal.c
 *
 *  (C) 1991  Linus Torvalds
 */

#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/segment.h>

#include <signal.h>

volatile void do_exit(int error_code);
// 获取当前信号屏蔽位
int sys_sgetmask()
{
	return current->blocked;
}
// 设置新的信号屏蔽位 并返回原屏蔽位
int sys_ssetmask(int newmask)
{
	int old = current->blocked;
	current->blocked = newmask & ~(1 << (SIGKILL - 1));
	return old;
}
// 复制sigaction数据到fs数据段to处。即从内核空间复制到用户（任务）数据段中
static inline void save_old(char * from,char * to){
	int i;
	//首先验证to处的内存是否足够大。然后把一个sigaction结构复制到fs段（用户）
	verify_area(to, sizeof(struct sigaction));
	for (i=0; i<sizeof(struct sigaction);i++){
		put_fs_byte(*from, to);
		from++;
		to++;
	}
}
// 复制sigaction数据从fs数据段from位置复制到to处。从用户空间复制到内核空间
static inline void get_new(char * from,char * to){
	int i;
	for (i=0;i<sizeof(struct sigaction);i++){
		*(to++) = get_fs_byte(from++);
	}	
}

int sys_signal(int signum, long handler, long restorer)
{
	
	struct sigaction tmp;

	if (signum<1 || signum>32 || signum==SIGKILL)
		return -1;
	tmp.sa_handler = (void (*)(int)) handler;
	tmp.sa_mask = 0;
	tmp.sa_flags = SA_ONESHOT | SA_NOMASK;
	tmp.sa_restorer = (void (*)(void)) restorer;
	handler = (long) current->sigaction[signum-1].sa_handler;
	current->sigaction[signum-1] = tmp;
	return handler;
}

int sys_sigaction(int signum, const struct sigaction * action,
	struct sigaction * oldaction)
{
	
	struct sigaction tmp;

	if (signum<1 || signum>32 || signum==SIGKILL)
		return -1;
	tmp = current->sigaction[signum-1];
	get_new((char *) action,
		(char *) (signum-1+current->sigaction));
	if (oldaction)
		save_old((char *) &tmp,(char *) oldaction);
	if (current->sigaction[signum-1].sa_flags & SA_NOMASK)
		current->sigaction[signum-1].sa_mask = 0;
	else
		current->sigaction[signum-1].sa_mask |= (1<<(signum-1));
	return 0;
}

void do_signal(long signr,long eax, long ebx, long ecx, long edx,
	long fs, long es, long ds,
	long eip, long cs, long eflags,
	unsigned long * esp, long ss){		// 执行中断时压入用户栈地址ss和esp、标志寄存器、返回地址cs和ip
	unsigned long sa_handler;
	long old_eip = eip;
	struct sigaction * sa = current->sigaction + signr - 1; // current->sigaction[signr-1]  获取待处理信号
	int longs;
	unsigned long * tmp_esp;
	sa_handler = (unsigned long) sa->sa_handler;
	if (sa_handler == 1)	// 如果信号是SIG_IGN(1,默认忽略句柄) 不处理
		return ;
	if(!sa_handler)	{			// 如果句柄为SIG_DEL(0,默认处理)
		if (signr == SIGCHLD)	// 子进程停止或终止
			return ;
		else
			do_exit(1<<(signr - 1));
	}
	if (sa->sa_flags & SA_ONESHOT) //如果信号句柄只使用一次则信号句柄置空
		sa->sa_handler = NULL;
	*(&eip) = sa_handler;				// 将一个变量作为指针指向一个函数 （可能只有C语言才有这样灵活的写法）
	longs = (sa->sa_flags & SA_NOMASK)?7:8;
	*(&esp) -= longs;
	verify_area(esp, longs*4);
	tmp_esp=esp;
	put_fs_long((long) sa->sa_restorer,tmp_esp++);
	put_fs_long(signr,tmp_esp++);
	if (!(sa->sa_flags & SA_NOMASK))
		put_fs_long(current->blocked,tmp_esp++);
	put_fs_long(eax,tmp_esp++);
	put_fs_long(ecx,tmp_esp++);
	put_fs_long(edx,tmp_esp++);
	put_fs_long(eflags,tmp_esp++);
	put_fs_long(old_eip,tmp_esp++);
	current->blocked |= sa->sa_mask;
}
