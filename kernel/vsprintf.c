/*
 *  linux/kernel/vsprintf.c
 *
 *  (C) 1991  Linus Torvalds
 */

/* vsprintf.c -- Lars Wirzenius & Linus Torvalds. */
/*
 * Wirzenius wrote this portably, Torvalds fucked it up :-)
 */

#include <stdarg.h>
#include <string.h>


#define is_digit(c) ((c)>='0' && (c) <= '9')		//判断一个字符串是否在0-9范围内

static int skip_atoi(const char **s)				//将数字字符串转换成数值
{
	int i = 0;

	while (is_digit(**s))
		i = i * 10 +*((*s)++) - '0';
	return i;
}
//定义转换类型的符号常量23d
#define ZEROPAD	1		/* pad with zero 填充0*/ 
#define SIGN	2		/* unsigned/signed long 无符号/符号长整型*/
#define PLUS	4		/* show plus 显示加*/
#define SPACE	8		/* space if plus 如果加则置空格*/
#define LEFT	16		/* left justified 左调整*/
#define SPECIAL	32		/* 0x */
#define SMALL	64		/* use 'abcdef' instead of 'ABCDEF' 使用小写*/

#define do_div(n, base) ({ \
	int __res; \
	__asm__("divl %4":"=a" (n),"=d" (__res):"0" (n),"1" (0),"r" (base)); \
	__res; \
})
/**
  *将指定数字转换成不同格式的数字字符串
  * num 整数
  * base 进制
  * size 字符串长度
  * precision 字符串长度（精度）
  * type 类型选项
  * str 输出字符串指针
  */
static char* number(char * str, int num, int base, int size, int precision, int type){
	char c, sign, tmp[36];
	const char *digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int i;

	if(type & SMALL) digits="0123456789abcdefghijklmnopqrstuvwxyz"; // 如果指定小写则使用小写库
	if(type&LEFT) type &= ~ZEROPAD; // 如果类型中指定左调整 则屏蔽类型中的填零标记
	if (base<2 || base>36) // 如果进制不在2-36之间则退出
		return 0;
	c = (type & ZEROPAD) ? '0':' '; //如果type中存在填零标记则c='0'否正c=' '
	if (type&SIGN && num < 0){// 如果类型标记是有符号数且数值小于0 说明是负数
		sign = '-';// 符号为负数
		num = -num; // 将数值转为正数
	}else{
		sign = (type&PLUS) ? '+' : ((type&SPACE) ? ' ':0); // 如果类型中包含PLUS则赋'+' 否则如果包含SPACE则赋值' ',要么赋值0
	}
	if(sign) size--; // 若有符号则宽度减1
	if(type&SPECIAL){ // 如果是特殊字符
		if (base == 16)
			size -= 2;	// 如果是16进制则长度减2
		else if(base == 8) // 如果是8进制则长度减1
			size --;
	}
	i = 0;
	if (num == 0)					//如果num为0 则直接把0放入字符串缓冲区
		tmp[i++] = '0';
	else while (num != 0)
		tmp[i++] = digits[do_div(num, base)]; // 将数值转换成特定进制字符串
	
	if(i > precision) precision = i; // 如果字符长度大于精度，则长度扩展为精度
	size -= precision; // 字符长度减去用于存放数值字符的个数

	if (!(type&(ZEROPAD + LEFT))) //如果不允许填零也没有左对齐 
		while (size-- > 0)
			*str++ = ' ';
	if (sign)
		*str++ = sign; //如果有符号数先将符号存入str缓冲区

	if (type&SPECIAL) { // 存在特殊字符
		if (base == 8) //如果是8进制 以0打头
			*str++ = '0';
		if (base == 16){//如果是16进制 以0x打头
			*str++ = '0';
			*str++ = digits[33];
		}
	}
	if (!(type&LEFT))
		while (size-- > 0)
			*str++ = c; // 如果没有左对齐剩余的长度用c变量填充

	while (i<precision--)
		*str++ = '0';
	while(i-->0)			////将转好的字符传入str
		*str++ = tmp[i];
	while(size-->0)
		*str++ = ' ';
	return str;
}

// buf 为输出缓冲区 fmt 是格式字符串 args 是数值变换值
int vsprintf(char *buf, const char *fmt, va_list args)
{
	int len;
	int i;
	char * str;
	char * s;
	int * ip;
	int flags;

	int field_width;		//输出字段宽度
	int precision;			//格式转换字符串的精度
	int qualifier;			//长度修饰符

	// 首先将字符串指向buf,然后扫描格式字符串，对各个格式转换指示进行相应的处理
	for (str=buf;*fmt;++fmt)
	{
		// 格式字符串均以‘%’开头，如果不是以'%'开头则做普通字符串处理，依次存入str
		if(*fmt != '%')
		{
			*str++ = *fmt;
			continue;
		}

		flags = 0;

		repeat:
			++fmt;
			switch (*fmt)
			{
				case '-': flags |= LEFT; goto repeat; // 左靠齐调整
				case '+': flags |= PLUS; goto repeat; // 放加号。
				case ' ': flags |= SPACE; goto repeat; // 放空格。
				case '#': flags |= SPECIAL; goto repeat; // 是特殊转换。
				case '0': flags |= ZEROPAD; goto repeat; // 要填零(即'0')
			}
			
		//以下取当前参数字段宽度阈值，放入field_width中。
		field_width = -1; 
		if(is_digit(*fmt))
			field_width = skip_atoi(&fmt); // 如果宽度域后是数值则直接将数值赋给宽度域
		else if(*fmt == '*'){	// 如果宽度域后是'*'号，则代表下一个数值是宽度域
			++fmt; // 源码中没有这句 是一个bug   	ars Wirzenius 是 Linus 的好友，在 Helsinki 大学时曾同处一间办公室。在 1991 年夏季开发 Linux时，Linus 当时对 C 语言还不是很熟悉，还不会使用可变参数列表函数功能。因此 Lars Wirzenius 就为他编写了这段用于内核显示信息的代码。他后来(1998 年)承认在这段代码中有一个 bug，直到 1994 年才有人发现，并予以纠正。这个 bug 是在使用*作为输出域宽度时，忘记递增指针跳过这个星号了。在本代码中这个 bug 还仍然存在（130 行）。 他的个人主页是 http://liw.iki.fi/liw
			field_width = va_arg(args, int); // 通过va_arg获取宽度值
			if(field_width < 0){ // 如果宽度值小于0则说明其带有标志'-' 即左靠齐调整 首先将字段宽度取绝对值赋给field_width，然后加入左对齐标记
				field_width = -field_width;
				flags |= LEFT;
			}
		}
		// 以下取格式转换的精度域赋给precision变量中;其处理过程与field_width基本相同
		precision = -1;
		if (*fmt == '.'){ // 精度域开始的标记是'.'
			++fmt; //跳过'.'  
			if(is_digit(*fmt))
				precision = skip_atoi(&fmt);
			else if(*fmt == '*')
				++fmt; // 源码中没有这句 是一个bug
				precision = va_arg(args, int);
			if(precision < 0)
				precision = 0;
		}
		// 以下分析长度修饰符，存入qualifier变量，
		qualifier = -1;
		if(*fmt == 'h' || *fmt == 'l' || *fmt == 'L'){// h 带符号整数或无符号短整数 l 长整数或无符号长整数 L 长双精度参数
			qualifier = *fmt;
			++fmt;
		}

		switch (*fmt)
		{
			case 'c':					// 标记'c'表示对应参数是字符
				if (!(flags & LEFT))	// 如果 没有左对齐标记则在字符前面添加field_width个空格
					while (--field_width > 0)
						*str++ = ' ';
							
					*str++ = (unsigned char) va_arg(args, int); // 添加字符
					while (--field_width > 0)	//如果有左对齐或者field_width还有值则在字符后面添加field_width个空格
						*str++ = ' ';
				break;
			case 's': 					// 标记's'表示对应参数是字符串
				s = va_arg(args, char *); 	// 获取字符串
				len = strlen(s);			// 获取字符串长度
				if (precision < 0)			// 如果精度域没有明确表示则把字符长度赋给精度域	
					precision = len;
				if (len > precision)		// 如果精度域小于字符串长度则扩展精度长度=字符串长度	
					precision = len;
				if(!(flags & LEFT))			// 如果没有左对齐则在字符串前添加field_width-len个空格
					while (len < field_width--)
						*str++ = ' ';
				for (i = 0; i < len; i++)	// 将字符串添加到参数字符串
					*str++ = *s++;
				
				while (--field_width > len)	//如果有左对齐或者field_width还有值则在字符后面添加field_width-len个空格
						*str++ = ' ';
				break;
			case 'o':			// 如果格式转换符是'o'，表示需将对应的参数转换成八进制数的字符串。调用 number()函数处理
				str = number(str, va_arg(args, unsigned long), 8,
								field_width, precision, flags);
				break;
			case 'p': // 代表指针类型
				if (field_width == -1){
					field_width = 8;		//此时若该参数没有设置宽度域，则默认宽度为 8，并且需要添零
					flags |= ZEROPAD;
				}
				str = number(str, (unsigned long) va_arg(args, char *), 16
							, field_width, precision, flags);
				break;
			case 'x':
				flags |= SMALL;
			case 'X':
				str = number(str, va_arg(args, unsigned long), 16,
								field_width, precision, flags);
				break;
			case 'd':
			case 'i':
				flags |= SIGN;
			case 'u':
				str = number(str, va_arg(args, unsigned long), 10,
								field_width, precision, flags);
				break;
			case 'n':
				ip = va_arg(args, int*);
				*ip = (str - buf);
				break;

			default:
				if(*fmt != '%')			// 如果转换字符不是'%'则把'%'传入参数字符串
					*str++ = '%';
				if(*fmt)				// 如果格式转换符还有值则直接添加到参数字符串
					*str++ = *fmt;
				else
					--fmt;
				break;
		}
	}
		
	*str = '\0';
	return str - buf;
}

