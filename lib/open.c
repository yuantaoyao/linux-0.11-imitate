/*
 *  linux/lib/open.c
 *
 *  (C) 1991  Linus Torvalds
 */

#define __LIBRARY__
#include <unistd.h>
#include <stdarg.h>

int open(const char * filename, int flag,...){
	register int res;
	va_list args;

	va_start(args, flag);
	__asm__ ("int $0x80"
		:"=a" (res)
		:"0" (__NR_open), "b" (filename), "c" (flag), "d" (va_arg(args, int)));
	if(res>=0)
		return res;
	errno = -res;
	return -1;
}





