/*
 *  linux/lib/write.c
 *
 *  (C) 1991  Linus Torvalds
 */

#define __LIBRARY__
#include <unistd.h>

inline _syscall3(int,write,int,fd,const char *,buf,off_t,count)

