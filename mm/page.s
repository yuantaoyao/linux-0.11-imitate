/*
 *  linux/mm/page.s
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 * page.s contains the low-level page-exception code.
 * the real work is done in mm.c
 */

.globl page_fault
/*
 * 1、当CPU发现对应页目录项或页表项磁道存在位（Present）标志位0 
 * 2、当前进程没有访问指定页面的权限
 * 以上两个条件满足时，就会发生页异常中断
 */
page_fault:
	xchgl %eax,(%esp)		# 取出错码到eax 出错码格式是一个32位的长字，但只用了后3位。位2（U/S）- 0表示在超级用户下执行 1表示在用户下执行
	pushl %ecx			#							   位1（W/R）- 0表示读 1表示写
	pushl %edx			#							   位0（P）     - 0表示页不存在 1表示页级保护 
	push %ds
	push %es
	push %fs
	movl $0x10,%edx			# 设置内核数据段选择符
	mov %dx,%ds
	mov %dx,%es
	mov %dx,%fs
	movl %cr2,%edx			# 取引起缺页异常的线性地址
	pushl %edx			# cr2(控制寄存器2)cpu将造成异常的用于访问的地址存放在CR2中。异常程序可以使用这个地址来定位相应的页目录和页表项，如果不在异常处理期间允许发生另一个页异常，那么处理程序应该将CR2压入堆栈中
	pushl %eax
	testl $1,%eax			# 测试页存在标志P(位0)， 如果不是缺页引起的异常则跳转到1
	jne 1f
	call do_no_page			# 调用缺页异常处理程序
	jmp 2f
1:	call do_wp_page			# 调用写保护处理函数
2:	addl $8,%esp
	pop %fs
	pop %es
	pop %ds
	popl %edx
	popl %ecx
	popl %eax
	iret