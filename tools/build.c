/*
 *  linux/tools/build.c
 *
 *  (C) 1991  Linus Torvalds
 */

/*
 * This file builds a disk-image from three different files:
 *
 * - bootsect: max 510 bytes of 8086 machine code, loads the rest
 * - setup: max 4 sectors of 8086 machine code, sets up system parm
 * - system: 80386 code for actual system
 *
 * It does some checking that all files are of the correct type, and
 * just writes the result to stdout, removing headers and padding to
 * the right amount. It also writes some system data to stderr.
 */

/*
 * Changes by tytso to allow root device specification
 */

#include <stdio.h>	/* fprintf */
#include <string.h>
#include <stdlib.h>	/* contains exit */
#include <sys/types.h>	/* unistd.h needs this */
#include <sys/stat.h>
#include <linux/fs.h>
#include <unistd.h>	/* contains read/write */
#include <fcntl.h>

/*
 * Changes by falcon<zhangjinw@gmail.com> to define MAJOR and MINOR for they
 * are not defined in current linux header file linux/fs.h,I copy it from
 * include/linux/fs.h directly.
 */

#ifndef MAJOR
	#define MAJOR(a) (((unsigned)(a))>>8)
#endif
#ifndef MINOR
	#define MINOR(a) ((a)&0xff)
#endif

#define MINIX_HEADER 32
#define GCC_HEADER 1024

#define SYS_SIZE 0x3000

/*
 * Changes by falcon<zhangjinw@gmail.com> to let this kernel Image file boot
 * with a root image file on the first hardware device /dev/hd1, hence, you
 * should prepare a root image file, and configure the bochs with
 * the following lines(please set the ... as suitable info):
 * 	...
 *      floppya: 1_44="Image", status=inserted
 *      ata0-master: type=disk, path="/path/to/rootimage.img", mode=flat ...
 *      ...
 */

#define DEFAULT_MAJOR_ROOT 3
#define DEFAULT_MINOR_ROOT 1

/* max nr of sectors of setup: don't change unless you also change
 * bootsect etc */
#define SETUP_SECTS 4

#define STRINGIFY(x) #x

void die(char * str)
{
	fprintf(stderr,"%s\n",str);
	exit(1);
}

void usage(void)
{
	die("Usage: build bootsect setup system [rootdev] [> image]");
}
int main(int argc, char ** argv)
{
	int i,c,id;
	char buf[1024];
	char major_root, minor_root;
	struct stat sb;
	
	if ((argc != 4) && (argc != 5))			// 如果参数是5 个，则说明带有根设备名
		usage();
	if (argc == 5) {
		if (strcmp(argv[4], "FLOPPY")) {
			if (stat(argv[4], &sb)) {
				perror(argv[4]);
				die("Couldn't stat root device.");
			}
			major_root = MAJOR(sb.st_rdev);
			minor_root = MINOR(sb.st_rdev); //若成功则取该设备名状态结构中的主设备号和次设备号
		} else {
			major_root = 0;
			minor_root = 0;
		}
	} else {
		major_root = DEFAULT_MAJOR_ROOT;
		minor_root = DEFAULT_MINOR_ROOT;
	}
	
	if ((major_root != 2) && (major_root != 3) &&
	    (major_root != 0)) {
		fprintf(stderr, "Illegal root device (major = %d)\n",
			major_root);
		die("Bad root device --- major #");
	}
	
	
	for (i=0;i<sizeof buf; i++) buf[i]=0;
	
	if ((id=open(argv[1],O_RDONLY,0))<0)              //检测bootsect文件是否可以打开
		die("Unable to open 'boot'");

	
/*
	if (((long *) buf)[0]!=0x04100301)
		die("Non-Minix header of 'boot'");
	if (((long *) buf)[1]!=MINIX_HEADER)
		die("Non-Minix header of 'boot'");
	if (((long *) buf)[3]!=0)
		die("Illegal data segment in 'boot'");
	if (((long *) buf)[4]!=0)
		die("Illegal bss in 'boot'");
	if (((long *) buf)[5] != 0)
		die("Non-Minix header of 'boot'");
	if (((long *) buf)[7] != 0)
		die("Illegal symbol table in 'boot'");
*/

	i=read(id,buf,sizeof buf);						 //打开文件并放入buf中 返回读取的长度 长度应该为512							
	fprintf(stderr,"Boot sector %d bytes.\n",i);
	
	if (i != 512)
		die("Boot block must be exactly 512 bytes");
	if ((*(unsigned short *)(buf+510)) != 0xAA55)		//检查bootsect 是否以 AA55 结尾
		die("Boot block hasn't got boot flag (0xAA55)");
		
	buf[508] = (char) minor_root;
	buf[509] = (char) major_root; 
	 /* 将该bootsect 块512 字节的数据写到标准输出stdout，若写出字节数不对，则显示出错信息，退出。
	 0 是标准输入（stdin）
	 1 是一个文件描述符，指向标准输出（stdout）
	 2 指向标准错误输出（stderr）*/
	i=write(1,buf,512);  
	if (i!=512)
		die("Write call failed");
	 /* 最后关闭bootsect 模块文件。*/
	close (id);
	
	if ((id=open(argv[2],O_RDONLY,0))<0) //检测setup文件是否可以打开
		die("Unable to open 'setup'");

	
/*
	if (((long *) buf)[0]!=0x04100301)
		die("Non-Minix header of 'setup'");
	if (((long *) buf)[1]!=MINIX_HEADER)
		die("Non-Minix header of 'setup'");
	if (((long *) buf)[3]!=0)
		die("Illegal data segment in 'setup'");
	if (((long *) buf)[4]!=0)
		die("Illegal bss in 'setup'");
	if (((long *) buf)[5] != 0)
		die("Non-Minix header of 'setup'");
	if (((long *) buf)[7] != 0)
		die("Illegal symbol table in 'setup'");
*/
	for (i=0 ; (c=read(id,buf,sizeof buf))>0 ; i+=c )  //这里的i是真实的setup文件大小 以下将用到i
		if (write(1,buf,c)!=c)
			die("Write call failed");
	close (id);
	
	if (i > SETUP_SECTS*512)                      // 如果setup大于4个扇区 则这个文件太大了
		die("Setup exceeds " STRINGIFY(SETUP_SECTS)" sectors - rewrite build/boot/setup");
	fprintf(stderr,"Setup is %d bytes.\n",i);   // 输出setup字节数
	
	/* 将缓冲区buf 清零。*/
	for (c=0 ; c<sizeof(buf) ; c++)       
		buf[c] = '\0';
	/* 若setup 长度小于4*512 字节，则用/0 将setup 填足为4*512 字节。*/
	while (i<SETUP_SECTS*512) {       //如果当前已填充不到4个扇区
		c = SETUP_SECTS*512-i;         // 获取剩余扇区
		if (c > sizeof(buf))		  // 如果剩余扇区大于1024 则只处理1024个字节剩下的下次处理 也就是剩余带扩展大小大于1k时说缓冲区的大小以1k/次的处理递增，否则缓冲区大小增加c字节
			c = sizeof(buf);
		if (write(1,buf,c) != c)
			die("Write call failed");
		i += c;
	}
	
	/* 开始处理system*/
	if ((id=open(argv[3],O_RDONLY,0))<0)
		die("Unable to open 'system'");
	
	for (i=0 ; (c=read(id,buf,sizeof buf))>0 ; i+=c )     //打开system 这里的i是真实的system文件大小 
		if (write(1,buf,c)!=c)			//  将system 不做任何处理地追加到标准输出设备
			die("Write call failed");
	
	close(id);
	fprintf(stderr,"System is %d bytes.\n",i);
	if (i > SYS_SIZE*16)
		die("System is too big");
	return(0);
}
